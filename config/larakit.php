<?php
return [
    //варианты авторизации
    //"simple" - через логин/пароль
    //"cas" - через Taxsee
    //"ulogin" - через Ulogin
    'auth'      => 'simple',
    'translate' => env('TRANSLATE_KEY'),
    'drivers'   => [
        'ulogin' => [
            'title'        => 'Авторизация через социальные сети',
            'display'      => 'panel',
            'fields'       => [
                //first_name – имя пользователя,
                'first_name',
                //last_name – фамилия пользователя,
                'last_name',
                //email – email пользователя [можно запросить его верификацию],
                'email',
                //nickname – псевдоним пользователя,
                //        'nickname',
                //bdate – дата рождения в формате DD.MM.YYYY,
                'bdate',
                //sex – пол пользователя (0 – не определен, 1 – женский, 2 – мужской),
                'sex',
                //phone – телефон пользователя в цифровом формате без лишних символов,
                'phone',
                //photo – адрес квадратной аватарки (до 100*100),
                'photo',
                //photo_big – адрес самой большой аватарки, выдаваемой соц. сетью,
                'photo_big',
                //city – город,
                'city',
                //country – страна.
                'country',
            ],

            'providers'    => [
                'vkontakte',
                'google',
                'facebook',
                'odnoklassniki',
                'mailru',
                'instagram',
            ],
            'hidden'       => 'other',
        ],
        'cas' => [
            'host'    => env('LK2_CAS_SERVER_HOST'),
            'port'    => env('LK2_CAS_SERVER_PORT'),
            'uri'     => env('LK2_CAS_SERVER_URI'),
            'skip'    => (bool) env('LK2_CAS_SKIP'),
            'clients' => env('LK2_CAS_ALLOWED_CLIENTS'),
            'after'   => env('LK2_CAS_AFTER_LOGIN_URL', '/'),
            'log'     => env('LK2_CAS_LOG'),
        ],
    ],
];
