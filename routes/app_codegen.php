<?php

use \Larakit\Attach\ModelLkNgAttach;

\Larakit\Event\Event::listener('larakit:apps', function () {
    \Larakit\LkNgApp::register('codegen', '/codegen', 'CodeGen');
});
\Larakit\Event\Event::listener('larakit:menus', function () {
    $title = 'Главная';
    $icon  = 'fa fa-gears';
    $url   = '/codegen';
    $r     = \Larakit\LkNgSidebar::section('codegen', 'Codegen')
                                 ->item('codegen', $title, $icon, $url);
});

\Larakit\Event\Event::listener('larakit:init', function () {
    \Route::namespace('Larakit\Controllers')
          ->middleware('web', 'auth', 'role_admin')
          ->group(function () {
              \Route::get('/codegen', 'AppCodegenController@web_index')
                    ->name('api.codegen.web_index');
          });
    \Route::namespace('Larakit\Controllers')
          ->middleware('api')
          ->group(function () {
              \Route::get('/api/codegen/tables', 'AppCodegenController@tables')
                    ->name('api.codegen.tables');
          });
});
