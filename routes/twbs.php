<?php
Route::get('twbs', function () {
    $env = env('BREAKPOINTS');
    if ($env) {
        $breakpoints = explode(',', $env);
        $breakpoints = array_map('intval', $breakpoints);
        $breakpoints = array_unique($breakpoints);
    } else {
        $breakpoints = [
            1920,
            1440,
            1280,
            1024,
            768,
            410,
            360,
            320,
        ];
    }
    sort($breakpoints);
    $min = min($breakpoints);
    $max = max($breakpoints);
    //    dump($max);
    $sizes = [];
    $prev  = 0;
    foreach ($breakpoints as $breakpoint) {
        $item = [];
        if (!$prev) {
            $item['name'] = 'lower';
        } else {
            $item['name'] = $prev . '_' . ($breakpoint - 1);
        }

        if ($breakpoint == $max) {
            $item['min'] = $prev;
        } else {
            //            if ($prev) {
            $item['min'] = $prev;
            //            }
        }
        $item['max']        = $breakpoint - 1;
        $prev               = $breakpoint;
        $sizes[$breakpoint] = $item;
    }
    $sizes[] = [
        'min'  => $max,
        'name' => 'upper',
    ];
    foreach ($sizes as $breakpoint => $size) {
        $compare = '';
        $min     = \Illuminate\Support\Arr::get($size, 'min');
        $max     = \Illuminate\Support\Arr::get($size, 'max');
        if ($min) {
            $compare .= $min . 'px <= ';
        }
        $compare .= ' X ';
        if ($max) {
            $compare .= ' <= ' . $max . 'px';
        }
        $sizes[$breakpoint]['compare'] = $compare;
    }
    $prefixes         = [
        'block'        => '',
        'inline'       => '-inline',
        'inline-block' => '-inline-block',
    ];
    $css_display_none = [];
    $widths           = [
        1  => 8.33333333,
        2  => 16.66666667,
        3  => 25,
        4  => 33.33333333,
        5  => 41.66666667,
        6  => 50,
        7  => 58.33333333,
        8  => 66.66666667,
        9  => 75,
        10 => 83.33333333,
        11 => 91.66666667,
        12 => 100,
    ];
    foreach ($sizes as $size) {
        foreach ($prefixes as $prefix) {
            $name               = 'visible-' . \Illuminate\Support\Arr::get($size, 'name') . $prefix;
            $css_display_none[] = $name;
        }
    }
    $css_display_none = '.' . implode(', ' . PHP_EOL . '.', $css_display_none);
    //    dump($sizes, $prefixes, $breakpoints);
    $table = [];
    foreach ($sizes as $size) {
        $prefix              = '';
        $item                = [];
        $name                = \Illuminate\Support\Arr::get($size, 'name');
        $class               = $name . $prefix;
        $item['class']       = '.visible-' . $class;
        $item['breakpoints'] = [];
        foreach ($sizes as $_size) {
            $min     = \Illuminate\Support\Arr::get($_size, 'min');
            $max     = \Illuminate\Support\Arr::get($_size, 'max');
            $_name   = \Illuminate\Support\Arr::get($_size, 'name');
            $subname = '';
            if ($min) {
                $subname .= $min . 'px <=';
            }
            $subname .= ' W <= ';
            if ($max) {
                $subname .= $max . 'px';
            }
            $item['breakpoints'][$subname] = [
                'text'  => ($_name != $name) ? 'hidden' : 'visible',
                'color' => ($_name != $name) ? 'responsive-hidden' : 'responsive-visible',
            ];
        }
        $table[] = $item;
    }
    foreach ($sizes as $size) {
        $prefix              = '';
        $item                = [];
        $name                = \Illuminate\Support\Arr::get($size, 'name');
        $item['breakpoints'] = [];
        $class               = $name;
        $item['class']       = '.hidden-' . $class;
        foreach ($sizes as $_size) {
            $min     = \Illuminate\Support\Arr::get($_size, 'min');
            $max     = \Illuminate\Support\Arr::get($_size, 'max');
            $_name   = \Illuminate\Support\Arr::get($_size, 'name');
            $subname = '';
            if ($min) {
                $subname .= $min . 'px <=';
            }
            $subname .= ' W <= ';
            if ($max) {
                $subname .= $max . 'px';
            }
            $item['breakpoints'][$subname] = [
                'text'  => ($_name == $name) ? 'hidden' : 'visible',
                'color' => ($_name == $name) ? 'responsive-hidden' : 'responsive-visible',
            ];
        }
        $table[] = $item;
    }

    //    dd($table);

    return view('bootstrap', compact('breakpoints', 'min', 'max', 'css_display_none', 'sizes', 'table', 'widths', 'cols'));
});
