<?php
\Route::get('tags', function () {
    return \Larakit\Helpers\HelperOptions::get(\Larakit\Tags\Tag::orderBy('name')
                                                                ->get(), 'name');
});
