<?php
//$controller = 'SnippetController';
//\Larakit\CrudRoute::factory(\Larakit\Snippet\Snippet::class, $controller)
//    ->index()
//    ->options()
//    ->config()
//    ->create()
//    ->item()
//    ->itemUpdate();
$controller = 'SnippetGroupController';
\Larakit\CrudRoute::factory(\Larakit\Snippet\SnippetGroup::class, $controller)
    ->setRoutePrefix('/snippets')
    ->index()
    ->config()
    ->item()
    ->itemUpdate();
