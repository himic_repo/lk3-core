<?php

use \Larakit\Gallery\ModelLkNgGallery;

\Larakit\Event\Event::listener('larakit:init', function () {
    \Gate::policy(ModelLkNgGallery::class, \Larakit\Policies\AppGalleryPolicy::class);
    //регистрируем роуты для всех моделей с галереями
    foreach (\Larakit\Gallery\LkNgGallery::models() as $model_name) {
        $route_prefix      = \Larakit\CrudRoute::getRoutePrefix($model_name);
        $route_name_prefix = \Larakit\CrudRoute::getRouteNamePrefix($model_name);
        $model_param       = \Larakit\CrudRoute::getModelParam($model_name);
        \Route::prefix('/api' . $route_prefix . '/{' . $model_param . '}/galleries')
              ->namespace('Larakit\Controllers')
              ->middleware('api')
              ->group(function () use ($model_param, $route_name_prefix) {
                  \Route::post('/{block}/upload', 'AppGalleryController@blockUpload')
                        ->middleware('can:galleries,' . $model_param)
                        ->name($route_name_prefix . '.id.galleries.type.upload');
                  \Route::post('/{block}/clear', 'AppGalleryController@blockClear')
                        ->middleware('can:galleries,' . $model_param)
                        ->name($route_name_prefix . '.id.galleries.type.clear');
                  \Route::post('/{block}/dnd', 'AppGalleryController@blockDnd')
                        ->middleware('can:galleries,' . $model_param)
                        ->name($route_name_prefix . '.id.galleries.type.dnd');
              });
    }

    Route::prefix('/api/galleries/{ModelLkNgGallery}')
         ->namespace('Larakit\Controllers')
         ->middleware('api')
         ->group(function () {
             $controller = 'AppGalleryController';
             \Route::model('ModelLkNgGallery', ModelLkNgGallery::class);
             \Route::pattern('ModelLkNgGallery', '[0-9]+');

             \Route::post('/update', $controller . '@update')
                   ->middleware('can:manage,ModelLkNgGallery')
                   ->name('api.galleries.id.update');
             \Route::post('/delete', $controller . '@delete')
                   ->middleware('can:manage,ModelLkNgGallery')
                   ->name('api.galleries.id.delete');
         });
    \Larakit\Thumb\LkNgThumb::registerModel(ModelLkNgGallery::class, []);
});
