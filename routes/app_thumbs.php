<?php
\Larakit\Event\Event::listener('larakit:init', function () {
    foreach (\Larakit\Thumb\LkNgThumb::models() as $model_name) {
        $route_prefix      = \Larakit\CrudRoute::getRoutePrefix($model_name);
        $route_name_prefix = \Larakit\CrudRoute::getRouteNamePrefix($model_name);
        $model_param       = \Larakit\CrudRoute::getModelParam($model_name);
        \Route::prefix($route_prefix . '/{' . $model_param . '}/thumbs')
              ->namespace('Larakit\Controllers')
              ->middleware('api')
              ->group(function () use ($model_param, $route_name_prefix) {
                  \Route::any('/', 'AppThumbController@thumbs')
                        ->middleware('can:thumbs,' . $model_param)
                        ->name($route_name_prefix . '.id.thumbs');
                  \Route::any('/{type}/upload', 'AppThumbController@thumbsTypeUpload')
                        ->middleware('can:thumbs,' . $model_param)
                        ->name($route_name_prefix . '.id.thumbs.type.upload');
                  \Route::any('/{type}/clear', 'AppThumbController@thumbsTypeClear')
                        ->middleware('can:thumbs,' . $model_param)
                        ->name($route_name_prefix . '.id.thumbs.type.clear');
                  \Route::any('/{type}/{size}/crop', 'AppThumbController@thumbsTypeSizeCrop')
                        ->middleware('can:thumbs,' . $model_param)
                        ->name($route_name_prefix . '.id.thumbs.type.size.crop');
              });
    }
});
