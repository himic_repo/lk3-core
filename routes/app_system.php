<?php
\Route::prefix('/system')
     ->group(function () {
         $controller = 'SystemController';
         \Route::get('/apps', $controller . '@apps')
              ->name('api.system.apps');
         \Route::get('/menus', $controller . '@menus')
              ->name('api.system.menus');
         \Route::any('/locales', $controller . '@locales')
              ->name('api.system.locale');
         \Route::post('/locales/{locale}', $controller . '@localeSet')
              ->name('api.system.locale.set');
     });
