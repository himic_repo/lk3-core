<?php
//$controller = 'SnippetController';
//\Larakit\CrudRoute::factory(\Larakit\Snippet\Snippet::class, $controller)
//    ->index()
//    ->options()
//    ->config()
//    ->create()
//    ->item()
//    ->itemUpdate();
$controller = 'FileModelController';
\Larakit\CrudRoute::factory(\Larakit\Filemanager\FileModel::class, $controller)
    ->setRoutePrefix('/filemanager')
    ->index()
    ->config()
    ->itemUpdate();
