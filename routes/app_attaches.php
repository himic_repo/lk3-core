<?php

use \Larakit\Attach\ModelLkNgAttach;

\Larakit\Event\Event::listener('larakit:init', function () {
    if (!function_exists('return_bytes')) {
        function return_bytes($val) {
            $val = trim($val);

            if (is_numeric($val)) {
                return $val;
            }

            $last = strtolower($val[strlen($val) - 1]);
            $val  = substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

            switch ($last) {
                // The 'G' modifier is available since PHP 5.1.0
                case 'g':
                    $val *= 1024;
                case 'm':
                    $val *= 1024;
                case 'k':
                    $val *= 1024;
            }

            return $val;
        }
    }

    Route::any('api/upload_max_filesize', 'Larakit\Controllers\AppAttachController@upload_max_filesize');
    \Gate::policy(ModelLkNgAttach::class, \Larakit\Policies\AppAttachPolicy::class);
    //регистрируем роуты для всех моделей с аттачами
    foreach (\Larakit\Attach\LkNgAttach::models() as $model_name) {
        $route_prefix      = \Larakit\CrudRoute::getRoutePrefix($model_name);
        $route_name_prefix = \Larakit\CrudRoute::getRouteNamePrefix($model_name);
        $model_param       = \Larakit\CrudRoute::getModelParam($model_name);
        \Route::prefix('/api' . $route_prefix . '/{' . $model_param . '}/attaches')
              ->namespace('Larakit\Controllers')
              ->middleware('api')
              ->group(function () use ($model_param, $route_name_prefix) {
                  \Route::post('/{block}/upload', 'AppAttachController@blockUpload')
                        ->middleware('can:attaches,' . $model_param)
                        ->name($route_name_prefix . '.id.attaches.type.upload');
                  \Route::post('/{block}/dnd', 'AppAttachController@blockDnd')
                        ->middleware('can:attaches,' . $model_param)
                        ->name($route_name_prefix . '.id.attaches.type.dnd');
              });
    }

    \Route::prefix('/api/attaches/{ModelLkNgAttach}')
          ->namespace('Larakit\Controllers')
          ->middleware('api')
          ->group(function () {
              $controller = 'AppAttachController';
              \Route::model('ModelLkNgAttach', ModelLkNgAttach::class);
              \Route::pattern('ModelLkNgAttach', '[0-9]+');
              \Route::post('/update', $controller . '@update')
                    ->middleware('can:manage,ModelLkNgAttach')
                    ->name('api.attaches.id.update');
              \Route::post('/delete', $controller . '@delete')
                    ->middleware('can:manage,ModelLkNgAttach')
                    ->name('api.attaches.id.delete');
          });
    \Route::bind('ModelLkNgAttach_hash', function ($value) {
        $id = \Illuminate\Support\Arr::get(hashids_decode($value), 0);

        return $id ? ModelLkNgAttach::find($id) : abort(404);
    });

    \Route::namespace('Larakit\Controllers')
          ->middleware('api')
          ->group(function () {
              \Route::get('/api/download/{ModelLkNgAttach_hash}', 'AppAttachController@download')
                    ->middleware('can:download,ModelLkNgAttach_hash')
                    ->name('api.download.hash');
          });
});
