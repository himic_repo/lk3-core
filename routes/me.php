<?php
\Route::get('/me', function (\Request $request) {
    $user = Request::user();

    return $user ? new \App\Http\Resources\User($user) : ['data' => ['id' => 0]];
});
