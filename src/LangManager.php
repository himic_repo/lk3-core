<?php

namespace Larakit;

use Illuminate\Support\Arr;
use Larakit\Event\Event;

/**
 * Class LangManager
 * @package Larakit
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class LangManager {
    const KEY = 'current-lang';

    /**
     * Массив вида
     * [
     *      'ru'=>'Русский',
     *      'en'=>'English',
     * ]
     * @return array
     */
    static function getLocales() {
        $langs = config('app.locales');
        if (!$langs) {
            $langs = [
                self::getCurrent() => [
                    'locale'    => self::getCurrent(),
                    'direction' => 'ltr',
                    'name'      => self::getCurrent(),
                ],
            ];
        }
        $env = config('app.locales_used');
        if($env){
            $env = trim($env);
            $env = explode(',', $env);
            $env = array_map(function ($v){
                return trim($v);
            }, $env);
            $langs = Arr::only($langs, $env);
        }

        return $langs;
    }

    static function check($lang) {
        $langs = self::getLocales();

        return isset($langs[$lang]);
    }

    static function getLangByLocale($locale) {
        $lang = '';
        foreach (self::getLocales() as $k => $data) {
            if ($locale == Arr::get($data, 'locale')) {
                $lang = $k;
                break;
            }
        }

        return $lang;
    }

    /**
     * Установить текущий язык
     */
    static function setCurrentSlug($lang) {
        Event::notify('lkng.setlocale', $lang);
        //запоминаем выбранный язык на месяц
        \Cookie::queue(\Cookie::make(static::KEY, $lang, 60 * 24 * 30));
        $locales       = self::getLocales();
        self::$current = Arr::get($locales, $lang);
        //        dump($lang);
        //        dump($locales);
        //        dump(self::$current);
    }

    /**
     * Получить текущий язык
     */
    static function getCurrentSlug() {
        $lang = \Cookie::get(self::KEY);
        if (!$lang) {
            $lang = config('app.locale');
        }

        return $lang;
    }

    static $current = null;

    static function getCurrent() {
        if (is_null(self::$current)) {
            $locales       = self::getLocales();
            self::$current = Arr::get($locales, self::getCurrentSlug());
        }

        return self::$current;
    }

    static function getCurrentDirection() {
        return Arr::get(self::getCurrent(), 'direction');
    }

    static function getCurrentName() {
        return Arr::get(self::getCurrent(), 'name');
    }

    static function getCurrentLocale() {
        return Arr::get(self::getCurrent(), 'locale');
    }

}
