<?php

namespace Larakit\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Larakit\Attach\ModelLkNgAttach;

class AppAttachPolicy {
    use HandlesAuthorization;

    public function manage(User $me, ModelLkNgAttach $model) {
        return \Gate::allows('attaches', $model->attachable);
    }

    public function download(User $me, ModelLkNgAttach $model) {
        return \Gate::allows('download', $model->attachable);
    }

}
