<?php

namespace Larakit\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Larakit\Gallery\ModelLkNgGallery;

class AppGalleryPolicy {
    use HandlesAuthorization;

    public function manage(User $me, ModelLkNgGallery $model) {
        return $me->id && \Gate::allows('galleries', $model->galleriable);
    }

    public function thumbs(User $me, ModelLkNgGallery $model) {
        return $me->id && \Gate::allows('galleries', $model->galleriable);
    }

}
