<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 20.07.16
 * Time: 19:42
 */

namespace Larakit\FormFilter;

use Illuminate\Support\Arr;

class FilterIn extends Filter
{
    protected $data_type     = 'int';
    protected $group_by      = null;
    protected $data_not_null = false;

    function setDataNotNull()
    {
        $this->data_not_null = true;

        return $this;
    }

    function setDataTypeString()
    {
        $this->data_type = 'string';

        return $this;
    }

    function setTypeSelect2()
    {
        $this->type = 'select2';

        return $this;
    }

    function setTypeCheckbox()
    {
        $this->type = 'checkbox';

        return $this;
    }

    function setGroupBy($field)
    {
        $this->group_by = $field;

        return $this;
    }

    function setTypeButton()
    {
        $this->type = 'button';

        return $this;
    }

    function getType()
    {
        if (!$this->type) {
            return count($this->options) > 5 ? 'select2' : 'checkbox';
        }

        return $this->type;
    }

    function element()
    {
        $ret = parent::element();
        if ($this->group_by) {
            $ret['group_by'] = $this->group_by;
        }

        return $ret;
    }

    function getValues()
    {
        $value = (array) \Request::input('filters.' . $this->form_field);
        $ret   = [];
        if ('select2' == $this->getType()) {
            foreach ($value as $v) {
                $ret[] = Arr::get($v, 'id');
            }
        } else {
            foreach ($value as $k => $v) {
                if ($v && $k) {
                    $ret[] = $k;
                }
            }
        }
        if ('int' == $this->data_type) {
            $ret = array_map('intval', $ret);
            if ($this->data_not_null) {
                $ret = array_filter($ret, function ($v)
                {
                    return $v;
                });
            }
        }

        return $ret;
    }

    function query($model)
    {
        $values = $this->getValues();
        $values = array_unique($values);
        $values = array_filter($values, function ($k) {
            return $k;
        });
        if (count($values)) {
            if ($this->relation) {
                $model->whereHas($this->relation, function ($query) use ($values)
                {
                    $query->whereIn($this->db_field, $values);
                });
            } else {
                $table = $model->getModel()
                    ->getTable()
                ;
                $model->whereIn($table . '.' . $this->db_field, $values);
            }
        }
    }
}
