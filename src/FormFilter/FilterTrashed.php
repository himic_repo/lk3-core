<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 20.07.16
 * Time: 19:42
 */

namespace Larakit\FormFilter;

class FilterTrashed extends Filter {

    protected $no          = 'vendor.rtdl.rtdl-list-filter.soft_deletes.all';
    protected $yes         = 'vendor.rtdl.rtdl-list-filter.soft_deletes.deleted';
    protected $all         = 'vendor.rtdl.rtdl-list-filter.soft_deletes.actived';
    protected $type        = 'boolean';
    protected $is_location = true;

    /**
     * @param string $no
     *
     * @return FilterBoolean;
     */
    public function setNo($no) {
        $this->no = $no;

        return $this;
    }

    /**
     * @param string $yes
     *
     * @return FilterBoolean;
     */
    public function setYes($yes) {
        $this->yes = $yes;

        return $this;
    }

    /**
     * @param string $all
     *
     * @return FilterBoolean;
     */
    public function setAll($all) {
        $this->all = $all;

        return $this;
    }

    function element() {
        $element        = parent::element();
        $element['all'] = $this->all;
        $element['off'] = $this->no;
        $element['on']  = $this->yes;

        return $element;
    }

    function query($model) {
        if (\Request::has('filters.' . $this->form_field)) {
            $value = (int) \Request::input('filters.' . $this->form_field);
            if ($this->relation) {
                $model->whereHas($this->relation, function ($query) use ($value) {
                    switch ($value) {
                        case -1:
                            $query->withTrashed();
                            break;
                        case 1:
                            $query->withTrashed()
                                  ->where('deleted_at', '!=', null);
                            break;
                    }
                });
            } else {
                $table = $model->getModel()
                               ->getTable();
                switch ($value) {
                    case -1:
                        $model->withTrashed();
                        break;
                    case 1:
                        $model->withTrashed()
                              ->where($table . '.' . 'deleted_at', '!=', null);
                        break;
                }
            }
        }
    }
}
