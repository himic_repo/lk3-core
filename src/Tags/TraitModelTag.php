<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 14.06.17
 * Time: 14:10
 */

namespace Larakit\Tags;

use Larakit\Tags\Tag;

trait TraitModelTag {
    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
