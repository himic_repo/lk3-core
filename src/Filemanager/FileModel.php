<?php

namespace Larakit\Filemanager;

use Illuminate\Database\Eloquent\Model;
use Larakit\Tags\TraitModelTag;

class FileModel extends Model {
    use TraitModelTag;
    protected $table    = 'filemanager';
    protected $fillable = [
        'name',
        'desc',
        'size',
    ];
    protected $appends  = [
        'path',
        'is_exist',
    ];

    function getIsExistAttribute() {
        return file_exists($this->path);
    }

    function getPathAttribute() {
        return public_path($this->name);
    }

    function touch() {
        parent::touch();
        if (file_exists($this->path)) {
            $this->size = \File::size($this->path);
        } else {
            $this->size = 0;
        }
        $this->save();
    }
}
