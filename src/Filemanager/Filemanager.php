<?php

namespace Larakit\Filemanager;

use Illuminate\Support\Arr;

class Filemanager {
    static    $instance;
    protected $files = [];

    /**
     * @return Filemanager
     */
    public static function instance() {
        if (!static::$instance) {
            static::$instance = new Filemanager();
        }

        return static::$instance;
    }

    function add($file_name, $tags, $desc = null) {
        $tags                    = (array) $tags;
        $file_name               = trim($file_name, '/');
        $this->files[$file_name] = [
            'tags' => $tags,
            'desc' => $desc,
        ];

        return $this;
    }

    function all() {
        $ret = [];
        foreach ($this->files as $file_name => $file) {
            $ret[] = [
                'file' => $file_name,
                'tags' => (array) Arr::get($file, 'tags'),
                'desc' => Arr::get($file, 'desc'),
            ];
        }

        return $ret;
    }
}
