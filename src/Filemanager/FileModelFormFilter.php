<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 17.05.17
 * Time: 13:46
 */

namespace Larakit\Filemanager;

use Larakit\Filemanager\FileModel;
use Larakit\FormFilter\FilterIn;
use \Larakit\FormFilter\FormFilter;
use Larakit\FormFilter\FilterLike;
use Larakit\FormFilter\Sorter;
use Larakit\Helpers\HelperOptions;

class FileModelFormFilter extends FormFilter {

    protected $per_page = 20;

    static function classModel() {
        return FileModel::class;
    }

    function init() {
        $this->addFilter(FilterLike::factory('desc')
                                   ->label('Описание файла'));
        $this->addFilterTags(FileModel::class);
        $this->addSorter(Sorter::factory('name')
                               ->label('По имени файла'), 1, 1);
    }
}
