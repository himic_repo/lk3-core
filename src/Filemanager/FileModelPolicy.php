<?php

namespace Larakit\Filemanager;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FileModelPolicy {
    use HandlesAuthorization;

    public function index(User $me) {
        return true;
    }

    public function manage(User $me) {
        return (bool) $me->is_admin;
    }

    public function update(User $me, $model = null) {
        return $this->manage($me);
    }

}
