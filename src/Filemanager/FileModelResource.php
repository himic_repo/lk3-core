<?php

namespace Larakit\Filemanager;

use Larakit\Helpers\HelperText;
use \Larakit\Resource\JsonResource;

class FileModelResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function _toArray() {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'desc'        => $this->desc,
            'size'        => $this->size,
            'size_string' => HelperText::fileSize($this->size),
            'url_upload'  => route('api.file_models.id.update', ['FileModel' => $this->id]),
            'ext'         => \File::extension($this->name),
        ];
    }
}
