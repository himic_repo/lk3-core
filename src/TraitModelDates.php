<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 14.06.17
 * Time: 14:10
 */

namespace Larakit;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait TraitModelDates {

    public function setAttribute($key, $value){
        return parent::setAttribute($key, $value);
    }

    static function bootTraitModelTranslate() {
        static::addLarakitAppends('translate');
    }

}
