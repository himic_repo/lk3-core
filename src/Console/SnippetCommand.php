<?php

namespace Larakit\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\Snippet\Snippet;
use Larakit\Snippet\SnippetGroup;

class SnippetCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:snippet {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Инициализация сниппетов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dt = Carbon::now();
        sleep(1);
        $snippets     = \Larakit\Snippet\SnippetConstructor::instance();
        $rows         = [];
        $group_models = [];
        foreach ($snippets->groups() as $code => $label) {
            $row                = [];
            $group_model        = SnippetGroup::firstOrNew([
                'code' => $code,
            ]);
            $row[]              = $label;
            $row[]              = $group_model->id ? 'обновлен' : 'добавлен';
            $group_model->label = $label;
            $group_model->save();
            $group_model->touch();
            $rows[]              = $row;
            $group_models[$code] = $group_model;
        }
        //подчистим старые теги сниппетов
        \DB::table('taggables')
           ->where('taggable_type', 'Larakit\Snippet\SnippetGroup')
           ->delete();
        foreach ($snippets->tags() as $code => $tags) {
            $ids = [];
            foreach ($tags as $tag) {
                $tag_model = \Larakit\Tags\Tag::firstOrCreate([
                    'name' => $tag,
                ]);
                $ids[]     = $tag_model->id;
            }
            if (count($ids)) {
                $group_models[$code]->tags()
                                    ->sync($ids);
            }
        }
        echo PHP_EOL;
        $this->error(Str::upper('Инициализируем группы сниппетов'));
        $this->table([
            'Название группы сниппетов',
            'Состояние',
        ], $rows);
        $groups      = SnippetGroup::pluck('id', 'code')
                                   ->toArray();
        $group_names = SnippetGroup::pluck('label', 'code')
                                   ->toArray();

        $rows = [];
        $i    = 1;
        foreach ($snippets->snippets() as $snippet) {
            $row                 = [];
            $group               = Arr::get($snippet, 'group');
            $code                = Arr::get($snippet, 'code');
            $html                = Arr::get($snippet, 'html');
            $type                = Arr::get($snippet, 'type');
            $label               = Arr::get($snippet, 'label');
            $group_id            = Arr::get($groups, $group);
            $group_name          = Arr::get($group_names, $group);
            $snippet_model       = Snippet::firstOrNew([
                'group_id' => $group_id,
                'code'     => $code,
            ]);
            $snippet_model->type = $type;
            if (!$snippet_model->id || $this->option('reset')) {
                $snippet_model->html = $html;
            }
            $snippet_model->label = $label;
            $row[]                = $i;
            $row[]                = $group_name;
            $row[]                = $label;
            $row[]                = $type;
            $html                 = strip_tags($html);
            $html                 = str_replace(PHP_EOL, ' ', $html);
            $html                 = Str::limit($html, 50);
            $row[]                = $html;
            $row[]                = $snippet_model->id ? 'обновлен' : 'добавлен';
            $snippet_model->save();
            $snippet_model->touch();
            $rows[] = $row;
            $i++;
        }
        echo PHP_EOL;
        $this->error(Str::upper('Инициализируем сами сниппеты'));
        $this->table([
            '№',
            'Группа',
            'Название',
            'Тип',
            'Значение',
            'Состояние',
        ], $rows);
        $this->error(Str::upper('Подчищаемся от старых сниппетов и групп'));
        Snippet::where('updated_at', '<', $dt)
               ->delete();
        SnippetGroup::where('updated_at', '<', $dt)
                    ->delete();



        $ret = [];
        foreach (Snippet::get() as $snippet) {
            $ret[$snippet->group->code][$snippet->code] = $snippet->html;
        }
        $this->error(Str::upper('Делаем бэкапы'));
        $to_lang = config('app.fallback_locale');
        $path    = resource_path('lang/' . $to_lang . '/snippets.php');
        $path    = str_replace('\\', '/', $path);
        if (file_exists($path)) {
            $this->info('- файл есть');
            $to_backup = storage_path('backup_langs/' . date('Y/m/d/H_i_s') . '/' . $to_lang . '/snippets.php');
            $to_backup = str_replace('\\', '/', $to_backup);
            $this->info('- делаем бэкап'.$to_backup);
            $this->dir($to_backup);
            file_put_contents($to_backup, file_get_contents($path));
            $this->info('- готово');
        } else {
            $this->info('- файла нет');
        }
        $this->error(Str::upper('Сохраняем сниппеты в языковой файл'));
        file_put_contents($path, '<?php' . PHP_EOL . 'return ' . var_export($ret, true) . ';');
        $this->info('- готово');
        \Artisan::call('larakit:lang');
    }

    function dir($file) {
        $dir = dirname($file);
        $dir = str_replace('\\', '/', $dir);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }

}
