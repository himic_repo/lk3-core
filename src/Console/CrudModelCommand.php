<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudModelCommand extends CrudCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:model {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация модели';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->crudInit();
        $this->header();
        $this->save('Класс модели существует, перезаписать?');
        if ($this->confirm('Создать миграцию',$this->autoreplace)) {
            \Artisan::call('make:migration', [
                'name'     => 'create_' . $this->table_name . '_table',
                '--create' => $this->table_name,
            ]);
        }
    }

    function content()
    {
        return (string) $this->parseTemplate('Model');
    }

    function path()
    {
        return app_path($this->model . '.php');
    }

}
