<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация модуля';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    protected $autoreplace = true;
    //"AdvType"
    protected $model;
    //advtype
    protected $model_lower;
    //Advtype
    protected $model_first_upper;
    //"adv_types"
    protected $table_name;
    //"adv-types"
    protected $slug;
    //"admin"
    protected $app;
    //"Admin"
    protected $app_studly;
    protected $namespace;

    protected function crudInit() {
        if ($this->hasArgument('model')) {
            //"AdvType"
            $this->model             = Str::studly($this->argument('model'));
            $this->model_lower       = Str::lower($this->model);
            $this->model_first_upper = Str::ucfirst($this->model_lower);
            //dump($this->model);
            //"adv_types"
            $this->table_name = Str::snake(Str::plural($this->model));
            //dump($this->table_name);
            //"adv-types"
            $this->slug = Str::snake(Str::plural($this->model), '-');
            //dump($this->slug);
        }
        if ($this->hasArgument('app')) {
            //"admin"
            $this->app = Str::lower($this->argument('app'));
            //dump($this->app);
            //"Admin"
            $this->app_studly = Str::studly($this->app);
            //dump($this->app_studly);
        }
        $this->namespace = trim(\App::getNamespace(), '\\');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->call('larakit:crud:model', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:form-filter', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:controller', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:resource', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:resource-collection', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:policy', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:thumb', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:validator', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:ng', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
        $this->call('larakit:crud:routes', [
            'model' => $this->model,
            'app'   => $this->app,
        ]);
    }

    function makeClassFullName($group, $suffix, $use_app = true) {
        return $this->makeClassNamespace($group, $use_app) . '\\' . $this->makeClassName($suffix);
    }

    function makeClassName($suffix) {
        return $this->model . $suffix;
    }

    function makeClassNamespace($group, $use_app = true) {
        return trim(\App::getNamespace() . $group . ($use_app && $this->app_studly ? '\\' . $this->app_studly : ''), '\\');
    }

    function makeClassPath($group = null, $suffix = null, $use_app = true) {
        return app_path(($group ? (str_replace('\\', '/', $group) . '/') : '') . ($use_app && $this->app_studly ? ($this->app_studly . '/') : '') . $this->makeClassName($suffix) . '.php');
    }

    function save($exist_mesage) {
        $path = $this->path();
        $this->warn($path);
        $dir = dirname($path);
        if (!file_exists($path) || $this->confirm($exist_mesage, $this->autoreplace)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($this->path(), $this->content());
            $this->warn('Файл ' . str_replace(base_path(), '', $this->path()) . ' создан');
        }
    }

    function parseTemplate($template) {
        return view('larakit-stubs::' . $template, [
            'model'             => $this->model,
            'model_lower'       => $this->model_lower,
            'model_first_upper' => $this->model_first_upper,
            'table_name'        => $this->table_name,
            'slug'              => $this->slug,
            'app'               => $this->app,
            'thumb'             => (bool) $this->hasOption('thumb'),
            'app_studly'        => $this->app_studly,
            'namespace'         => $this->namespace,
        ]);
    }

    function header() {
        $this->error(str_repeat('#', 50));
        $this->error(str_pad($this->description, 50, '.', STR_PAD_BOTH));
        $this->error(str_repeat('#', 50));
    }

}
