<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudValidatorCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:validator {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация валидатора';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс валидатора существует, перезаписать?');
    }

    function content() {
        return (string) $this->parseTemplate('Validator');
    }

    function path() {
        return $this->makeClassPath('Validators', 'Validator', false);
    }

}
