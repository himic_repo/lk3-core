<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudFormFilterCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:form-filter {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация форм-фильтра';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс фильтра существует, перезаписать?');
    }

    function content() {
        return (string) $this->parseTemplate('FormFilter');
    }

    function path() {
        return $this->makeClassPath('FormFilters', 'FormFilter');
    }

}
