<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\Helpers\HelperArr;
use Larakit\LangManager;

class LangFileGuiCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:lang-gui';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация файла переводов интерфейса';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    function build($path) {
        $gui  = [];
        $path = str_replace('\\', '/', $path);
        $name = resource_path('lang_source') . '/';
        $name = str_replace('\\', '/', $name);
        $name = str_replace($name, '', $path);
        $dest = resource_path('lang/' . config('app.fallback_locale') . '/' . $name . '.php');
        $dest = str_replace('\\', '/', $dest);
        $this->warn(Str::upper('# ./' . str_replace(str_replace('\\', '/', resource_path()) . '/', '', $dest)));
        $files = rglob('*.php', 0, $path);
        $cnt   = 0;
        foreach ($files as $file) {
            $cnt++;
            $file    = str_replace('\\', '/', $file);
            $key     = str_replace([$path . '/', '.php', '/'], ['', '', '.'], $file);
            $content = include $file;
            Arr::set($gui, $key, $content);
        }
        $this->info('- найдено файлов: ' . $cnt);
        $this->info('- создано ключевых фраз: ' . count(HelperArr::flatten($gui)));
        $backup = storage_path('backup_langs/' . $this->timestamp . '/' . $name . '.php');
        $backup = str_replace('\\', '/', $backup);
        //бэкапим старый файл переводов
        if (file_exists($dest)) {
            $this->dir($backup);
            $this->info('- сделан бэкап языкового файла');
            file_put_contents($backup, file_get_contents($dest));
        }
        $this->dir($dest);
        //записываем новый файл переводов
        file_put_contents($dest, '<?php' . PHP_EOL . 'return ' . var_export($gui, true) . ';');
        //        dump($dest, $backup);
        //        dd($files, $gui);
        echo PHP_EOL;
        echo PHP_EOL;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo PHP_EOL;
        $this->timestamp = date('Y/m/d/H_i_s');
        $this->error('Производим сборку файлов перевода интерфейса');
        $path = resource_path('lang_source');
        $path = str_replace('\\', '/', $path);
        foreach (\File::directories($path) as $dir) {
            $this->build($dir);
        }
    }

    function dir($file) {
        $dir = dirname($file);
        $dir = str_replace('\\', '/', $dir);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
