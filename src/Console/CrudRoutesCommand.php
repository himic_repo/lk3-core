<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudRoutesCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:routes {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация роутов и middleware';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс middleware существует, перезаписать?');
        $this->routes_web();
        $this->routes_api();
        $this->inject();
    }

    function inject() {
        $path = app_path('Http/Kernel.php');
        $dir  = dirname($path);
        $content = file_get_contents($path);
        //а теперь добавим в роутинг страницу
        $string = "\t\t".'\'role_'.$this->app.'\'      => \\'.$this->namespace.'\Http\Middleware\Role'.$this->app_studly.'::class,';
        if (!mb_strpos($content, 'Role'.$this->app_studly.'.php')) {
            $content_page = $this->parseTemplate('ng-app-page');
            $content      = str_replace(
                'protected $routeMiddleware = [' . PHP_EOL,
                'protected $routeMiddleware = [' . PHP_EOL . $string . PHP_EOL,
                $content
            );
            file_put_contents($path, $content);
        }

    }
    function content() {
        return (string) $this->parseTemplate('Middleware');
    }

    function path() {
        return app_path('Http/Middleware/Role'.$this->app_studly.'.php');
    }

    function routes_web()
    {
        $path = base_path('routes/web/ng-' . $this->app . '.php');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл routes/web.php уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('routes_web');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }


    function routes_api()
    {
        $path = base_path('routes/api/' . $this->table_name . '.php');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл routes/api.php уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('routes_api');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

}
