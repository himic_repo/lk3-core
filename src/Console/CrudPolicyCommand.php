<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudPolicyCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:policy {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация модели';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс политики существует, перезаписать?');
        if($this->confirm('Зарегистрировать политику?',$this->autoreplace)){
            $file = app_path('Providers/AuthServiceProvider.php');
            $content = file_get_contents($file);
            $class_policy = $this->makeClassFullName('Policies', 'Policy', false);
            $class_model = $this->makeClassFullName('', '', false);
            if(!mb_strpos($content, $class_policy)){
                $start = 'protected $policies = [';
                $content = str_replace($start,$start.PHP_EOL."\t\t".'\''.$class_model.'\'=>\''.$class_policy.'\',', $content);
                file_put_contents($file, $content);
                $this->info('Политика зарегистрирована');
            }
        }
    }

    function content() {
        return (string) $this->parseTemplate('Policy');
    }

    function path() {
        return $this->makeClassPath('Policies', 'Policy', false);
    }

}
