<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\LangManager;
use Larakit\LkLangExporter;

class LangFileContentCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:lang-content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация файла переводов контента';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo PHP_EOL;
        $this->error('Производим сборку переводов контент-файла');
        $content = [];
        foreach (LkLangExporter::all() as $model_name => $attributes) {
            foreach ($model_name::all() as $item) {
                $content[$item->getTable()][$item->id] = Arr::only($item->toArray(), $attributes);
            }
        }
        foreach (LkLangExporter::keys() as $key => $original) {
            Arr::set($content, $key, $original);
        }
        $suffix = config('larakit.lang_content_prefix');
        $suffix = $suffix ? '_' . $suffix : '';
        $dest   = resource_path('lang/' . config('app.fallback_locale') . '/content' . $suffix . '.php');
        $dest   = str_replace('\\', '/', $dest);
        $this->warn(Str::upper('# ./' . str_replace(resource_path() . '/', '', $dest)));
        $backup = storage_path('backup_langs/' . date('Y/m/d/H_i_s') . '/content' . $suffix . '.php');
        $backup = str_replace('\\', '/', $backup);
        //бэкапим старый файл переводов
        if (file_exists($dest)) {
            $this->dir($backup);
            file_put_contents($backup, file_get_contents($dest));
        }
        $this->dir($dest);
        //записываем новый файл переводов
        file_put_contents($dest, '<?php' . PHP_EOL . 'return ' . var_export($content, true) . ';');
        $this->info('Готово');
        echo PHP_EOL;
        echo PHP_EOL;
    }

    function dir($file) {
        $dir = dirname($file);
        $dir = str_replace('\\', '/', $dir);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
