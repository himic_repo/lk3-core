<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudControllerCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:controller {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация контроллера';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс контроллера существует, перезаписать?');
    }

    function content() {
        return (string) $this->parseTemplate('Controller');
    }

    function path() {
        return $this->makeClassPath('Http\Controllers\Api', 'Controller');
    }

}
