<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Larakit\LangManager;

class LangFileCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:lang';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Комплексная работа с переводами';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        \Artisan::call('larakit:lang-gui', [], $this->output);
        if ('production' != config('app.env')) {
            \Artisan::call('larakit:lang-gui-translate', [], $this->output);
        }
        \Artisan::call('larakit:lang-content', [], $this->output);
        if ('production' != config('app.env')) {
            \Artisan::call('larakit:lang-content-translate', [], $this->output);
        }
        \Artisan::call('larakit:lang-deploy', [], $this->output);
    }

}
