<?php

namespace Larakit\Console;

use File;
use Illuminate\Console\Command;

class VueCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'larakit:vue {app=public : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда проверки версии файлов.';

    protected $app = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->app = $this->argument('app');
        $this->replaceContentWebpackMix();
        $this->createWebpackMixJs();
        $this->cloneApp();
        $this->replacePackageJson();
    }

    function replacePackageJson() {
        $file = base_path('package.json');
        $this->error($file);
        $this->info('Исправлены вызовы скриптов NPM RUN');

        $content                                   = file_get_contents($file);
        $content                                   = json_decode($content, true, 512, JSON_OBJECT_AS_ARRAY);
        $suffix                                    = $this->app == 'public' ? '' : '-' . $this->app;
        $content['scripts']['prod' . $suffix]         = 'npm run production' . $suffix;
        $content['scripts']['dev' . $suffix]         = 'npm run development' . $suffix;
        $content['scripts']['development' . $suffix] = 'cross-env process.env.section=' . $this->app . ' NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js';
        $content['scripts']['production' . $suffix] = 'cross-env process.env.section=' . $this->app . ' NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js';
        $content['scripts']['watch' . $suffix]       = 'npm run development' . $suffix . ' -- --watch';
        $content['scripts']['watch-poll' . $suffix]  = 'npm run watch' . $suffix . ' -- --watch-poll';
        ksort($content['scripts']);
        $content = json_encode($content, JSON_PRETTY_PRINT);
        file_put_contents($file, $content);

    }

    function cloneApp() {
        $this->error(resource_path('js'));
        $this->info('Скопирован в ' . resource_path('vue/' . $this->app));
        \Illuminate\Support\Facades\File::copyDirectory(resource_path('js'), resource_path('vue/' . $this->app));
        $file = resource_path('vue/' . $this->app . '/' . $this->app . '.js');
        rename(resource_path('vue/' . $this->app . '/app.js'), $file);
        $content = file_get_contents($file);
        $content = str_replace([
            '// const files = require.context(\'./\', true, /\.vue$/i);',
        ], [
            'const files = require.context(\'./\', true, /\.vue$/i);',
        ], $content);
        $content = str_replace([
            '// files.keys().map(key => Vue.component(key.split(\'/\').pop().split(\'.\')[0], files(key).default));',
        ], [
            'files.keys().map(key => Vue.component(key.split(\'/\').pop().split(\'.\')[0], files(key).default));',
        ], $content);
        $content = str_replace([
            'Vue.component(\'example-component\', require(\'./components/ExampleComponent.vue\').default);',
        ], [
            '//Vue.component(\'example-component\', require(\'./components/ExampleComponent.vue\').default);',
        ], $content);
        file_put_contents($file, $content);

        $this->error(resource_path('sass'));
        $this->info('Скопирован в ' . resource_path('vue/' . $this->app));
        \Illuminate\Support\Facades\File::copyDirectory(resource_path('sass'), resource_path('vue/' . $this->app));
        rename(resource_path('vue/' . $this->app . '/app.scss'), resource_path('vue/' . $this->app . '/' . $this->app . '.scss'));
    }

    function replaceContentWebpackMix() {
        $content = 'if (process.env.section) {
    require(`${__dirname}/webpack.mix._${process.env.section}.js`);
}
';
        file_put_contents(base_path('webpack.mix.js'), $content);
    }

    function createWebpackMixJs() {
        $this->save(base_path('webpack.mix._' . $this->app . '.js'), $this->parseTemplate('vue-webpack'), true);
    }

    function parseTemplate($template) {
        return view('larakit-stubs::' . $template, [
            'name' => $this->app,
        ]);
    }

    function save($file, $content, $is_force = false) {
        $is_exist = file_exists($file);
        switch (true) {
            case $is_exist && !$is_force:
                $this->error($file);
                if (!$this->confirm('Файл существует. Перезаписать?')) {
                    return false;
                }
                break;
            case $is_exist && $is_force:
                $this->error($file);
                $this->info('Файл существует и будет принудительно перезаписан');
                break;
            case !$is_exist :
                $this->error($file);
                $this->info('Файл не существует и будет создан');
                break;
        }
        file_put_contents($file, $content);

    }

}
