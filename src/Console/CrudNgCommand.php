<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Larakit\Event\Event;

class CrudNgCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:ng {model : Название модели} {app : Название приложения} {--thumb : Нужна тумба?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация NG-приложения';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->gulp();
        $this->gulpfile();
        $this->bootstrap_sidebar();
        $this->bootstrap_apps();
        $this->app();
        $this->lang_sidebar();
        $this->lang_public_list();
        $this->lang_public_modal();
        $this->pulic_page_js();
        $this->pulic_page_html();
        $this->pulic_modal_js();
        $this->pulic_modal_html();
        $this->lang_app_names();
    }

    function pulic_modal_js() {
        $path = public_path('!/apps/' . $this->app . '/modals/modal' . $this->app_studly . $this->model_first_upper  . '/component.js');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл component.js для модалки уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('modal-js');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function pulic_modal_html() {
        $path = public_path('!/apps/' . $this->app . '/modals/modal' . $this->app_studly . $this->model_first_upper  . '/component.html');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл component.html для модалки уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('modal-html');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function pulic_page_js() {
        $path = public_path('!/apps/' . $this->app . '/pages/page' . $this->app_studly . $this->model_first_upper . '/component.js');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл component.js для списка уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('page-js');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function pulic_page_html() {
        $path = public_path('!/apps/' . $this->app . '/pages/page' . $this->app_studly . $this->model . '/component.html');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('Файл component.html для списка уже существует, перезаписать?', $this->autoreplace)) {
            $content = $this->parseTemplate('page-html');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function app() {
        $path = public_path('!/apps/' . $this->app . '/app.js');
        $dir  = dirname($path);
        if (!file_exists($path) || $this->confirm('app.js уже существует, перезаписать?', 0)) {
            $content = $this->parseTemplate('ng-app');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        } else {
            $content = file_get_contents($path);
        }
        //а теперь добавим в роутинг страницу
        $string = '.when(\'/' . $this->app . '/' . $this->slug . '\',';
        if (!mb_strpos($content, $string)) {
            $content_page = $this->parseTemplate('ng-app-page');
            $content      = str_replace('/*replace*/' . PHP_EOL, $content_page . PHP_EOL.'/*replace*/' . PHP_EOL , $content);
        }
        $string = '.otherwise(';
        if (!mb_strpos($content, $string)) {
            $content_otherwise = '                .otherwise(\'/' . $this->app . '/' . $this->slug . '\')';
            $content           = str_replace('$routeProvider' . PHP_EOL, '$routeProvider' . PHP_EOL . $content_otherwise . PHP_EOL, $content);
        }
        file_put_contents($path, $content);
    }

    function gulpfile() {
        $path    = base_path('gulpfile.js');
        $string  = 'lkGulp.registerApp(\'' . $this->app . '\', require(\'./gulp/apps/' . $this->app . '.json\'));';
        $content = file_get_contents($path);
        if (!mb_strpos($content, $string)) {
            $content = str_replace('lkGulp.run();', $string . PHP_EOL . 'lkGulp.run();', $content);
        }
        file_put_contents($path, $content);
        $this->warn('Задача ' . $this->app . ' в ' . str_replace(base_path(), '', $path) . ' создана');

    }

    function gulp() {
        $path    = base_path('gulp/apps/' . $this->app . '.json');
        $dir     = dirname($path);
        $content = $this->parseTemplate('ng-gulp');
        if (!file_exists($path) || $this->confirm('Gulp-JSON уже существует, перезаписать?', $this->autoreplace)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function lang_public_list() {
        $path    = resource_path('lang_gui/apps/' . $this->app . '/page' . $this->model . '.php');
        $dir     = dirname($path);
        $content = $this->parseTemplate('lang_public_list');
        if (!file_exists($path) || $this->confirm('Языковой файл списка уже существует, перезаписать?', $this->autoreplace)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function lang_public_modal() {
        $path    = resource_path('lang_gui/apps/' . $this->app . '/modal' . $this->model . '.php');
        $dir     = dirname($path);
        $content = $this->parseTemplate('lang_public_modal');
        if (!file_exists($path) || $this->confirm('Языковой файл формы уже существует, перезаписать?', $this->autoreplace)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function lang_sidebar() {
        //admin.apps.moder.sidebar.user
        $path    = resource_path('lang_gui/apps/' . $this->app . '/sidebar.php');
        $dir     = dirname($path);
        $content = [];
        if (file_exists($path)) {
            $content = include $path;
        }
        if (!\Arr::get($content, $this->table_name)) {
            $content[$this->table_name] = $this->table_name;
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, '<?php' . PHP_EOL . 'return ' . var_export($content, 1) . ';');
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' исправлен');
        }
    }
    function lang_app_names() {
        //admin.apps.moder.sidebar.user
        $path    = resource_path('lang_gui/app_names.php');
        $content = [];
        if (file_exists($path)) {
            $content = include $path;
        }
        if (!\Arr::get($content, $this->app)) {
            $content[$this->app] = 'Application '.$this->app;
            file_put_contents($path, '<?php' . PHP_EOL . 'return ' . var_export($content, 1) . ';');
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' исправлен');
        }
    }

    function bootstrap_sidebar() {
        $path    = base_path('bootstrap/ng/' . $this->app . '/' . $this->app . '-' . $this->slug . '.php');
        $dir     = dirname($path);
        $content = $this->parseTemplate('ng-bootstrap-sidebar');
        if (!file_exists($path) || $this->confirm('Файл для sidebar уже существует, перезаписать?', $this->autoreplace)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($path, $content);
            $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
        }
    }

    function bootstrap_apps() {
        $path   = base_path('bootstrap/ng/apps.php');
        $dir    = dirname($path);
        $string = '\Larakit\LkNgApp::register(\'' . $this->app . '\', \'/' . $this->app . '\', \'gui.app_names.' . $this->app . '\');';
        if (!file_exists($path)) {
            $content = '<?php' . PHP_EOL . $string;
        } else {
            $content = file_get_contents($path);
            if (!mb_strpos($content, $string)) {
                $content .= PHP_EOL . $string;
            }
        }
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents($path, $content);
        $this->warn('Файл ' . str_replace(base_path(), '', $path) . ' создан');
    }

}
