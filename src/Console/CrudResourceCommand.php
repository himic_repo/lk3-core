<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudResourceCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:resource {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация ресурса';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс ресурса существует, перезаписать?');
    }

    function content() {
        return (string) $this->parseTemplate('Resource');
    }

    function path() {
        return $this->makeClassPath('Http\Resources', '', false);
    }

}
