<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\LangManager;

class LangFileDeployCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:lang-deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Деплой файлов перевода в public';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->error('Выкладываем языковые файлы для Angular-приложений');
        $suffix = config('larakit.lang_content_prefix');
        $suffix = $suffix ? '_' . $suffix : '';
        //если есть проектный языковой суффикс
        if ($suffix) {
            foreach (LangManager::getLocales() as $data) {
                $lang = Arr::get($data, 'locale');
                $dest = resource_path('lang/' . $lang . '/content.php');
                $dest = str_replace('\\', '/', $dest);
                $source = resource_path('lang/' . $lang . '/content'.$suffix.'.php');
                $source = str_replace('\\', '/', $source);
                if(file_exists($source)){
                    $source = file_get_contents($source);
                    file_put_contents($dest, $source);
                }
            }
        }

        foreach (LangManager::getLocales() as $data) {
            $lang = Arr::get($data, 'locale');
            $this->warn(Str::upper('# /!/locales/' . $lang . '.json'));
            $ret  = [];
            $path = public_path('/!/locales/' . $lang . '.json');
            $path = str_replace('\\', '/', $path);
            $this->dir($path);
            $locale = Arr::get($data, 'locale');
            $files  = rglob('*', 0, resource_path('lang/' . $locale) . '/');
            if (!count($files)) {
                $this->info('- нет файлов переводов');
            } else {
                foreach ($files as $file) {
                    $this->info('- ./' . str_replace(resource_path() . '/', '', $file));
                    $file    = str_replace('\\', '/', $file);
                    $replace = resource_path('lang/' . $locale);
                    $replace = str_replace('\\', '/', $replace);
                    $file    = str_replace($replace, '', $file);
                    $file    = str_replace('.php', '', $file);
                    $file    = trim($file, '/');
                    $file    = str_replace('/', '.', $file);
                    if ($file) {
                        if ('content' == mb_substr($file, 0, 7)) {
                            if ('content' == $file) {
                                Arr::set($ret, 'content', \Lang::get($file, [], $locale, false));
                            }
                        } else {
                            Arr::set($ret, $file, \Lang::get($file, [], $locale, false));
                        }
                    }
                }
                file_put_contents($path, json_encode($ret, JSON_PRETTY_PRINT));
                chmod($path, 0777);
            }
        }
        $this->warn('Языковые файлы сформированы');
        echo PHP_EOL;
        echo PHP_EOL;
    }

    function dir($file) {
        $dir = dirname($file);
        $dir = str_replace('\\', '/', $dir);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
