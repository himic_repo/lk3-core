<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudThumbCommand extends CrudCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:crud:thumb {model : Название модели} {app : Название приложения}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация тумбы';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->crudInit();
        $this->header();
        $this->save('Класс тумбы существует, перезаписать?');
        $this->lang();
    }

    function content() {
        return (string) $this->parseTemplate('Thumb');
    }

    function path() {
        return $this->makeClassPath('Thumbs', 'Thumb', false);
    }

    function lang(){
        $path = resource_path('lang_gui/thumbs/'.$this->model.'.php');
        $this->warn($path);
        $dir = dirname($path);
        if (!file_exists($path) || $this->confirm('Языковой файл тумбы существует, перезаписать?',$this->autoreplace)) {
            if(!file_exists($dir)){
                mkdir($dir, 0777, true);
            }
            $content = (string) $this->parseTemplate('lang_thumb');
            file_put_contents($path, $content);
            $this->info('Файл '.str_replace(base_path(), '', $path).' создан');
        }

    }

}
