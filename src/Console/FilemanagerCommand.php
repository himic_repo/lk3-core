<?php

namespace Larakit\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\Filemanager\Filemanager;
use Larakit\Filemanager\FileModel;
use Larakit\Snippet\Snippet;
use Larakit\Snippet\SnippetGroup;

class FilemanagerCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:filemanager';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Инициализация файлового хранилища';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dt = Carbon::now();
        sleep(1);
        //1) получить список файлов
        $files = Filemanager::instance()
                                       ->all();
        //подчистим старые теги файлов
        \DB::table('taggables')
           ->where('taggable_type', 'Larakit\Filemanager\FileModel')
           ->delete();

        $rows  = [];
        $paths = [];
        foreach ($files as $file) {
            $row         = [];
            $name        = Arr::get($file, 'file');
            $tags        = Arr::get($file, 'tags');
            $desc        = Arr::get($file, 'desc');
            $model       = FileModel::firstOrNew([
                'name' => $name,
            ]);
            $row[]       = $model->is_exist ? '+' : '-';
            $row[]       = $name;
            $row[]       = implode(', ', $tags);
            $row[]       = Str::limit($desc, 77);
            $model->name = $name;
            $model->desc = $desc;
            $model->save();
            $model->touch();
            $rows[] = $row;
            $ids    = [];
            foreach ($tags as $tag) {
                $tag_model = \Larakit\Tags\Tag::firstOrCreate([
                    'name' => $tag,
                ]);
                $ids[]     = $tag_model->id;
            }
            if (count($ids)) {
                $model->tags()
                      ->sync($ids);
            }
            $paths [] = '/public/' . $name;
        }
        $this->table([
            '#',
            'Путь к файлу',
            'Метки',
            'Описание',
        ], $rows);

        FileModel::where('updated_at', '<', $dt)
                 ->delete();
        $gitignore_file = base_path('.gitignore');
        $gitignore      = file_get_contents($gitignore_file);
        $gitignore      = explode(PHP_EOL, $gitignore);
        foreach ($paths as $path) {
            if (!in_array($path, $gitignore)) {
                $gitignore[] = $path;
            }
        }
        file_put_contents($gitignore_file, implode(PHP_EOL, $gitignore));
    }

}
