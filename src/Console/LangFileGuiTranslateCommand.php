<?php

namespace Larakit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\LangManager;
use Larakit\LkLangExporter;
use Larakit\YaTranslate;

class LangFileGuiTranslateCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:lang-gui-translate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Перевод сгенерированного файла интерфейса';

    protected $to_lang = 'en-US';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }


    function translate($from_dest) {
        $from_dest = str_replace('\\', '/', $from_dest);
        $name = resource_path('lang/' . config('app.fallback_locale')) . '/';
        $name = str_replace('\\', '/', $name);
        $name = str_replace($name, '', $from_dest);
        $name = mb_substr($name, 0, -4);
        $this->warn(Str::upper('# ./' . str_replace(resource_path() . '/', '', $from_dest)));
        $to_dest = resource_path('lang/' . $this->to_lang . '/' . $name . '.php');
        $to_dest = str_replace('\\', '/', $to_dest);
        if (file_exists($to_dest)) {
            $this->info('- файл есть');
            $this->info('- делаем бэкап');
            $to_backup = storage_path('backup_langs/' . date('Y/m/d/H_i_s') . '/' . $this->to_lang . '/' . $name . '.php');
            $to_backup = str_replace('\\', '/', $to_backup);
            $this->dir($to_backup);
            file_put_contents($to_backup, file_get_contents($to_dest));
            $this->info('- готово');
            $to_data = include $to_dest;
        } else {
            $this->info('- файла нет');
            $to_data = [];
        }
        $from_data = include $from_dest;
        $this->info('Начинаем перевод оригинального файла');
        $rows = [];
        $i    = 0;
        foreach (Arr::dot($from_data) as $k => $v) {
            $row      = [];
            $row[]    = $name . '.' . $k;
            $to_value = Arr::get($to_data, $k);
            $row[]    = Str::limit($v, 32);
            if (!$to_value) {
                $translate = YaTranslate::translate($v);
                Arr::set($to_data, $k, $translate);
                $this->dir($to_dest);
                file_put_contents($to_dest, '<?php' . PHP_EOL . 'return ' . var_export($to_data, true) . ';');
                $row[] = 'Яндекс.Переводы';
                $row[] = Str::limit($translate, 32);
                echo '+';
            } else {
                $row[] = 'Кэш';
                $row[] = Str::limit($v, 32);
                echo '-';
            }
            $rows[] = $row;
            $i++;
        }
        echo PHP_EOL;
        $this->table(['Ключ', 'Оригинал на ' . config('app.fallback_locale'), 'Откуда взято', 'Перевод на ' . $this->to_lang], $rows);
        echo PHP_EOL;
        echo PHP_EOL;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo PHP_EOL;
        $this->error('Производим перевод интерфейса');
        $path = resource_path('lang/' . config('app.fallback_locale'));
        $path = str_replace('\\', '/', $path);
        foreach (rglob('*.php', 0, $path) as $file) {
            $this->translate($file);
        }
    }

    function dir($file) {
        $dir = dirname($file);
        $dir = str_replace('\\', '/', $dir);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}
