<?php

namespace Larakit;

use Illuminate\Support\Arr;

/**
 * Class LkLangExporter
 * @package Lk2
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class LkLangExporter {
    protected static $items = [];
    protected static $keys  = [];

    static function all() {
        return static::$items;
    }

    static function keys() {
        return static::$keys;
    }

    static function register($model_name, $attributes) {
        static::$items[$model_name] = $attributes;
    }

    static function registerKey($key, $original) {
        static::$keys[$key] = $original;
    }

    static function modelAttributes($model_name) {
        return Arr::get(static::$items, $model_name, []);
    }

}
//'content.formats.audio'
