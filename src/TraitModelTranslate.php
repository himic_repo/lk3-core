<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 14.06.17
 * Time: 14:10
 */

namespace Larakit;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait TraitModelTranslate {

    function getTranslateAttribute() {
        $fields = Arr::get(LkLangExporter::all(), static::class);
        $ret    = [];
        foreach ((array)$fields as $field) {
            $ret[$field] = __('content.' . $this->table . '.' . $this->id . '.' . $field);
        };

        return $ret;
    }

    static function bootTraitModelTranslate() {
        static::addLarakitAppends('translate');
    }

}
