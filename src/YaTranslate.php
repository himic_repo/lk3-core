<?php

namespace Larakit;

use Illuminate\Support\Arr;

class YaTranslate {
    static function translate($text) {
        if (!$text) {
            return '';
        }
        $params           = [];
        $params['key']    = config('larakit.translate');
        $params['text']   = mb_substr($text, 0, 100);
        $params['lang']   = 'en';
        $params['format'] = 'plain';
        $url              = 'https://translate.yandex.net/api/v1.5/tr.json/translate' . '?' . http_build_query($params);
        $val              = file_get_contents($url);
        $val              = json_decode($val);

        return Arr::get($val->text, 0);
    }
}
