<?php

namespace Larakit;

use Illuminate\Support\Str;
use Larakit\Attach\LkNgAttach;
use Larakit\Gallery\LkNgGallery;
use Larakit\Thumb\LkNgThumb;

/**
 * Class CrudRoute
 *
 * @package Larakit
 *
 * @author  Sergey Koksharov <sharoff45@gmail.com>
 */
class CrudRoute {
    
    protected $controller;
    protected $model_class;
    protected $model_param;
    protected $route_name_prefix;
    protected $route_prefix;
    
    static function factory($model_class, $controller) {
        return new CrudRoute($model_class, $controller);
    }
    
    function __construct($model_class, $controller) {
        
        $this->model_param       = self::getModelParam($model_class);
        $this->model_class       = $model_class;
        $this->controller        = $controller;
        $this->route_prefix      = self::getRoutePrefix($model_class);
        $this->route_name_prefix = self::getRouteNamePrefix($model_class);
        \Route::model($this->model_param, $model_class);
        \Route::pattern($this->model_param, '[0-9]+');
        \Route::bind($this->model_param . '_with_trashed', function ($value) use ($model_class) {
            return $model_class::withTrashed()
                    ->find($value) ?? abort(404);
        });
    }
    
    static function getModelParam($model_class) {
        $r = new \ReflectionClass($model_class);
        
        return $r->getShortName();
    }
    
    function setRoutePrefix($value) {
        $this->route_prefix = $value;
        
        return $this;
    }
    
    static function getRoutePrefix($model_class) {
        $model_param = self::getModelParam($model_class);
        
        return '/' . Str::snake(Str::plural($model_param), '-');
    }
    
    static function getRouteNamePrefix($model_class) {
        $model_param = self::getModelParam($model_class);
        
        return 'api.' . Str::snake(Str::plural($model_param));
    }
    
    function getController() {
        return $this->controller;
    }
    
    function index() {
        \Route::any($this->route_prefix . '/', $this->controller . '@index')
            ->middleware('can:index,' . $this->model_class)
            ->name($this->route_name_prefix);
        
        return $this;
    }
    
    function options() {
        \Route::any($this->route_prefix . '/options', $this->controller . '@options')
            ->middleware('can:index,' . $this->model_class)
            ->name($this->route_name_prefix . '.options');
        
        return $this;
    }
    
    function config() {
        \Route::any($this->route_prefix . '/config', $this->controller . '@config')
            ->middleware('can:index,' . $this->model_class)
            ->name($this->route_name_prefix . '.config');
        
        return $this;
    }
    
    function stat() {
        \Route::any($this->route_prefix . '/stat', $this->controller . '@stat')
            ->middleware('can:index,' . $this->model_class)
            ->name($this->route_name_prefix . '.stat');
        
        return $this;
    }
    
    function create() {
        \Route::post($this->route_prefix . '/create', $this->controller . '@create')
            ->middleware('can:create,' . $this->model_class)
            ->name($this->route_name_prefix . '.create');
        
        return $this;
    }
    
    function item() {
        \Route::any($this->route_prefix . '/{' . $this->model_param . '}', $this->controller . '@item')
            ->middleware('can:item,' . $this->model_param)
            ->name($this->route_name_prefix . '.id');
        
        return $this;
    }
    
    function itemUpdate() {
        \Route::post($this->route_prefix . '/{' . $this->model_param . '}/update', $this->controller . '@update')
            ->middleware('can:update,' . $this->model_param)
            ->name($this->route_name_prefix . '.id.update');
        
        return $this;
    }
    
    function itemDelete() {
        \Route::post($this->route_prefix . '/{' . $this->model_param . '}/delete', $this->controller . '@delete')
            ->middleware('can:delete,' . $this->model_param)
            ->name($this->route_name_prefix . '.id.delete');
        
        return $this;
    }
    
    function itemRestore() {
        \Route::post($this->route_prefix . '/{' . $this->model_param . '_with_trashed}/restore', $this->controller . '@restore')
            ->middleware('can:restore,' . $this->model_param . '_with_trashed')
            ->name($this->route_name_prefix . '.id.restore');
        
        return $this;
    }
    
    function routePrefix() {
        return $this->route_prefix;
    }
    
    function routePrefixName() {
        return $this->route_name_prefix;
    }
    
    function extItem($uri, $method = null) {
        if(!$method) {
            $method = Str::camel($uri);
        }
        \Route::any($this->route_prefix . '/{' . $this->model_param . '}/' . $uri, $this->controller . '@' . $method)
            ->middleware('can:' . $method . ',' . $this->model_param)
            ->name($this->route_name_prefix . '.id.' . $uri);
        
        return $this;
    }
    
    function ext($uri, $method = null) {
        if(!$method) {
            $method = Str::camel($uri);
        }
        \Route::any($this->route_prefix . '/' . $uri, $this->controller . '@' . $method)
            ->middleware('can:' . $method . ',' . $this->model_class)
            ->name($this->route_name_prefix . '.' . $uri);
        
        return $this;
    }
    
    function galleries($config) {
        LkNgGallery::registerModel($this->model_class, $config);
        
        return $this;
    }
    
    function attaches($config) {
        LkNgAttach::registerModel($this->model_class, $config);
        
        return $this;
    }
}
