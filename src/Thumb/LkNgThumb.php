<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 22.05.17
 * Time: 13:27
 */

namespace Larakit\Thumb;

use Illuminate\Support\Arr;

class LkNgThumb {

    protected static $filters = [];

    static function filters() {
        return self::$filters;
    }

    static function filter($name) {
        return Arr::get(self::$filters, $name);
    }

    static function registerFilter($name, $callback) {
        static::$filters[$name] = $callback;
    }

    protected static $configs = [];

    static function registerModel($model_class, $config) {
        self::$configs[$model_class] = $config;
    }

    static function models() {
        return array_keys(self::$configs);
    }

    static function types($model) {
        $model_class = get_class($model);

        return Arr::get(self::$configs, $model_class);
    }

    static function thumb($model, $type) {
        $model_class = get_class($model);

        $thumb_class = Arr::get(self::$configs, $model_class . '.' . $type);

        return new $thumb_class($model->id);
    }

}
