<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 14.06.17
 * Time: 14:10
 */

namespace Larakit\Thumb;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait TraitModelThumb {

    //    function thumbsConfig() {
    //        return [
    //            'base' => ColorThumb::class,
    //        ];
    //    }

//    static function getThumbKey() {
//        $r = new \ReflectionClass(static::class);
//
//        return md5(env('APP_KEY') . Str::snake($r->getShortName(), '-'));
//    }

    protected $is_thumb_hashed = false;

    function thumbHashed() {
        $this->is_thumb_hashed = true;
    }

}
