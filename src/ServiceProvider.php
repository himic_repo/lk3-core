<?php

namespace Larakit;

use Illuminate\Support\Facades\App;
use Larakit\Console\CommandVcs;
use Larakit\Console\CrudCommand;
use Larakit\Console\CrudControllerCommand;
use Larakit\Console\CrudFormFilterCommand;
use Larakit\Console\CrudModelCommand;
use Larakit\Console\CrudNgCommand;
use Larakit\Console\CrudPolicyCommand;
use Larakit\Console\CrudResourceCollectionCommand;
use Larakit\Console\CrudResourceCommand;
use Larakit\Console\CrudRoutesCommand;
use Larakit\Console\CrudThumbCommand;
use Larakit\Console\CrudValidatorCommand;
use Larakit\Console\FilemanagerCommand;
use Larakit\Console\LangFileCommand;
use Larakit\Console\LangFileContentCommand;
use Larakit\Console\LangFileContentTranslateCommand;
use Larakit\Console\LangFileDeployCommand;
use Larakit\Console\LangFileGuiCommand;
use Larakit\Console\LangFileGuiTranslateCommand;
use Larakit\Console\SnippetCommand;
use Larakit\Console\SnippetDeployCommand;
use Larakit\Console\VueCommand;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        require_once __DIR__ . '/../boot/functions/rglob.php';
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/larakit.php' => config_path('larakit.php'),
            ]);
            $this->publishes([
                __DIR__ . '/../publishes/' => base_path('/'),
            ]);
        }
        $this->loadViewsFrom(__DIR__ . '/Console/stubs', 'larakit-stubs');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        require_once __DIR__ . '/../boot/functions/rglob.php';
        foreach (rglob('*.php', 0, __DIR__ . '/../boot') as $file) {
            include_once $file;
        }
        $this->mergeConfigFrom(__DIR__ . '/../config/larakit.php', 'larakit');
        $auth_driver = base_path('routes/auth/' . config('larakit.auth') . '.php');
        if (!file_exists($auth_driver)) {
            $auth_driver = base_path('routes/auth/simple.php');
        }
        if (file_exists($auth_driver)) {
            require_once $auth_driver;
        }

        \Route::middleware('api')
              ->prefix('api')
              ->namespace('Larakit\Controllers')
              ->group(function () {
                  foreach (rglob('*.php', 0, __DIR__ . '/../routes/') as $file) {
                      include_once $file;
                  };
              });

        $this->app->alias('Telegram', \Telegram\Bot\Laravel\Facades\Telegram::class);
        $this->app->registerDeferredProvider(\Telegram\Bot\Laravel\TelegramServiceProvider::class);
        $this->app->bind('larakit:lang-gui', LangFileGuiCommand::class);
        $this->app->bind('larakit:lang-content', LangFileContentCommand::class);
        $this->app->bind('larakit:lang-deploy', LangFileDeployCommand::class);
        $this->app->bind('larakit:lang-content-translate', LangFileContentTranslateCommand::class);
        $this->app->bind('larakit:lang-gui-translate', LangFileGuiTranslateCommand::class);
        $this->app->bind('larakit:lang', LangFileCommand::class);
        //        $this->app->bind('larakit:crud', CrudCommand::class);
        //        $this->app->bind('larakit:crud:model', CrudModelCommand::class);
        //        $this->app->bind('larakit:crud:form-filter', CrudFormFilterCommand::class);
        //        $this->app->bind('larakit:crud:controller', CrudControllerCommand::class);
        //        $this->app->bind('larakit:crud:resource', CrudResourceCommand::class);
        //        $this->app->bind('larakit:crud:resource-collection', CrudResourceCollectionCommand::class);
        //        $this->app->bind('larakit:crud:policy', CrudPolicyCommand::class);
        //        $this->app->bind('larakit:crud:thumb', CrudThumbCommand::class);
        //        $this->app->bind('larakit:crud:validator', CrudValidatorCommand::class);
        //        $this->app->bind('larakit:crud:ng', CrudNgCommand::class);
        //        $this->app->bind('larakit:crud:routes', CrudRoutesCommand::class);
        $this->app->bind('larakit:vcs', CommandVcs::class);
        $this->app->bind('larakit:snippet', SnippetCommand::class);
        $this->app->bind('larakit:filemanager', FilemanagerCommand::class);
        $this->app->bind('larakit:vue', VueCommand::class);
        $this->commands([
            'larakit:vue',
            'larakit:lang-gui',
            'larakit:lang-content',
            'larakit:lang-deploy',
            'larakit:lang-content-translate',
            'larakit:lang-gui-translate',
            'larakit:lang',
            //            'larakit:crud',
            //            'larakit:crud:model',
            //            'larakit:crud:form-filter',
            //            'larakit:crud:controller',
            //            'larakit:crud:resource',
            //            'larakit:crud:resource-collection',
            //            'larakit:crud:policy',
            //            'larakit:crud:thumb',
            //            'larakit:crud:validator',
            //            'larakit:crud:ng',
            //            'larakit:crud:routes',
            'larakit:vcs',
            'larakit:snippet',
            'larakit:filemanager',
        ]);
    }
}
