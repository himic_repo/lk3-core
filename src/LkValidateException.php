<?php

namespace Larakit;

use Exception;

class LkValidateException extends Exception {
    protected $errors = [];

    function setErrors($errors) {
        $this->errors = $errors;
    }

    function getErrors() {
        return $this->errors;
    }
}
