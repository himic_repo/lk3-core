<?php

namespace Larakit\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class HelperArr {

    public static function onlyKey($list, $keys) {
        $ret = [];
        foreach ($list as $k => $item) {
            foreach ($keys as $key) {
                Arr::set($ret, $k . '.' . $key, Arr::get($item, $key));
            }
        }

        return $ret;
        //        return array_map(function ($v) use ($keys) {
        //            return \Illuminate\Support\Arr::only($v, $keys);
        //        }, $list);
    }

    static function flatten($array, $parentKey = '') {
        $storage = [];
        foreach ($array as $key => $value) {
            $itemKey = (($parentKey) ? $parentKey . '.' : '') . $key;
            if (is_array($value)) {
                $storage = array_merge($storage, self::flatten($value, $itemKey));
            } else {
                $storage[$itemKey] = $value;
            }
        }

        return $storage;
    }

    static function fromResource($item) {
        return $item->toArray(\Request::instance());
    }

    static function fromCollection($items) {
        $request = \Request::instance();
        $ret     = [];
        foreach ($items->toArray($request) as $item) {
            $ret[] = $item->toArray($request);
        };

        return $ret;
    }

}
