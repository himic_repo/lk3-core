<?php

namespace Larakit\Helpers;

use Illuminate\Support\Arr;

class HelperOptions {
    static function get($resources, $fieldToString, $fields = []) {
        $ret = [];
        foreach ($resources as $resource) {
            $item = [];
            if (is_callable($fieldToString)) {
                $toString = $fieldToString($resource);
            } else {
                $toString = $resource[$fieldToString];
            }

            foreach ((array) $fields as $_fields) {
                $_fields = (array) $_fields;
                foreach ($_fields as $k => $v) {
                    if (is_numeric($k)) {
                        if (is_callable($v)) {
                            Arr::set($item, $v, $v($resource));
                        } else {
                            Arr::set($item, $v, Arr::get($resource, $v));
                        }
                    } else {
                        if (is_callable($v)) {
                            Arr::set($item, $k, $v($resource));
                        } else {
                            Arr::set($item, $k, Arr::get($resource, $v));
                        }
                    }
                }
            }
            $ret[] = array_merge([
                'id'       => $resource->id,
                'toString' => $toString,
            ], $item);
        }

        return $ret;
    }
}


