<?php
/**
 * Created by PhpStorm.
 * User: aberdnikov
 * Date: 31.08.2017
 * Time: 8:24
 */

namespace Larakit\Helpers;

class HelperPhone
{

    static function denormalize($phone)
    {
        $phone = self::normalize($phone);
        return '+7 (' . mb_substr($phone, 1, 3) . ') ' . mb_substr($phone, 4, 3) . '-' . mb_substr(
            $phone, 7, 2
        ) . '-' . mb_substr($phone, 9, 2);
    }

    static function normalize($phone)
    {
        $p = preg_replace('/\D/', '', $phone);
        switch (true) {
            case 11 == mb_strlen($p):
                return $p;
                break;
            case (10 == mb_strlen($p)):
                return '7' . $p;
                break;
            default:
                break;
        }

        return null;
    }

}
