<?php
namespace Larakit\Helpers;

use Illuminate\Mail\Message;

class HelperNotify {
    
    static function notifyEmail($to, $subject, $content) {
        $to = (array) $to;
        foreach($to as $email) {
            \Mail::send('pushka-core::email', compact('content', 'subject'), function (Message $mailbox) use ($email, $subject) {
                $mailbox->to($email);
                $mailbox->subject($subject);
            });
        }
    }
    
    static function notifySms($to, $text) {
        $data   = [
            'login'   => 'megafilm45',
            'psw'     => md5('babaJaga45'),
            'sender'  => 'pushka.club',
            'charset' => 'utf-8',
            'mes'     => $text,
            'phones'  => HelperPhone::normalize($to),
        ];
        $url    = 'https://smsc.ru/sys/send.php?' . http_build_query($data);
        $result = file_get_contents($url);
        
        return true;
    }
    
}