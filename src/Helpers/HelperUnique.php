<?php

namespace Larakit\Helpers;

use Illuminate\Support\Arr;

/**
 * Class HelperUnique
 * @package Larakit\Helpers
 *
 * @author  Sergey Koksharov <sharoff45@gmail.com>
 */
class HelperUnique
{
    static function value($text)
    {
        $ret = self::check($text);

        return (int) Arr::get($ret, 'value');
    }

    static function check($text)
    {
        $post_data = [
            'key'  => 'csFYsZ4RKVyOWui',
            'text' => $text,
            'test' => 0 // при значении 1 вы получите валидный фиктивный ответ (проверки не будет, деньги не будут списаны)
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_URL, 'https://content-watch.ru/public/api/');
        $return = json_decode(trim(curl_exec($curl)), true);
        curl_close($curl);

        // если в ответе нет переменной error, значит запрос не удался
        if (isset($return['error'])) {
            if (empty($return['error'])) {
                // инициализируем дефолтные значения
                $defaults = [
                    'text'      => '',
                    'percent'   => '100.0',
                    'highlight' => [],
                    'matches'   => [],
                ];
                $return   = array_merge($defaults, $return);
                $val_uniq = (int) \Illuminate\Support\Arr::get($return, 'percent', 0);

                return [
                    'result'  => 'success',
                    'message' => 'Значение получено',
                    'value'   => $val_uniq,
                ];
            }
        }

        return [
            'result'  => 'error',
            'message' => \Illuminate\Support\Arr::get($return, 'error'),
        ];

    }
}
