<?php

namespace Larakit\Codegen;

use Illuminate\Support\Str;

/**
 * Class Codegen
 *
 * @package Larakit\Codegen
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class CodegenModel {
    protected $model_name;
    protected $table_name;
    protected $label;
    protected $fields    = [];
    protected $fillable  = [];
    protected $relations = [];
    protected $traits    = [
        'softdeletes' => true,
        'timestamps'  => true,
    ];

    function __construct($model_name) {
        $this->model_name = $model_name;
        $this->table_name = Codegen::makeTableName($model_name);
        $this->setTraitSoftdeletes(true)
             ->setTable($this->table_name)
             ->setTraitTimestamps(true);
    }

    function setAttributeTable($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeTable($this->table_name, $k, $v);

        return $this;
    }

    function setAttributeModel($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeModel($this->model_name, $k, $v);

        return $this;
    }

    function setTable($value) {
        return $this->setAttributeModel(__FUNCTION__, $value)
                    ->setAttributeTable(__FUNCTION__, $value);
    }

    function setTraitSoftdeletes($value) {
        return $this->setAttributeModel(__FUNCTION__, (bool) $value, true);
    }

    function setTraitTags($value) {
        return $this->setAttributeModel(__FUNCTION__, (bool) $value, true);
    }

    function setTraitTimestamps($value) {
        return $this->setAttributeModel(__FUNCTION__, (bool) $value, true);
    }

    function setTraitAttach($code, $label, array $ext = null, $max = 0) {
        $ret = [
            'label' => $label,
        ];
        if ($ext) {
            $ret['ext'] = $ext;
        }
        if ($max) {
            $ret['max'] = $max;
        }

        return $this->setAttributeModel(__FUNCTION__, $ret);
    }

    function setTraitGallery($prefix = '_') {
        $class = Str::studly($this->model_name . '_' . Str::lower($prefix) . '_gallery_thumb');

        return $this->setAttributeModel(__FUNCTION__, $prefix, $class);
    }

    function setTraitThumbs($prefix = '_') {
        $class                           = Str::studly($this->model_name . '_' . Str::lower($prefix) . '_thumb');
        $this->traits['thumbs'][$prefix] = $class;

        return $this->setAttributeModel(__FUNCTION__, $prefix, $class);

        return $this;
    }

    function setRelation($relation_name, $data) {
        return $this->setAttributeModel(__FUNCTION__ . '.' . $relation_name, $data);
    }

    function setRelationHasmany($model_related, $field, $relation_name = null) {
        if (!$relation_name) {
            $relation_name = Str::plural(Str::snake($model_related));
        }

        return $this->setRelation($relation_name, [
            'type'  => 'hasMany',
            'model' => $model_related,
            'field' => $field,
        ]);
    }

    function setRelationBelongsToMany($model_related, $relation_name_one = null, $relation_name_two = null, $is_recurse = true) {
        if (!$relation_name_one) {
            $relation_name_one = Str::plural(Str::lower($model_related));
        }
        if ($relation_name_two) {
            $relation_name_two = Str::plural(Str::lower($this->model_name));
        }

        $table = Codegen::makeTableNameBelongsToMany($model_related, $this->model_name);
        if ($is_recurse) {
            Codegen::model($model_related, function (CodegenModel $model) use ($relation_name_two) {
                $model->setRelationBelongsToMany($this->model_name, $relation_name_two, null, false);
            });
        }
        Codegen::setAttributeTableField($table, Str::snake($model_related) . '_id', 'type', 'integer');
        Codegen::setAttributeTableField($table, Str::snake($model_related) . '_id', 'index', true);

        return $this->setRelation($relation_name_one, [
            'type'  => 'belongsToMany',
            'model' => $model_related,
            'table' => $table,
        ]);
    }

    function setRelationBelongsTo($field, $model_related, $relation_name = null) {
        if (!$relation_name) {
            $relation_name = str_replace('_id', '', $field);
        }

        return $this->setRelation($relation_name, [
            'type'  => 'belongsTo',
            'field' => $field,
            'model' => $model_related,
        ]);
    }

    function setLabel($value) {
        return $this->setAttributeModel(__FUNCTION__, $value);
    }

    protected function field($field_name, $db_type) {
        $db_type = Str::lower(str_replace('field', '', $db_type));

        return new CodegenModelField($this->model_name, $field_name, $db_type);
    }

    /**
     * @param $field_name
     *
     * @return CodegenModelField
     */
    function fieldText($field_name) {
        return $this->field($field_name, __FUNCTION__)
                    ->setFormfilterLike()
                    ->setFormInputTextarea();
    }

    /**
     * @param $field_name
     *
     * @return CodegenModelField
     */
    function fieldString($field_name) {
        return $this->field($field_name, __FUNCTION__)
                    ->setFormfilterLike()
                    ->setFormInputText();
    }

    function fieldInteger($field_name) {
        return $this->field($field_name, __FUNCTION__)
                    ->setFormInputNumber();
    }
    //
    //    /**
    //     * @param $field_name
    //     *
    //     * @return CodegenModelField
    //     */
    //    function fieldBoolean($field_name) {
    //        $this->fields[$field_name] = $this->field($field_name, __FUNCTION__, Codegen::INPUT_CHECKBOX);
    //
    //        return $this->fields[$field_name];
    //    }
    //
    //    /**
    //     * @param $field_name
    //     *
    //     * @return CodegenModelField
    //     */
    //    function fieldInteger($field_name) {
    //        $this->fields[$field_name] = $this->field($field_name, __FUNCTION__, Codegen::INPUT_NUMBER);
    //
    //        return $this->fields[$field_name];
    //    }
    //
    //

    //

    //
    //    function toArray1() {
    //        $ret = [];
    //        if ($this->label) {
    //            $ret['label'] = $this->label;
    //        }
    //        foreach ($this->fields as $field_name => $field) {
    //            $ret['fields'][$field_name] = $field->toArray();
    //        }
    //        foreach ($this->relations as $rel_name => $data) {
    //            $ret['relations'][$rel_name] = $data;
    //        }
    //        $ret['traites']  = $this->traits;
    //        $ret['table']    = $this->table_name;
    //        $ret['fillable'] = $this->fillable;
    //
    //        return $ret;
    //    }
}
