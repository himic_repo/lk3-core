<?php

namespace Larakit\Codegen;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Class Codegen
 *
 * @package Larakit\Codegen
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class Codegen {
    static protected $models = [];
    static protected $tables = [];

    static function setAttributeTable($table_name, $k = null, $v) {
        Arr::set(self::$tables, $table_name . ($k ? '.' . $k : ''), $v);
    }

    static function getAttributeTable($table_name, $k = null, $default = null) {
        return Arr::get(self::$tables, $table_name . ($k ? '.' . $k : ''), $default);
    }

    static function setAttributeTableField($table_name, $field_name, $k = null, $v) {
        Arr::set(self::$tables, $table_name . '.fields.' . $field_name . ($k ? '.' . $k : ''), $v);
    }

    static function getAttributeTableField($table_name, $field_name, $k = null, $default = null) {
        return Arr::get(self::$tables, $table_name . '.fields.' . $field_name . ($k ? '.' . $k : ''), $default);
    }

    static function setAttributeModel($model_name, $k = null, $v) {
        Arr::set(self::$models, $model_name . ($k ? '.' . $k : ''), $v);
    }

    static function getAttributeModel($model_name, $k = null, $default = null) {
        return Arr::get(self::$models, $model_name . ($k ? '.' . $k : ''), $default);
    }

    static function setAttributeModelField($model_name, $field_name, $k = null, $v) {
        Arr::set(self::$models, $model_name . '.fields.' . $field_name . ($k ? '.' . $k : ''), $v);
    }

    static function getAttributeModelField($model_name, $field_name, $k = null, $default = null) {
        return Arr::get(self::$models, $model_name . '.fields.' . $field_name . ($k ? '.' . $k : ''), $default);
    }

    static function setAttributeModelForm($model_name, $field_name, $k = null, $v) {
        Arr::set(self::$models, $model_name . '.form.' . $field_name . ($k ? '.' . $k : ''), $v);
    }

    static function getAttributeModelForm($model_name, $field_name, $k = null, $default = null) {
        return Arr::get(self::$models, $model_name . '.form.' . $field_name . ($k ? '.' . $k : ''), $default);
    }

    //const ATTR_CAST       = 'cast';
    //const ATTR_NULLABLE   = 'db_nullable';
    //const ATTR_DB_SIZE    = 'db_size';
    //const ATTR_REQUIRED   = 'required';
    //const ATTR_UNIQUE     = 'unique';
    //const ATTR_RELATION   = 'relation';
    //const ATTR_INPUT      = 'input';
    //const ATTR_LABEL_FORM = 'label_form';
    //const ATTR_LABEL_LIST = 'label_list';
    //const ATTR_DESC       = 'desc';
    //const ATTR_EXAMPLES   = 'examples';
    //const ATTR_INDEX      = 'db_index';

    /**
     * @param $model_name
     *
     * @return CodegenModel
     */
    static function model($model_name, $callback) {
        $model_name = Str::singular($model_name);
        $model_name = Str::studly($model_name);
        $callback(new CodegenModel($model_name));
    }

    static function makeTableName($model_name) {
        return Str::plural(Str::snake($model_name));
    }

    static function makeTableNameBelongsToMany($model_one, $model_two) {
        $table = [
            Str::snake($model_one),
            Str::snake($model_two),
        ];
        sort($table);
        $table = implode('_has_', $table);

        return $table;
    }

    static function tableFromModel($model_name, $callback) {
        $table_name = CodegenTable::tableName($model_name);
        self::table($table_name, $callback);
    }

    static function table($table_name, $callback) {
        if (!isset(self::$tables[$table_name])) {
            self::$tables[$table_name] = new CodegenTable($table_name);
        }
        $callback(self::$tables[$table_name]);
    }

    static function toArray() {
        $ret           = [];
        $ret['models'] = self::$models;
        $ret['tables'] = self::$tables;

        return $ret;
    }

}
