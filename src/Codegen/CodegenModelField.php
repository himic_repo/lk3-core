<?php

namespace Larakit\Codegen;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Class Codegen
 *
 * @package Larakit\Codegen
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class CodegenModelField {
    protected $model_name;
    protected $field_name;
    protected $field_type;
    protected $table_name;

    function __construct($model, $field_name, $field_type) {
        $this->model_name = $model;
        $this->table_name = Str::plural(Str::snake($model));
        $this->field_name = $field_name;
        $this->setType($field_type);
        $this->setFillable(true);
        $this->setFormLabel($field_name);
        $field = (array) Codegen::getAttributeTableField($this->table_name, $field_name, null, []);
        Codegen::setAttributeTableField($this->table_name, $field_name, null, $field);
    }

    function setType($value) {
        return $this->setAttributeTableField(__FUNCTION__, $value);
    }

    function setAttributeTableField($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeTableField($this->table_name, $this->field_name, $k, $v);

        return $this;
    }

    function setFillable($value) {
        $fillable = Codegen::getAttributeModel($this->model_name, 'fillable', []);
        if ($value) {
            $fillable[] = $this->field_name;
        } else {
            $fillable = array_filter($fillable, function ($v) {
                return $v !== $this->field_name;
            });
        }
        $fillable = array_unique($fillable);

        return $this->setAttributeModel(__FUNCTION__, $fillable);
    }

    function setAttributeModel($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeModel($this->model_name, $k, $v);

        return $this;
    }

    function setAttributeTable($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeTable($this->table_name, $k, $v);

        return $this;
    }

    function setCast($value) {
        $cast                    = Codegen::getAttributeModel($this->model_name, 'cast', []);
        $cast[$this->field_name] = $value;

        return $this->setAttributeModel(__FUNCTION__, $cast);
    }

    function setLabel($value) {
        return $this->setFormLabel($value)
                    ->setLabelList($value);
    }

    function setLabelList($value) {
        return $this->setAttributeModelField(__FUNCTION__, $value);
    }

    function setAttributeModelField($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeModelField($this->model_name, $this->field_name, $k, $v);

        return $this;
    }

    function setAttributeModelForm($k, $v) {
        $k = Str::snake(Str::substr($k, 3), '.');
        Codegen::setAttributeModelForm($this->model_name, $this->field_name, $k, $v);

        return $this;
    }

    function setFormLabel($value) {
        return $this->setAttributeModelForm('___label', $value);
    }

    function setFormDesc($value) {
        return $this->setAttributeModelForm('___desc', $value);
    }

    function setFormExamples($value) {
        return $this->setAttributeModelForm('___example', $value);
    }

    function setNullable($value = true) {
        return $this->setAttributeTableField(__FUNCTION__, (bool) $value);
    }

    function setUnique($value = true) {
        return $this->setAttributeTableField(__FUNCTION__, (bool) $value)
                    ->setRuleUnique();
    }

    function setSize($value) {
        return $this->setAttributeTableField(__FUNCTION__, $value);
    }

    function setFormfilter($filter) {
        $filter = Str::snake(Str::substr($filter, 13), '.');

        return $this->setAttributeModelField(__FUNCTION__, $filter);
    }

    function setFormfilterLike() {
        return $this->setFormfilter(__FUNCTION__);
    }

    function setFormfilterIn() {
        return $this->setFormfilter(__FUNCTION__);
    }

    function setIndex($value = true) {
        return $this->setAttributeTableField(__FUNCTION__, $this->field_name . '_idx');
    }

    function setFormInputText() {
        return $this->setFormInput(__FUNCTION__);
    }

    function setFormInputNumber() {
        return $this->setFormInput(__FUNCTION__);
    }

    protected function setFormInput($type) {
        $type = Str::snake(Str::substr($type, 12));

        return $this->setAttributeModelForm('___input', $type);
    }

    function setFormInputTextarea() {
        return $this->setFormInput(__FUNCTION__);
    }

    function setFormInputSelect() {
        return $this->setFormInput(__FUNCTION__);
    }

    function setSort($value = true) {
        return $this->setAttributeModelField(__FUNCTION__, (bool) $value);
    }

    function setRuleUnique() {
        return $this->setAttributeModelField(__FUNCTION__, [
            'table' => $this->table_name,
            'field' => $this->field_name,
        ]);
    }

    function setRuleRequired() {
        return $this->setAttributeModelField(__FUNCTION__, true);
    }

    function setRuleNumeric() {
        return $this->setAttributeModelField(__FUNCTION__, true);
    }

    function setRuleExists($table_name) {
        return $this->setAttributeModelField(__FUNCTION__, [
            'table' => $table_name,
            'field' => 'id',
        ]);
    }

    function relationBelongsTo($model_related, $relation_name_has_many = null, $relation_name_belongs_to = null) {
        $this->setFormfilterIn()
             ->setRuleRequired()
             ->setRuleExists(Str::plural(Str::snake($model_related)))
             ->setRuleNumeric()
             ->setSort();
        Codegen::model($model_related, function (CodegenModel $model) use ($relation_name_has_many) {
            $model->setRelationHasmany(Str::lower(Str::plural($this->model_name)), $this->field_name, $relation_name_has_many);
        });
        Codegen::model($this->model_name, function (CodegenModel $model) use ($model_related, $relation_name_belongs_to) {
            $model->setRelationBelongsTo($this->field_name, $model_related, $relation_name_belongs_to);
        });

        return $this;
    }
}
