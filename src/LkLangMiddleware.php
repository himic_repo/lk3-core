<?php

namespace Larakit;

use Closure;

class LkLangMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        \App::setLocale(\Larakit\LangManager::getCurrentLocale());

        return $next($request);
    }
}
