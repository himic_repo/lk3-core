<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 22.05.17
 * Time: 13:27
 */

namespace Larakit;

use Illuminate\Support\Str;

class LkNgApp {

    protected static $items = [];

    /**
     * @param $default_url
     * @param $html
     *
     * @return mixed
     */
    static function register($code, $url, $html = null) {
        self::$items[$code]['html']   = $html ? $html : $code;
        self::$items[$code]['code']   = $code;
        self::$items[$code]['url']    = $url;
        $html                         = self::$items[$code]['html'];
        $html                         = strip_tags($html);
        self::$items[$code]['letter'] = mb_substr(__($html), 0, 1);
    }

    static function items() {
        return array_values(self::$items);
    }

}
