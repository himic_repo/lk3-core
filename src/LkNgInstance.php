<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 22.05.17
 * Time: 13:27
 */

namespace Larakit;

use App\Project;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class LkNgInstance {

    protected static $items = [];
    protected static $slug  = null;
    protected static $id    = null;

    /**
     * @param $default_url
     * @param $html
     *
     * @return mixed
     */

    static function items() {
        if (!self::$items) {
            self::$items = Project::get()
                                  ->keyBy('id');
        }

        return self::$items;
    }

    static function base() {
        return env('LK2_BASE_DOMAIN');
    }

    static function slug($slug = null) {
        return \Request::route('Project_Slug')->slug;
    }

    static function getCurrent() {
        return Arr::get(self::items(), self::id());
    }

    static function id() {
        if (is_null(self::$id)) {
            self::$id = 0;
            foreach (self::items() as $item) {
                if ($item->slug == self::slug()) {
                    self::$id = $item->id;
                }
            }
        }

        return self::$id;
    }

    static function route($route_name, $params) {
        if(!isset($params['slug'])){
            $params['slug'] = self::slug();
        }

        return route($route_name, $params, true);
    }

}
