<?php

namespace Larakit\Controllers;

use App\Validators\SnippetGroupValidator;
use Larakit\Controllers\ApiController;
use Illuminate\Http\Request;
use Larakit\Helpers\HelperOptions;
use Larakit\Snippet\Snippet;
use Larakit\Snippet\SnippetGroup;
use Larakit\Snippet\SnippetGroupFormFilter;
use Larakit\Snippet\SnippetGroupResource;
use Larakit\Snippet\SnippetGroupResourceCollection;

class SnippetGroupController extends ApiController {

    public function index(Request $request) {
        $resources = new SnippetGroupResourceCollection(SnippetGroupFormFilter::load());

        return $this->apiResponseSuccess($resources);
    }

    public function options(Request $request) {
        $options = HelperOptions::get(SnippetGroup::orderBy('name')
            ->get(), 'name');

        return $this->apiResponseSuccess($options);
    }

    public function config(Request $request) {
        return SnippetGroupFormFilter::config();
    }

    public function item(SnippetGroup $SnippetGroup, Request $request) {
        $resource = new SnippetGroupResource($SnippetGroup);

        return $this->apiResponseSuccess($resource);
    }

    public function update(SnippetGroup $model) {
        $snippets = (array)\Request::get('snippets');
        foreach($snippets as $snippet) {
            $id    = (int) \Arr::get($snippet, 'id');
            $html  = \Arr::get($snippet, 'html');
            $model = Snippet::find($id);
            if($model) {
                $model->html = (string)$html;
                $model->save();
            }
        }
        \Artisan::call('larakit:snippet');
        return $this->apiResponseSuccess(null, __('vendor.models.SnippetGroup.controller.update'));
    }

}
