<?php

namespace Larakit\Controllers;

use Larakit\Filemanager\FileModel;
use Larakit\Filemanager\FileModelFormFilter;
use Larakit\Controllers\ApiController;
use Larakit\Filemanager\FileModelCollection;
use Larakit\Filemanager\FileModelResource;
use Larakit\Filemanager\FileModelResourceCollection;
use Larakit\Filemanager\FileModelValidator;
use Illuminate\Http\Request;
use Larakit\Helpers\HelperOptions;

class FileModelController extends ApiController {

    public function index(Request $request) {
        $resources = new FileModelResourceCollection(FileModelFormFilter::load());

        return $this->apiResponseSuccess($resources);
    }

    public function options(Request $request) {
        $options = HelperOptions::get(FileModel::orderBy('name')
                                               ->get(), 'name');

        return $this->apiResponseSuccess($options);
    }

    public function config(Request $request) {
        return FileModelFormFilter::config();
    }

    public function update(FileModel $model) {
        $file = \Request::file('file');
        $dir  = dirname($model->path);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file->move($dir, \File::basename($model->path));
        $model->touch();

        return $this->apiResponseSuccess(null, 'Файл успешно загружен');
    }

}
