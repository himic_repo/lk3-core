<?php

namespace Larakit\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Larakit\Event\Event;
use Larakit\LkNgSidebar;

class SystemController extends Controller {
    function menus(Request $request) {
        $files = rglob('*.php', 0, base_path('bootstrap/ng'));
        foreach ($files as $file) {
            include_once $file;
        }
        Event::notify('larakit:menus');

        return [
            'result' => 'success',
            'data'   => LkNgSidebar::sidebars(),
        ];
    }

    function apps() {
        $files = rglob('*.php', 0, base_path('bootstrap/ng'));
        foreach ($files as $file) {
            include_once $file;
        }
        Event::notify('larakit:apps');

        return [
            'result' => 'success',
            'data'   => \Larakit\LkNgApp::items(),
        ];
    }

    function locales() {
        $ret = [
            'langs'     => \Larakit\LangManager::getLocales(),
            'lang'      => \Larakit\LangManager::getCurrent(),
            'lang_slug' => \Larakit\LangManager::getCurrentSlug(),
        ];

        return [
            'result' => 'success',
            'data'   => $ret,
        ];
    }

    function localeSet($locale) {
        \Larakit\LangManager::setCurrentSlug($locale);

        return (new \Illuminate\Http\JsonResponse([
            'result' => 'success',
            'data'   => [
                'current' => \Larakit\LangManager::getCurrent(),
                'lang'    => $locale,

            ],
        ]))->cookie(\Cookie::make(\Larakit\LangManager::KEY, $locale, 60 * 24 * 30));;
    }

}
