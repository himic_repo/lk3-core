<?php

namespace Larakit\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Larakit\Attach\Attach;
use Larakit\Attach\LkNgAttach;
use Larakit\Attach\ModelLkNgAttach;
use Larakit\Codegen\Codegen;
use Larakit\Gallery\ModelLkNgGallery;
use Larakit\Resource\HelperAttach;
use Larakit\Resource\HelperGallery;

class AppCodegenController extends ApiController {

    function web_index() {
        return view('ng', ['app' => 'codegen']);
    }

    function tables() {
        $tables = Arr::get(Codegen::toArray(), 'tables');

        return $this->apiResponseSuccess(array_keys($tables));
    }

}
