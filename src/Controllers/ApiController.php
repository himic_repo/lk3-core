<?php

namespace Larakit\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Request;
use Larakit\Resource\JsonResource;
use Larakit\Resource\JsonResourceCollection;

class ApiController extends \App\Http\Controllers\Controller {
    protected $per_page = 10;

    function __construct(Request $request) {
        $this->request = $request;
    }

    private function apiResponse($type, $data, $message = null) {
        $ret = [];
        if ($data) {
            $ret['data'] = $data;
        }
        $ret['result'] = $type;
        if ($message) {
            $ret['message'] = $message;
        }
        if (!\Request::expectsJson() && !Request::hasFile('file')) {
            if (is_a($data, JsonResourceCollection::class)) {
                $ret['data'] = resources_to_array($data);
            }
            if (is_a($data, JsonResource::class) || is_a($data, AnonymousResourceCollection::class)) {
                $ret['data'] = $data->toArray(\Request::instance());
            }
            dd($ret);
        }

        return $ret;

    }

    public function apiResponseSuccess($data, $message = null) {
        return $this->apiResponse('success', $data, $message);
    }

    public function apiResponseWarning($data, $message = null) {
        return $this->apiResponse('warning', $data, $message);
    }

    public function apiResponseError($data, $message = null) {
        return $this->apiResponse('error', $data, $message);
    }

    public function apiResponseInfo($data, $message = null) {
        return $this->apiResponse('info', $data, $message);
    }

    function saveTags($taggeble){
        $tags = (array)\Request::input('tags');
        $taggeble->tags()->detach();
        $ids = [];
        foreach($tags as $tag){
            $tag = \Illuminate\Support\Arr::get($tag,'toString');
            if($tag){
                $tag_model = \Larakit\Tags\Tag::firstOrCreate([
                    'name'=>$tag
                ]);
                $ids[] = $tag_model->id;
            }
        }
        if(count($ids)){
            $taggeble->tags()->sync($ids);
        }
    }


}
