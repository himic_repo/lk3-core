<?php

namespace Larakit\Resource;


/**
 * Trait TraitResourceColorize
 * @package Larakit\Resource
 * @property integer $id
 */
trait TraitResourceColorize {

    function bootTraitResourceColorize($ret) {
        $ret['colorize'] = HelperColor::toArray($this->id);

        return $ret;
    }
}
