<?php

namespace Larakit\Resource;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\Gallery\LkNgGallery;

class HelperGallery {
    static function toArray($resource) {
        $resource_name = (new \ReflectionClass($resource))->getShortName();
        $plurals       = Str::plural($resource_name);
        $plurals       = Str::snake($plurals);
        $route_upload  = 'api.' . $plurals . '.id.galleries.type.upload';
        $route_clear   = 'api.' . $plurals . '.id.galleries.type.clear';
        $route_dnd     = 'api.' . $plurals . '.id.galleries.type.dnd';
        $ret           = [];
        foreach ((array) LkNgGallery::types($resource) as $block_name => $thumb_class) {
            $ret[$block_name]['items']      = [];
            $ret[$block_name]['label']      = $thumb_class::getName();
            $ret[$block_name]['url_upload'] = route($route_upload, [
                $resource_name => $resource->{$resource->getRouteKeyName()},
                'block'        => $block_name,
            ]);
            $ret[$block_name]['url_clear'] = route($route_clear, [
                $resource_name => $resource->{$resource->getRouteKeyName()},
                'block'        => $block_name,
            ]);
            $ret[$block_name]['url_dnd']    = route($route_dnd, [
                $resource_name => $resource->{$resource->getRouteKeyName()},
                'block'        => $block_name,
            ]);
        }
        //        print '<pre>';
        //        debug_print_backtrace();
        //        print '</pre>';
        //        exit;
        $resource->load('galleries');
        foreach ($resource->galleries as $el) {
            $config                     = Arr::only(LkNgGallery::types($resource), [$el->block]);
            $ret[$el->block]['items'][] = [
                'id'       => $el->id,
                'name'     => $el->name,
                'desc'     => $el->desc,
                'priority' => (int) $el->priority,
                'thumbs'   => HelperThumbs::factory($el)
                                          ->setThumbConfig($config)
                                          ->setGateModel($resource)
                                          ->toArray(),
            ];
        }
        if (0) {

            $t = Arr::get($ret, 'base');
            dd($t);
            $t = Arr::get($ret, 'base.items.0')
                    ->toArray($request);
            dd($t);
            $t = Arr::get($t, 'thumbs')
                    ->toArray($request);
            dd($t);
            dd(Arr::get($t, 'base'));
            dd(Arr::get($t, 'base.sizes._'));
        }

        //
        return $ret;
    }
}
