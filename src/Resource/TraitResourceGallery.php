<?php

namespace Larakit\Resource;

use Larakit\Gallery\LkNgGallery;

trait TraitResourceGallery {
    function bootTraitResourceGallery($ret) {
        if (LkNgGallery::types($this->resource)) {
            $ret['gallery_blocks'] = HelperGallery::toArray($this->resource);
        }

        return $ret;
    }
}
