<?php

namespace Larakit\Resource;

use Illuminate\Http\Resources\Json\JsonResource as Base;

/**
 * Class JsonResource
 * @package Larakit
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
abstract class JsonResource extends Base {
    use TraitResourceSoftDeletes;
    use TraitResourceThumbs;
    use TraitResourceGallery;
    use TraitResourceAttach;
    use TraitResourceToString;
    use TraitResourceTranslate;
    use TraitResourceTags;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request) {
        $ret           = $this->_toArray();
        $ret['_hash_'] = md5($this->updated_at);
        $class         = static::class;
        foreach (class_uses_recursive($class) as $trait) {
            $method = 'boot' . class_basename($trait);
            if (method_exists($class, $method)) {
                $ret = call_user_func([$this, $method], $ret);
            }
        }

        return $ret;
    }

    public function toString() {
        return '';
    }

    abstract public function _toArray();

    function translateAttribute($attr) {
        $table = $this->resource->getTable();
        $key   = 'content.' . $table . '.' . $this->id . '.' . $attr;
        if (\Lang::has($key)) {
            return 'content.' . $table . '.' . $this->id . '.' . $attr;
        } else {
            return $this->{$attr};
        }
    }
}
