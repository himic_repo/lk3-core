<?php

namespace Larakit\Resource;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class JsonResourceCollection
 * @package Larakit
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class JsonResourceCollection extends ResourceCollection {
    public function toArray($request) {
        return $this->resource->toArray();
    }
}
