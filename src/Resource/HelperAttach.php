<?php

namespace Larakit\Resource;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Larakit\Attach\LkNgAttach;
use Larakit\CrudRoute;
use Larakit\Gallery\LkNgGallery;
use Larakit\Helpers\HelperText;

class HelperAttach {
    static function toArray($resource) {
        $model_name        = get_class($resource);
        $route_name_prefix = CrudRoute::getRouteNamePrefix($model_name);
        $model_param       = CrudRoute::getModelParam($model_name);
        $route_upload      = $route_name_prefix . '.id.attaches.type.upload';
        $route_dnd         = $route_name_prefix . '.id.attaches.type.dnd';
        $ret               = [];
        foreach ((array) LkNgAttach::types($resource) as $block_name => $config) {
            $ret[$block_name]['items']      = [];
            $ret[$block_name]['label']      = Arr::get($config, 'label');
            $ret[$block_name]['max']        = (int) Arr::get($config, 'max');
            $ext                            = Arr::get($config, 'ext');
            $ret[$block_name]['ext']        = $ext ? ('|' . implode('|', $ext) . '|') : '';
            $ret[$block_name]['url_upload'] = route($route_upload, [
                $model_param => $resource->{$resource->getRouteKeyName()},
                'block'      => $block_name,
            ]);
            $ret[$block_name]['url_dnd']    = route($route_dnd, [
                $model_param => $resource->{$resource->getRouteKeyName()},
                'block'      => $block_name,
            ]);
        }
        $resource->load('attaches');
        foreach ($resource->attaches as $el) {
            $ret[$el->block]['items'][] = [
                'id'          => $el->id,
                'name'        => $el->name,
                'ext'         => $el->ext,
                'size'        => $el->size,
                'hash'        => hashids_encode($el->id),
                'download'    => route('api.download.hash', ['ModelLkNgAttach_hash' => hashids_encode($el->id)]),
                'size_format' => HelperText::fileSize($el->size),
                'priority'    => (int) $el->priority,
                'url_update'  => route('api.attaches.id.update', ['ModelLkNgAttach' => $el->id]),
                'url_delete'  => route('api.attaches.id.delete', ['ModelLkNgAttach' => $el->id]),
            ];
        }

        return $ret;
    }
}
