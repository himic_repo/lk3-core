<?php

namespace Larakit\Resource;

use Illuminate\Database\Eloquent\SoftDeletes;

trait TraitResourceSoftDeletes {
    function bootTraitResourceSoftDeletes($ret) {
        $class  = get_class($this->resource);
        $traits = class_uses_recursive($class);
        if (in_array(SoftDeletes::class, $traits)) {
            $ret['deleted_at'] = $this->deleted_at;
        }

        return $ret;
    }
}
