<?php

namespace Larakit\Resource;

use Illuminate\Support\Str;
use Larakit\Thumb\LkNgThumb;

class HelperThumbs {
    protected $thumb_config = [];
    protected $resource;

    static function factory($resource) {
        return new HelperThumbs($resource);
    }

    function __construct($resource) {
        $this->resource = $resource;
    }

    function setThumbConfig($config) {
        $this->thumb_config = $config;

        return $this;
    }

    function getThumbConfig() {
        if (!$this->thumb_config) {
            return LkNgThumb::types($this->resource);
        }

        return $this->thumb_config;
    }

    protected $gate_model = null;

    function getGateModel() {
        if (!$this->gate_model) {
            return $this->resource;
        }

        return $this->gate_model;
    }

    function setGateModel($gate_model) {
        $this->gate_model = $gate_model;

        return $this;
    }

    function toArray() {
        $this->resource_name = ((new \ReflectionClass($this->resource))->getShortName());

        $plurals = Str::plural($this->resource_name);
        $plurals = Str::snake($plurals);
        $prefix  = 'api.' . $plurals . '.id.thumbs';
        $ret     = [];
        foreach ($this->getThumbConfig() as $type => $thumb_class) {
            $t = new $thumb_class($this->resource->id);
            /** @var Thumb $t */
            $ret[$type]['name']  = $t->getName();
            $ret[$type]['sizes'] = $t->toArray($this->resource->is_thumb_hashed);
            if (\Gate::allows('thumbs', $this->getGateModel())) {
                $ret[$type]['original']     = $t->getUrl();
                $this->resource_route_value = $this->resource->{$this->resource->getRouteKeyName()};
                $ret[$type]['url_thumb']    = route($prefix, [
                    $this->resource_name => $this->resource_route_value,
                ],false);
                $ret[$type]['url_upload']   = route($prefix . '.type.upload', [
                    $this->resource_name => $this->resource_route_value,
                    'type'               => $type,
                ],false);
                $ret[$type]['url_clear']    = route($prefix . '.type.clear', [
                    $this->resource_name => $this->resource_route_value,
                    'type'               => $type,
                ],false);
                foreach ($t->toArray() as $size => $data) {
                    $ret[$type]['sizes'][$size]['url_crop'] = route($prefix . '.type.size.crop', [
                        $this->resource_name => $this->resource_route_value,
                        'type'               => $type,
                        'size'               => $size,
                    ],false);
                }
            } else {
                foreach ($t->toArray() as $size => $data) {
                    unset($ret[$type]['sizes'][$size]['make_url']);
                }
            }
        }

        return $ret;

    }
}
