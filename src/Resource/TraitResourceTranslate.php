<?php

namespace Larakit\Resource;

use Larakit\LkLangExporter;

/**
 * Trait TraitResourceColorize
 * @package Larakit\Resource
 * @property integer $id
 */
trait TraitResourceTranslate {

    function bootTraitResourceTranslate($ret) {
        $attrs = LkLangExporter::modelAttributes(get_class($this->resource));
        $table = $this->resource->getTable();
        if ($attrs) {
            foreach ($attrs as $attr) {
                $key = 'content.' . $table . '.' . $this->id . '.' . $attr;
                if(\Lang::has($key)){
                    $ret['__'][$attr] = __($key);
                } else {
                    $ret['__'][$attr] = $this->{$attr};
                }
            }

        }

        return $ret;
    }
}
