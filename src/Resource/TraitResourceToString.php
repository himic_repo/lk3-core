<?php

namespace Larakit\Resource;

trait TraitResourceToString {
    function bootTraitResourceToString($ret) {
        $toString = $this->toString();
        if ($toString) {
            $ret['toString'] = $toString;
        }

        return $ret;
    }
}
