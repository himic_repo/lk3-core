<?php

namespace Larakit\Resource;

use Larakit\Attach\LkNgAttach;

trait TraitResourceAttach {
    function bootTraitResourceAttach($ret) {
        if (LkNgAttach::types($this->resource)) {
            $ret['attach_blocks'] = HelperAttach::toArray($this->resource);
        }

        return $ret;
    }
}
