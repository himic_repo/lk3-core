<?php

namespace Larakit\Resource;

use Larakit\Tags\TraitModelTag;

trait TraitResourceTags
{
    function bootTraitResourceTags($ret)
    {
        $class  = get_class($this->resource);
        $traits = class_uses_recursive($class);
        if (in_array(TraitModelTag::class, $traits)) {
            $ret['tags'] = [];
            foreach ($this->resource->tags as $tag) {
                $ret['tags'][] = [
                    'id'       => $tag->id,
                    'toString' => $tag->name,
                ];
            }
        }

        return $ret;
    }
}
