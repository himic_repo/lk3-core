<?php

namespace Larakit\Resource;

class HelperColor {
    static function number($id) {
        return $id ? ($id % 15) : '';
    }

    static function color($id) {
        return 'color-' . self::number($id);
    }

    static function colorBackground($id) {
        return 'color-' . self::number($id) . '__bg';
    }

    static function colorBorder($id) {
        return 'color-' . self::number($id) . '__border';
    }

    static function toArray($id) {
        return [
            'bg'     => self::colorBackground($id),
            'font'   => self::color($id),
            'border' => self::colorBorder($id),
        ];
    }
}
