<?php

namespace Larakit\Resource;

use Larakit\Thumb\LkNgThumb;

trait TraitResourceThumbs {
    function bootTraitResourceThumbs($ret) {
        if (LkNgThumb::types($this->resource)) {
            $ret['thumbs'] = HelperThumbs::factory($this->resource)
                                         ->toArray();
        }

        return $ret;
    }
}
