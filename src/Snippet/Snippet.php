<?php

namespace Larakit\Snippet;

use Illuminate\Database\Eloquent\Model;

class Snippet extends Model {
    
    protected $table    = 'snippets';
    protected $fillable = [
        'group_id',
        'code',
        'label',
        'type',
        'html',
    ];
    
    function group() {
        return $this->belongsTo(SnippetGroup::class, 'group_id');
    }
}
