<?php

namespace Larakit\Snippet;

use Larakit\Thumb\Thumb;
use Larakit\Thumb\ThumbSize;

class SnippetGroupThumb extends Thumb
{

    /**
     * Расширение файла
     *
     * @var string
     */
    //protected $ext = 'png';

    static function getName()
    {
        return __('vendor.thumbs.SnippetGroupThumb.name');
    }

    function getPrefix()
    {
        return 'snippet-groups';
    }

    function getSizesList()
    {
        return [
            '_' => ThumbSize::factory(__('vendor.thumbs.SnippetGroupThumb.sizes.list'))
                ->setW(900)
                ->setDefault('https://via.placeholder.com/900x900')
                ->filterAdd(Thumb::FILTER_RESIZE_BY_WIDTH),
        ];
    }
}
