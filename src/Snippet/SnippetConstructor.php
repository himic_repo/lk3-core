<?php

namespace Larakit\Snippet;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class SnippetConstructor {

    static    $instance;
    protected $groups   = [];
    protected $snippets = [];
    protected $group;
    protected $tags;
    const TYPE_TEXT     = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_QUILL    = 'quill';
    protected $types = [
        self::TYPE_TEXT,
        self::TYPE_TEXTAREA,
        self::TYPE_QUILL,
    ];

    /**
     * @return SnippetConstructor
     */
    public static function instance() {
        if (!static::$instance) {
            static::$instance = new SnippetConstructor();
        }

        return static::$instance;
    }

    public function registerSnippetText($code, $label, $html) {
        return $this->registerSnippet($code, $label, $html, self::TYPE_TEXT);
    }

    public function registerSnippetTextarea($code, $label, $html) {
        return $this->registerSnippet($code, $label, $html, self::TYPE_TEXTAREA);
    }

    public function registerSnippetQuill($code, $label, $html) {
        return $this->registerSnippet($code, $label, $html, self::TYPE_QUILL);
    }

    /**
     * @param $name
     * @param $default_html
     *
     * @return $this
     * @throws \Exception
     */
    public function registerSnippet($code, $label, $html, $type) {
        if (!$this->group) {
            throw new \Exception('Не указана группа тегов');
        }
        if (!in_array($type, $this->types)) {
            throw new \Exception('Неизвестный тип сниппета "' . $type . '"');
        }
        $cnt = mb_substr_count($code, '.');
        if ($cnt) {
            throw new \Exception('В коде сниппетов запрещено использование точек');
        }
        $this->snippets[$this->group][$code] = [
            'html'  => $html,
            'label' => $label,
            'type'  => $type,
        ];

        return $this;
    }

    function get($group, $code) {
        return Arr::get($this->snippets, $group . '.' . $code . '.html');
    }

    public function registerGroup($code, array $tags) {
        $cnt = mb_substr_count($code, '.');
        if ($cnt) {
            throw new \Exception('В коде группы сниппетов запрещено использование точек');
        }
        $this->groups[$code] = $code;
        $this->tags[$code]   = $tags;

        return $this->to($code);
    }

    public function to($group_code) {
        if (!isset($this->groups[$group_code])) {
            throw new \Exception('Не зарегистрирована такая группа тегов');
        }
        $this->group = $group_code;

        return $this;
    }

    public function snippets() {
        $ret = [];
        foreach ($this->snippets as $group => $snippets) {
            foreach ($snippets as $code => $snippet) {
                $ret[] = [
                    'group' => $group,
                    'code'  => $code,
                    'type'  => Arr::get($snippet, 'type'),
                    'label' => Arr::get($snippet, 'label'),
                    'html'  => Arr::get($snippet, 'html'),
                ];
            }
        }

        return $ret;
    }

    public function tags() {
        return $this->tags;
    }

    public function groups() {
        ksort($this->groups);
        $ret = [];
        foreach ($this->groups as $k => $v) {
            $cnt = mb_substr_count($k, '.');

            $ret[$k] = trim(str_repeat('-', $cnt) . ' ' . $v);
        }

        return $ret;
    }
}
