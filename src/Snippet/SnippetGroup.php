<?php

namespace Larakit\Snippet;

use Illuminate\Database\Eloquent\Model;
use Larakit\Tags\TraitModelTag;

class SnippetGroup extends Model {
    use TraitModelTag;
    protected $table    = 'snippet_groups';
    protected $fillable = [
        'code',
        'label',
    ];

    function snippets() {
        return $this->hasMany(Snippet::class, 'group_id');
    }
}
