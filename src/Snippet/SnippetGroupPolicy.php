<?php

namespace Larakit\Snippet;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SnippetGroupPolicy
{
    use HandlesAuthorization;

    public function index(User $me)
    {
        return true;
    }

    public function manage(User $me)
    {
        return (bool) $me->is_admin;
    }

    public function create(User $me)
    {
        return $this->manage($me);
    }

    public function item(User $me, $model = null)
    {
        return true;
    }

    public function update(User $me, $model = null)
    {
        return $this->manage($me);
    }

    public function delete(User $me, $model = null)
    {
        return $this->manage($me);
    }

    public function thumbs(User $me, $model = null)
    {
        return $this->manage($me);
    }

    public function reset(User $me, $model = null)
    {
        return $this->manage($me);
    }

    public function restore(User $me, $model = null)
    {
        return $this->manage($me) && $model->deleted_at;
    }
}
