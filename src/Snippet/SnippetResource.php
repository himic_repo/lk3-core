<?php

namespace Larakit\Snippet;

use \Larakit\Resource\JsonResource;

class SnippetResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function _toArray()
    {
        $this->resource->load('group');
        return [
            'id'       => $this->id,
            'group_id' => $this->group_id,
            'code'     => $this->code,
            'label'    => $this->label,
            'type'     => $this->type,
            'html'     => $this->html,
            'default'  => SnippetConstructor::instance()
                ->get($this->group->code, $this->code),
        ];
    }
}
