<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 17.05.17
 * Time: 13:46
 */

namespace Larakit\Snippet;

use Larakit\FormFilter\FilterIn;
use \Larakit\FormFilter\FormFilter;
use Larakit\FormFilter\FilterLike;
use Larakit\FormFilter\Sorter;
use Larakit\Helpers\HelperOptions;
use Larakit\Snippet\Snippet;

class SnippetGroupFormFilter extends FormFilter {

    protected $per_page = 20;

    static function classModel() {
        return SnippetGroup::class;
    }

    function init() {
        $this->model->with('snippets');
        $this->addFilter(FilterLike::factory('html')
                                   ->relation('snippets')
                                   ->dbField([
                                       'code',
                                       'label',
                                       'type',
                                       'html',
                                   ])
                                   ->label('vendor.models.Snippet.label.html'));
        //        $this->addFilter(FilterIn::factory('group_id')
        //            ->options(HelperOptions::get(SnippetGroup::get(), 'label'))
        //            ->label('vendor.models.Snippet.label.group'));
        $this->addFilterTags(SnippetGroup::class);
        $this->addSorter(Sorter::factory('label')
                               ->label('vendor.models.Snippet.label.label'), 1, 1);
        //        $this->addSorter(Sorter::factory('group_id')
        //            ->label('vendor.models.Snippet.label.group'));
    }
}
