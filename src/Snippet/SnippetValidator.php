<?php

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 13.06.17
 * Time: 17:30
 */

namespace App\Validators;

use Larakit\Validator\ValidateBuilder;

class SnippetValidator extends ValidateBuilder
{

    function build()
    {
        $model = \Request::route('Complex');
        $this->to('name')
            ->ruleUnique('complexes', 'name', $model ? $model->id : 0, __('vendor.validation.unique'))
            ->ruleRequired(__('vendor.validation.required'))
        ;
        $this->to('desc')
            ->ruleRequired(__('vendor.validation.required'))
        ;
        $this->to('address')
            ->ruleRequired(__('vendor.validation.required'))
        ;
        $this->to('readiness')
            ->ruleNumeric('Не число')
            ->ruleMin(0, 'Не меньше нуля')
            ->ruleMax(100, 'Не больше 100')
        ;
    }
}
