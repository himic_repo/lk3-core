<?php

namespace Larakit\Snippet;

use \Larakit\Resource\JsonResource;

class SnippetGroupResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function _toArray() {
        $url   = '/!/snippets/' . $this->code . '.png';
        $image = file_exists(public_path($url)) ? $url : false;

        return [
            'id'       => $this->id,
            'label'    => $this->label,
            'code'     => $this->code,
            'image'    => $image,
            'snippets' => new SnippetResourceCollection($this->snippets),
        ];
    }
}
