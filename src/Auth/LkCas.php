<?php

namespace Larakit\Auth;

class LkCas {

    static $is_init = false;

    static function init() {
        $log = config('larakit.drivers.cas.log');
        if ($log) {
            $file = storage_path('cas/' . $log);
            $dir  = dirname($file);
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            chmod($dir, 0777);
            \phpCAS::setDebug($file);
        }
        if (!self::$is_init) {
            \phpCAS::client(CAS_VERSION_2_0, config('larakit.drivers.cas.host'), (int) config('larakit.drivers.cas.port'), config('larakit.drivers.cas.uri'));
            \phpCAS::setNoCasServerValidation();
            \phpCAS::forceAuthentication();
            self::$is_init = true;
        }
    }
}
