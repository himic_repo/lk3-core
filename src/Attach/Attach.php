<?php

namespace Larakit\Attach;

/**
 * Class Attach
 * @package Larakit\Attach
 *
 * @author  Alexey Berdnikov <aberdnikov@gmail.com>
 */
class Attach {
    static function makePath($model) {
        return public_path(self::makeUrl($model));
    }

    static function makeUrl($model) {
        $prefix   = [];
        $prefix[] = '!';
        $prefix[] = 'attaches';
        $prefix[] = mb_substr($model->id, -1);
        $prefix[] = mb_substr($model->id, -2, 1);
        $prefix[] = $model->id;
        $link     = '/' . implode('/', $prefix) . '/';
        $link     .= hashids_encode($model->id);
        $link     .= '.' . $model->ext;
        $prefix   = (string) env('LARAKIT_ATTACH_PREFIX');

        return $prefix . $link;
    }
}
