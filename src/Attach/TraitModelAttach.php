<?php
namespace Larakit\Attach;

trait TraitModelAttach {

    public function attaches() {
        return $this->morphMany(ModelLkNgAttach::class, 'attachable')->orderBy('priority', 'desc');
    }

}
