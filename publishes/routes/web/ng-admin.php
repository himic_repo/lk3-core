<?php
\Route::pattern('path', '.*');
$report_paths = [
    '/admin{path?}',
];

foreach ($report_paths as $path) {
    Route::get($path, function (\Illuminate\Http\Request $request) {
        return view('ng', ['app' => 'admin']);
    })
        ->middleware('auth', 'role_admin')
    ;
}
