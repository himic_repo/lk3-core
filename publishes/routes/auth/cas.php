<?php
Route::middleware('web')
     ->group(function () {
         Route::get('login', function () {
             $return = \Request::input('return');
             if ($return) {
                 \Redirect::setIntendedUrl($return);
             }
             if (config('larakit.drivers.cas.skip')) {
                 $user = \App\User::first();
                 if (!$user) {
                     throw new Exception('Добавьте руками первого пользователя');
                 }
                 Auth::login($user, true);

                 return Redirect::to('/admin');
             }
             \Larakit\Auth\LkCas::init();
             $attrs = \phpCAS::getAttributes();
             /** @var \App\User $user */
             $email     = \Illuminate\Support\Arr::get($attrs, 'jabber');
             $person_id = \Illuminate\Support\Arr::get($attrs, 'icPersonId');
             $user      = \App\User::where('login', '=', \phpCAS::getUser())
                                   ->first();
             // dd($user);
             if (!$user && $email) {
                 $user = \App\User::where('email', '=', $email)
                                  ->first();
             }
             if (!$user && $person_id) {
                 $user = \App\User::where('person_id', '=', $person_id)
                                  ->first();
             }
             if (!$user) {
                 $user = new \App\User();
             }
             $user->person_id = $person_id;
             $user->name      = (string) \Illuminate\Support\Arr::get($attrs, 'name');
             $user->city      = (string) \Illuminate\Support\Arr::get($attrs, 'place');
             $user->login     = (string) \phpCAS::getUser();
             $user->email     = (string) $email;
             $user->password  = md5(microtime(true));
             $user->save();
             $user->touch();

             if (\Auth::guest()) {
                 \Auth::login($user, true);
             }

             return Redirect::intended(config('larakit.drivers.cas.after', '/?after_auth'));
         })
              ->name('login');

         Route::any('logout', function (\Illuminate\Http\Request $request) {
             Auth::logout();
             Session::put('cas_logout', true);
             if ($request->wantsJson()) {
                 return [
                     'result'  => 'success',
                     'message' => 'Вы успешно вышли',
                 ];
             } else {
                 return Redirect::to('/logout-cas?123');
             }
         })
              ->name('logout');

         Route::get('logout-cas', function () {
             \Larakit\Auth\LkCas::init();
             $allowed_clients = config('larakit.drivers.cas.clients');
             $allowed_clients = explode(',', $allowed_clients);
             \phpCAS::handleLogoutRequests(true, $allowed_clients);
             \phpCAS::logout();
             Session::forget('cas_logout');

             return Redirect::to('/?logout_cas');
         })
              ->name('logout-cas');
     });
