<?php
$controller = 'Api\UserController';
\Larakit\CrudRoute::factory(App\User::class, $controller)
                  ->index()
                  ->options()
                  ->config()
                  ->create()
                  ->item()
                  ->itemUpdate()
                  ->itemDelete()
                  ->itemRestore()
                  ->extItem('is-admin');
