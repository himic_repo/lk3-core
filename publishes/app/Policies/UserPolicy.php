<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy {
    use HandlesAuthorization;

    public function index(User $me) {
        return true;
    }

    public function manage(User $me) {
        return (bool) $me->is_admin;
    }

    public function create(User $me) {
        return $this->manage($me);
    }

    public function item(User $me, $model = null) {
        return true;
    }

    public function update(User $me, $model = null) {
        return $this->manage($me);
    }

    public function isAdmin(User $me, $model = null) {
        return $this->manage($me) && $me->id != $model->id && !$model->deleted_at;
    }

    public function isDesigner(User $me, $model = null) {
        return $this->manage($me);
    }

    public function isRdisk(User $me, $model = null) {
        return $this->manage($me);
    }

    public function isCopywriter(User $me, $model = null) {
        return $this->manage($me);
    }

    public function delete(User $me, $model = null) {
        return $this->manage($me) && $me->id != $model->id;
    }

    public function thumbs(User $me, $model = null) {
        return $this->manage($me) || ($me->id == $model->id);
    }

    public function reset(User $me, $model = null) {
        return $this->manage($me) || ($me->id == $model->id);
    }

    public function restore(User $me, $model = null) {
        return $this->manage($me) && $model->deleted_at;
    }
}
