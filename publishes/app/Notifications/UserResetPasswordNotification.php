<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserResetPasswordNotification extends Notification {
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $password) {
        $this->user     = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)->subject('Сброс пароля')
                                ->line('Вам был сброшен пароль на проекте ' . config('app.name'))
                                ->action('Войти', url('/login'))
                                ->line('Используйте для входа')
                                ->line('- email: ' . $this->user->email)
                                ->line('- пароль: ' . $this->password);
    }

    public function toArray($notifiable) {
        return [
            'name' => $this->user->name,
        ];
    }

}
