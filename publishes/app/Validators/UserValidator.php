<?php

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 13.06.17
 * Time: 17:30
 */

namespace App\Validators;

use Larakit\Validator\ValidateBuilder;

class UserValidator extends ValidateBuilder {

    function build() {
        $model = \Request::route('User');
        $this->to('name')
             ->ruleUnique('users', 'name', $model ? $model->id : 0, __('vendor.validation.unique'))
             ->ruleRequired(__('vendor.validation.required'));
        $this->to('email')
             ->ruleUnique('users', 'email', $model ? $model->id : 0, __('vendor.validation.unique'))
             ->ruleRequired(__('vendor.validation.required'));
    }
}
