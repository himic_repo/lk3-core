<?php

namespace App\Http\Controllers\Api;

use App\FormFilters\UserFormFilter;
use App\Http\Resources\UserCollection;
use App\Notifications\UserCreateNotification;
use App\Notifications\UserResetPasswordNotification;
use App\User;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Larakit\Controllers\ApiController;
use Larakit\Helpers\HelperOptions;

class UserController extends ApiController
{

    public function index(Request $request)
    {
        $resources = new UserCollection(UserFormFilter::load());

        return $this->apiResponseSuccess($resources);
    }

    public function options()
    {
        $options = HelperOptions::get(User::orderBy('name')
            ->get(), 'name');

        return $this->apiResponseSuccess($options);
    }

    public function config(Request $request)
    {
        return UserFormFilter::config();
    }

    public function item(User $model, Request $request)
    {
        $resource = new \App\Http\Resources\User($model);

        return $this->apiResponseSuccess($resource);
    }

    public function create(Request $request)
    {
        $data = \Request::all();
        UserValidator::instance()
            ->validate($data)
        ;
        $password         = Str::substr(md5(microtime(true)), 0, 6);
        $data['password'] = \Hash::make($password);
        $user             = User::create($data);
        $user->notify(new UserCreateNotification($user, $password));

        return $this->apiResponseSuccess(null, __('models.User.controller.create'));
    }

    public function reset(User $model)
    {
        $password         = Str::substr(md5(microtime(true)), 0, 6);
        $data['password'] = \Hash::make($password);
        $model->password  = $password;
        $model->save();
        $model->notify(new UserResetPasswordNotification($model, $password));

        return $this->apiResponseInfo(null, __('models.User.controller.reset'));
    }

    public function update(User $model)
    {
        $data = \Request::all();
        UserValidator::instance()
            ->validate($data)
        ;
        $model->fill($data);
        $model->save();

        return $this->apiResponseSuccess(null, __('models.User.controller.update'));
    }

    public function isAdmin(User $model)
    {
        $model->is_admin = !$model->is_admin;
        $model->save();

        if ($model->is_admin) {
            return $this->apiResponseSuccess(null, __('models.User.controller.isAdmin.on'));
        } else {
            return $this->apiResponseInfo(null, __('models.User.controller.isAdmin.off'));
        }

    }

    public function restore(User $model)
    {
        $model->restore();

        return $this->apiResponseSuccess(null, __('models.User.controller.restore'));
    }

    public function delete(User $model)
    {
        $model->delete();

        return $this->apiResponseSuccess(null, __('models.User.controller.delete'));
    }

}
