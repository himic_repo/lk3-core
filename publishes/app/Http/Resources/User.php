<?php

namespace App\Http\Resources;

use \Larakit\Resource\JsonResource;
use Larakit\Resource\TraitResourceColorize;

class User extends JsonResource
{
    use TraitResourceColorize;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function _toArray()
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'email'          => $this->email,
            'is_admin'       => $this->is_admin,
            'ulogin_network' => $this->ulogin_network,
        ];
    }
}
