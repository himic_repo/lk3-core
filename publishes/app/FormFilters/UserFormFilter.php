<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 17.05.17
 * Time: 13:46
 */

namespace App\FormFilters;

use App\User;
use Larakit\FormFilter\FilterBoolean;
use Larakit\FormFilter\FilterLike;
use Larakit\FormFilter\Sorter;
use Larakit\FormFilter\FormFilter;

class UserFormFilter extends FormFilter
{

    protected $per_page = 20;

    static function classModel()
    {
        return User::class;
    }

    function init()
    {
        $this->addFilter(FilterLike::factory('name')
            ->label('models.User.label.name'));
        $this->addFilter(FilterLike::factory('email')
            ->label('E-mail'));
        $this->addFilter(FilterBoolean::factory('is_admin')
            ->label('models.User.label.is_admin'));

        if (me('is_admin')) {
            $this->addFilterTrashed();
        }

        $this->addSorter(Sorter::factory('email')
            ->label('models.User.label.email'));
        $this->addSorter(Sorter::factory('is_admin')
            ->label('models.User.label.is_admin'));
    }
}
