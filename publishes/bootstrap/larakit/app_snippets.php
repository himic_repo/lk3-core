<?php

use \Larakit\Snippet\SnippetConstructor;

try {
    $snippets = SnippetConstructor::instance();
    $snippets->registerGroup('main', ['Главная страница'])
             ->to('main')
             ->registerSnippet('app', 'Название приложения', 'Laravel', SnippetConstructor::TYPE_TEXT);
}
catch (Exception $e) {

}
