<?php

use \Larakit\Snippet\SnippetGroup;
use \Larakit\LkNgSidebar;

\Larakit\Event\Event::listener('larakit:menus', function () {
    if(Gate::allows('manage', SnippetGroup::class)) {
        //snippets
        //    $title = 'vendor.admin.pageAdminSnippet.title';
        //    $icon  = 'fa fa-thumb-tack';
        //    $url   = '/admin/snippets';
        //    $r     = LkNgSidebar::section('admin', 'vendor.admin.pageAdminSnippet.sidebar')
        //        ->item('admin-snippets', $title, $icon, $url);
        //    $r->addLabel('admin-snippets', Snippet::count(), 'default');
        //groups
        $title = 'vendor.admin.pageAdminSnippetGroup.title';
        $icon  = 'fa fa-list';
        $url   = '/admin/snippets';
        $r     = LkNgSidebar::section('admin', 'vendor.admin.pageAdminSnippetGroup.sidebar')
            ->item('admin-snippet', $title, $icon, $url);
        $r->addLabel('admin-snippet', SnippetGroup::count(), 'default');
    }
});
