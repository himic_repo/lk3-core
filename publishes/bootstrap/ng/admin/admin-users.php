<?php
if (Gate::allows('manage', \App\User::class)) {
    $title = 'admin.pageAdminUser.title';
    $icon  = 'fa fa-users';
    $url   = '/admin/users';
    $r     = \Larakit\LkNgSidebar::section('admin', 'admin.pageAdminUser.sidebar')
                                 ->item('admin-users', $title, $icon, $url);
    $r->addLabel('admin-users', \App\User::count(), 'default');
}
