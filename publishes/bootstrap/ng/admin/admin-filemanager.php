<?php

use \Larakit\Snippet\SnippetGroup;
use \Larakit\LkNgSidebar;

\Larakit\Event\Event::listener('larakit:menus', function () {
    if(Gate::allows('filemanager', \App\User::class)) {
        $title = 'Filemanager';
        $icon  = 'fa fa-list';
        $url   = '/admin/filemanager';
        $r     = LkNgSidebar::section('admin', 'Контент')
            ->item('admin-filemanager', $title, $icon, $url);
        $r->addLabel('admin-filemanager', count(\Larakit\Filemanager\FilemanagerConstructor::instance()->all()), 'default');
    }
});
