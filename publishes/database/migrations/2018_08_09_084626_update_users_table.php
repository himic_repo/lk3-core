<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->string('city')
                  ->nullable();
            $table->string('person_id')
                  ->nullable();
            $table->dropUnique('users_email_unique');
            $table->string('login')
                  ->nullable();
            $table->boolean('is_admin')
                  ->default(false);
            $table->string('ulogin_network')
                  ->default('')
                  ->nullable();
            $table->string('ulogin_identity')
                  ->default('')
                  ->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('person_id');
            $table->dropColumn('login');
            $table->dropColumn('is_admin');
            $table->unique('email', 'users_email_unique');
            $table->dropColumn('ulogin_network');
            $table->dropColumn('ulogin_identity');
        });
    }
}
