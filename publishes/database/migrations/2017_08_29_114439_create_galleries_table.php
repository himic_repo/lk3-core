<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')
                  ->nullable()
            ;
            $table->text('desc')
                  ->nullable()
            ;
            $table->string('block');
            $table->string('galleriable_id');
            $table->string('galleriable_type');
            $table->integer('priority');
            $table->timestamps();
        }
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('galleries');
    }
}
