(function () {

    angular
        .module('larakit')
        .component('modalSnippet', {
            templateUrl: 'component.html',
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            controller: Controller
        });

    Controller.$inject = ['LkHttp'];

    function Controller (LkHttp) {
        var $ctrl = this;
        $ctrl.model = {};
        $ctrl.$onInit = function () {
            $ctrl.model = _.clone($ctrl.resolve.model);
        };
        $ctrl.callback = function (response) {
            if ('error' == response.result) {
                $ctrl.errors = response.errors;
            } else {
                $ctrl.errors = {};
                $ctrl.close({});
            }
        };

        $ctrl.update = function () {
            LkHttp
                .post('/api/snippets/' + $ctrl.model.id + '/update', $ctrl.model)
                .then($ctrl.callback);
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
})();
