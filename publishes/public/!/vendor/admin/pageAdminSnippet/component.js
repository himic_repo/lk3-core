(function () {

    angular
        .module('larakit')
        .component('pageAdminSnippet', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkCrud'];

    function Controller(LkCrud) {
        var $ctrl = this;
        //префикс переводов
        $ctrl.prefix_lang = 'vendor.admin.pageAdminGroupSnippet';
        $ctrl.modal_name = 'modalSnippet';
        $ctrl.modal_size = 'full';
        $ctrl.modal_is_modal = true;
        //получаем настройки списка
        LkCrud.init($ctrl, '/api/snippets');
        // Хлебные крошки
        $ctrl.breadcrumbs.add('admin.snippet');
    }
})();
