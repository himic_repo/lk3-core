(function () {

    angular
        .module('larakit')
        .component('pageAdminFilemanager', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkCrud'];

    function Controller(LkCrud) {
        var $ctrl = this;
        //префикс переводов
        $ctrl.prefix_lang = 'vendor.admin.pageAdminFilemanager';
        //получаем настройки списка
        LkCrud.init($ctrl, '/api/filemanager');
        // Хлебные крошки
        $ctrl.breadcrumbs.add('admin.filemanager');

        $ctrl.onCompleteItem = function(fileItem, response, status, headers){
            larakit_toastr(response);
            $ctrl.load();
        }
    }
})();
