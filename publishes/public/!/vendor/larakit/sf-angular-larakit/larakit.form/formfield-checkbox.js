angular
    .module('larakit.form')
    .component('formfieldCheckbox', {
        templateUrl: 'formfield-checkbox.html',
        transclude: true,
        bindings: {
            error: '=',
            desc: '=',
            label: '=',
            model: '='
        }
    });