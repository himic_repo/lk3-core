angular
    .module('larakit.form')
    .component('formfieldPriority', {
        templateUrl: 'formfield-priority.html',
        bindings: {
            error: '=?',
            examples: '=?',
            append: '=?',
            prepend: '=?',
            desc: '=?',
            step: '=?',
            change: '&?',
            model: '='
        }
    });