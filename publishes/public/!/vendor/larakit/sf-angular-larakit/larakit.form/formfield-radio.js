angular
    .module('larakit.form')
    .component('formfieldRadio', {
        templateUrl: 'formfield-radio.html',
        transclude: true,
        bindings: {
            error: '=',
            isVertical: '=?',
            isFull: '=?',
            desc: '=',
            options: '=',
            label: '=',
            model: '='
        }
    });