(function () {

    angular
        .module('lk-angular')
        .factory('LkSidebars', Factory);

    Factory.$inject = ['$cookies', '$http', '$q'];

    function Factory ($cookies, $http, $q) {
        var self = this;
        self.cookie_name_left = 'sidebar_left';
        self.cookie_name_right = 'sidebar_right';
        self.left = false;
        self.right = false;
        self.is_use_right = false;
        self.menu_items = undefined;
        leftSet('true' === localStorage.getItem(self.cookie_name_left));
        //leftSet(false);
        rightSet('true' === localStorage.getItem(self.cookie_name_right));
        //rightSet(false);

        return {
            getUseRight: getUseRight,
            setUseRight: setUseRight,
            getMenu: getMenu,
            rightSet: rightSet,
            leftSet: leftSet,
            leftValue: leftValue,
            leftToggle: leftToggle,
            rightValue: rightValue,
            rightToggle: rightToggle,
            rightNo: rightNo,
            rightYes: rightYes
        };

        function getMenu (is_clear) {
            var deferred = $q.defer();
            if (true == is_clear) {
                self.menu_items = {};
            }
            if ('undefined' === typeof (self.menu_items)) {
                $http
                    .get('/!/lkng/sidebar')
                    .then(function (response) {
                        self.menu_items = response.data;
                        deferred.resolve(self.menu_items);
                        return deferred.promise;
                    }, function () {
                        self.menu_items = {};
                        deferred.resolve(self.menu_items);
                        return deferred.promise;
                    });
            } else {
                deferred.resolve(self.menu_items);
            }
            return deferred.promise;
        }

        function leftSet (val) {
            self.left = val;
            localStorage.setItem(self.cookie_name_left, self.left)
        }

        function leftValue () {
            return self.left;
        }

        function leftToggle () {
            leftSet(!self.left);
        }

        function rightSet (val) {
            self.right = val;
            localStorage.setItem(self.cookie_name_right, self.right)
        }

        function rightValue () {
            return !self.right;
        }

        function rightToggle () {
            rightSet(!self.right);
        }
        function rightNo () {
            rightSet(false);
        }
        function rightYes () {
            rightSet(true);
        }

        function setUseRight (val) {
            self.is_use_right = val;
        }

        function getUseRight () {
            return self.is_use_right;
        }

    }

})();
