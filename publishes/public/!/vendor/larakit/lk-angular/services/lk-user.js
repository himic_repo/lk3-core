(function () {

    angular
        .module('lk-angular')
        .factory('LkUser', Factory);

    Factory.$inject = ['LkHttp', '$q'];

    function Factory (LkHttp, $q) {
        var self = this;
        self.user = {id: -1};
        return {
            me: getAuthUser
        };

        function getAuthUser (is_clear) {
            var deferred = $q.defer();
            if (true == is_clear) {
                self.user = {id: -1};
            }
            if (-1 === self.user.id) {
                LkHttp
                    .get('/api/me')
                    .then(function (response) {
                        self.user = response.data;
                        deferred.resolve(self.user);
                        return deferred.promise;
                    }, function () {
                        self.user = {id: -1};
                        deferred.resolve(self.user);
                        return deferred.promise;
                    });
            } else {
                deferred.resolve(self.user);
            }
            return deferred.promise;
        }
    }
})();
