(function () {

    angular
        .module('lk-angular')
        .factory('LkLocales', Factory);

    Factory.$inject = ['LkHttp', '$q'];

    function Factory (LkHttp, $q) {
        var $ctrl = this;
        $ctrl.data = null;
        $ctrl.current = {};
        return {
            get: get,
        };

        function get (is_clear) {
            var deferred = $q.defer();
            if (true == is_clear) {
                $ctrl.data = null;
            }
            if (null === $ctrl.data) {
                $ctrl.data = false;
                LkHttp.get('/api/system/locales')
                    .then(function (response) {
                        $ctrl.data = response.data;
                        deferred.resolve($ctrl.data);
                        return deferred.promise;
                    });
            } else {
                deferred.resolve($ctrl.data);
            }
            return deferred.promise;
        }

    }

})();
