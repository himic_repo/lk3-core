(function () {

        angular
            .module('lk-angular')
            .factory('LkCrud', Factory);

        Factory.$inject = ['LkSidebars', 'BreadCrumbs', 'LkHttp', 'LkEvent', 'LkList', '$timeout', '$filter', '$location', 'LkConfig'];

        function Factory (LkSidebars, BreadCrumbs, LkHttp, LkEvent, LkList, $timeout, $filter, $location, LkConfig) {
            return {
                init: function ($ctrl, api_url) {
                    $ctrl.sidebar = LkSidebars;
                    $ctrl.breadcrumbs = BreadCrumbs;
                    $ctrl.breadcrumbs.clear();
                    $ctrl.api_url = api_url;
                    /**
                     * Данные для формирования списка
                     */
                    $ctrl.params = {
                        filters: {},
                        page: 1,
                        order_field: null,
                        order_desc: false
                    };

                    /**
                     * Элементы списка
                     */
                    $ctrl.data = {};
                    $ctrl.details = {};

                    /**
                     * Развернутые элементы списка
                     * @type {{}}
                     */
                    $ctrl.listOpened = {};
                    /**
                     * Выделенные элементы списка
                     * @type {{}}
                     */
                    $ctrl.listChecked = {};
                    /**
                     * Настройки фильтров
                     * Настройки сортировщика
                     */
                    $ctrl.config = {
                        filters: [],
                        sorters: []
                    };
                    LkConfig
                        .get($ctrl.api_url + '/config')
                        .then(function (response) {
                            var build_filters = {};
                            $ctrl.config = response;

                            /**
                             * Поле сортировки
                             */
                            var val = $location.search()._order_field;
                            $ctrl.params.order_field = val ? val : response.sorter_default;
                            /**
                             * Направление сортировки
                             */
                            var val = $location.search()._order_desc;
                            $ctrl.params.order_desc = val ? ('true' === val) : response.sorter_desc;

                            //var val = $location.search()._page;
                            //if (val) {
                            //    $ctrl.params.page = val;
                            //}

                            //инициализацию фильтров
                            _.each(response.filters, function (filter) {
                                switch (true) {
                                    case (-1 !== ['like', 'equal', 'boolean'].indexOf(filter.type)):
                                        var val = $location.search()[filter.name];
                                        if (val) {
                                            build_filters[filter.name] = val;
                                        }
                                        break;
                                    case (-1 !== ['checkbox', 'button'].indexOf(filter.type)):
                                        var val = $location.search()[filter.name];
                                        if (val != undefined) {
                                            val = val.split(',');
                                            var value = {};
                                            _.each(val, function (v) {
                                                value[v] = true;
                                            });
                                            build_filters[filter.name] = value;
                                        }
                                        break;
                                    case (-1 !== ['select2'].indexOf(filter.type)):
                                        var val = $location.search()[filter.name];
                                        if (val != undefined) {
                                            val = val.split(',');
                                            _.each(val, function (v, k) {
                                                var parsed = parseInt(v);
                                                if (!isNaN(parsed)) {
                                                    val[k] = parsed;
                                                }
                                            });
                                            var value = [];
                                            _.each(filter.options, function (item) {
                                                if (-1 !== val.indexOf(item.id)) {
                                                    value.push(item);
                                                }
                                            });
                                            build_filters[filter.name] = value;
                                        }
                                        break;
                                }
                            });
                            $ctrl.params.filters = build_filters;

                            //первоначальное наполнение
                            $ctrl.load();
                        });
                    $ctrl.load = function (is_clear_filters, page) {
                        if (true == is_clear_filters) {
                            $ctrl.params.filters = {};
                        }
                        if (undefined != page) {
                            $ctrl.params.page = page;
                        }
                        $timeout(function () {
                            LkHttp
                                .post($ctrl.api_url, $ctrl.params)
                                .then(function (response) {
                                    $ctrl.data = response.data;
                                }, 100);
                            LkEvent.fire('sidebar-reload');
                        });
                    };

                    $ctrl.actionRemove = function (id) {
                        var key = $ctrl.prefix_lang + '.confirm.delete';
                        var message = $filter('translate')(key);
                        if (key === message || confirm(message)) {
                            LkHttp.post($ctrl.api_url + '/' + id + '/delete')
                                .then(function (response) {
                                    if ('success' == response.result) {
                                        $ctrl.load();
                                    }
                                });
                        }
                    };
                    $ctrl.actionToggle = function (id, field, message) {
                        if (!message || confirm($filter('translate')(message))) {
                            LkHttp.post($ctrl.api_url + '/' + id + '/' + field)
                                .then(function (response) {
                                    $ctrl.load();
                                });
                        }
                    };
                    $ctrl.actionRestore = function (id) {
                        var key = $ctrl.prefix_lang + '.confirm.delete';
                        var message = $filter('translate')(key);
                        if (key === message || confirm(message)) {
                            LkHttp.post($ctrl.api_url + '/' + id + '/restore')
                                .then(function (response) {
                                    $ctrl.load();
                                });
                        }
                    };
                    $ctrl.actionEdit = function (model) {
                        model = _.cloneDeep(model);
                        LkList.actionEdit(model, $ctrl.modal_name, function () {
                            $ctrl.load();
                        }, $ctrl.modal_size, $ctrl.modal_is_modal);
                    };
                }
            };
        }
    }
)();
