(function () {

    angular
        .module('lk-angular')
        .factory('LkTranslateOriginal', Factory);

    Factory.$inject = [];

    function Factory () {
        var $ctrl = this;
        $ctrl.value = ('true' == localStorage.getItem('LkTranslateOriginal') ? true : false);

        return {
            toggle: toggle,
            get: get,
        };

        function toggle () {
            $ctrl.value = !$ctrl.value;
            console.log($ctrl.value);
            localStorage.setItem('LkTranslateOriginal', $ctrl.value);
        }

        function get () {
            return $ctrl.value;
        }

    }

})();
