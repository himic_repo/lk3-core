(function () {

    angular
        .module('lk-angular')
        .factory('BreadCrumbs', Factory);

    Factory.$inject = ['$route', 'namedRouteService', '$translate', '$routeParams'];

    function Factory($route, namedRouteService, $translate, $routeParams) {
        var replaces = {}, page_routes = [], current_page_title;
        return {
            page: getPage,
            clear: clear,
            addReplace: addReplace,
            setReplaces: setReplaces,
            currentPageTitle: currentPageTitle,
            getReplaces: getReplaces,
            last: last,
            add: add,
            all: all
        };

        function last() {
            if (!page_routes) {
                return '';
            }
            if (undefined != page_routes[page_routes.length - 1]) {
                return page_routes[page_routes.length - 1];
            }
            return {
                title: '',
                subtitle: '',
                icon: ''
            };
        }

        function clear() {
            page_routes = [];
        }

        function all() {
            return page_routes;
        }

        function setReplaces(val) {
            replaces = val;
        }

        function addReplace(k, v) {
            replaces[k] = v;
        }

        function getReplaces() {
            return replaces;
        }

        function currentPageTitle() {
            return $translate(current_page_title, replaces);
        }

        function getPage() {
            return page;
        }

        function add(name, params) {
            var item = {};
            if (undefined == params) {
                params = {};
            }
            var url = namedRouteService.reverse(name, params),
                route;
            _.each($route.routes, function (o) {
                if (o.name == name) {
                    route = o;
                }
            });
            current_page_title = route.title;
            item = {
                title: route.title,
                subtitle: route.subtitle,
                url: url,
                icon: route.icon
            };
            page_routes.push(item);
            $translate(route.title, replaces).then(function (v) {
                $('title').html(v);
            });
            return item;
        }
    }
})();
