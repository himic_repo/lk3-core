(function () {

        angular
            .module('lk-angular')
            .factory('LkConfig', Factory);

        Factory.$inject = ['LkHttp', '$q'];

        function Factory (LkHttp, $q) {
            var $ctrl = this;
            $ctrl.data = {};
            return {
                get: get
            };

            function get (url) {
                var deferred = $q.defer();
                if (!$ctrl.data) {
                    $ctrl.data = {};
                }
                if (!$ctrl.data[url]) {
                    //console.log(1);
                    LkHttp
                        .get(url)
                        .then(function (response) {
                            $ctrl.data[url] = response;
                            deferred.resolve($ctrl.data[url]);
                        }, function () {
                            $ctrl.data[url] = null;
                            deferred.resolve($ctrl.data[url]);
                        });
                } else {
                    deferred.resolve($ctrl.data[url]);
                }
                return deferred.promise;
            }
        }
    }
)();
