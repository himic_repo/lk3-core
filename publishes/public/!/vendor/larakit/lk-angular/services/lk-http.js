(function () {

    angular
        .module('lk-angular')
        .factory('LkHttp', Factory);

    Factory.$inject = ['$http', '$q', 'LkEvent'];

    function Factory ($http, $q, LkEvent) {
        var self = this;
        self.cntRequest = 0;

        return {
            get: get,
            post: post,
        };

        function checkAuth (response) {
            if (response.auth) {
                location.href = '/login?return=' + encodeURI(location.href);
                return false;
            }
            if (response.refresh) {
                location.href = location.href;
                return false;
            }
        }

        function get (url) {
            self.cntRequest++;
            LkEvent.fire('http-request', self.cntRequest);
            console.log(url);
            var deferred = $q.defer();
            $http
                .get(url)
                .then(function (response) {
                    self.cntRequest--;
                    LkEvent.fire('http-request', self.cntRequest);
                    larakit_toastr(response.data);
                    checkAuth(response.data);
                    deferred.resolve(response.data);
                }, function (response) {
                    self.cntRequest--;
                    LkEvent.fire('http-request', self.cntRequest);
                    deferred.resolve({});
                });
            return deferred.promise;
        }

        function post (url, params) {
            self.cntRequest++;
            LkEvent.fire('http-request', self.cntRequest);
            console.log(url);
            //console.log(self.cntRequest);
            var deferred = $q.defer();
            $http
                .post(url, params)
                .then(function (response) {
                    self.cntRequest--;
                    LkEvent.fire('http-request', self.cntRequest);
                    larakit_toastr(response.data);
                    checkAuth(response.data);
                    deferred.resolve(response.data);
                }, function (response) {
                    self.cntRequest--;
                    LkEvent.fire('http-request', self.cntRequest);
                    deferred.resolve({});
                });
            return deferred.promise;
        }
    }
})();
