(function () {

    angular
        .module('lk-angular')
        .factory('LkList', Factory);

    Factory.$inject = ['LkHttp', 'LkAlerts', '$timeout', '$uibModal', 'LkEvent', '$location'];

    function Factory (LkHttp, LkAlerts, $timeout, $uibModal, LkEvent, $location) {
        var self = this;
        self.keyName = 'id';

        return {
            ids: ids,
            config: config,
            actionRemove: actionRemove,
            actionRestore: actionRestore,
            actionLoad: actionLoad,
            actionEdit: actionEdit
        };

        function ids (models) {
            var ids = [];
            _.each(models, function (model) {
                if (true == model.is_checkbox_bulk) {
                    ids.push(model.id);
                }
            });
            return ids;
        }

        function config ($ctrl, url_config) {
            /**
             * Данные для формирования списка
             */
            $ctrl.params = {
                filters: {},
                page: 1,
                order_field: null
            };

            /**
             * Элементы списка
             */
            $ctrl.data = {};

            /**
             * Развернутые элементы списка
             * @type {{}}
             */
            $ctrl.listOpened = {};
            /**
             * Выделенные элементы списка
             * @type {{}}
             */
            $ctrl.listChecked = {};
            /**
             * Настройки фильтров
             * Настройки сортировщика
             */
            $ctrl.config = {
                filters: [],
                sorters: []
            };
            LkHttp
                .post(url_config)
                .then(function (response) {
                    $ctrl.config = response;
                    $ctrl.params.order_field = response.sorter_default;
                    $ctrl.params.order_desc = response.sorter_desc;
                    console.log(response);
                    //первоначальное наполнение
                    $ctrl.load();
                });
        }

        function actionLoad($ctrl, url_load, is_clear_filters, page) {
            if (true == is_clear_filters) {
                $ctrl.params.filters = {};
                $location.search({});
            }
            if (undefined != page) {
                $ctrl.params.page = page;
            }
            $timeout(function () {
                LkHttp
                    .post(url_load, $ctrl.params)
                    .then(function (response) {
                        $ctrl.data = response.data;
                    }, 100);
                LkEvent.fire('sidebar-reload');
            });
        }

        function actionRemove (model, $ctrl, confirm_message, url, callback) {
            if (undefined == url) {
                url = $ctrl.url_delete;
            }
            if (confirm(confirm_message)) {
                LkHttp.post(url, model)
                    .then(function (response) {
                            if ('success' == response.result) {
                                if (callback) {
                                    callback.call($ctrl, response);
                                }
                            }
                        }
                    );
            }

        }

        function actionRestore (model, $ctrl, confirm_message, url, callback) {
            if (undefined == url) {
                url = $ctrl.url_restore;
            }
            if (url) {
                if (confirm(confirm_message)) {
                    LkHttp.post(url, model)
                        .then(function (response) {
                                if ('success' == response.result) {
                                    actionLoad($ctrl);
                                    if (callback) {
                                        callback.call($ctrl);
                                    }
                                }
                            }
                        );
                }
            }
        }

        function actionEdit (model, componentForm, callback, size, is_modal) {
            if (undefined == size) {
                size = 'lg';
            }
            var config = {
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: componentForm,
                size: size,
                resolve: {
                    model: model
                }
            };
            if (undefined != is_modal) {
                config.backdrop = 'static';
                config.keyboard = false;
            }
            var modalInstance = $uibModal.open(config);
            modalInstance.result.then(function (o) {
                callback.call(modalInstance, o);
            }, function () {
                // console.info('modal-component dismissed at: ' + new Date());
            });
        }

    }
})();
