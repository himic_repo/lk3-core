(function () {

    angular
        .module('lk-angular')
        .factory('unsafeFilter', Factory);

    Factory.$inject = ['$sce'];

    function Factory ($sce) {
        return $sce.trustAsHtml;
    }

})();
