(function () {

    angular
        .module('larakit')
        .component('adminlteHeader', {
            templateUrl: 'component.html',
            bindings: {
                home: '=?',
                logoLg: '=?',
                logoMini: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['LkSidebars', 'LkUser', '$location', '$http', '$translate', '$locale'];

    function Controller (LkSidebars, LkUser, $location, $http, $translate, $locale) {
        var $ctrl = this;
        $ctrl.me = {};
        $ctrl.menu_items = [];

        $ctrl.leftToggle = function () {
            console.log('$ctrl.leftToggle');
            LkSidebars.leftToggle();
        };
        $ctrl.rightToggle = function () {
            console.log('$ctrl.rightToggle');
            LkSidebars.rightToggle();
            return false;
        };
        LkUser.me().then(function (data) {
            $ctrl.me = data;
            if (undefined == $ctrl.me.id) {
                $location.href = '/?no_auth';
            }
        });
        $ctrl.load = function () {
            $http
                .get('/!/lkng/header')
                .then(function (response) {
                    $ctrl.menu_items = response.data.items;
                    $ctrl.langs = response.data.langs;
                    $ctrl.lang = response.data.lang;
                });
        };
        $ctrl.load();
        $ctrl.logout = function () {
            $http.post('/logout').then(function (response) {
                if ('success' == response.data.result) {
                    larakit_toastr(response.data);
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 1000);
                }

            })
        };

        $ctrl.currentLanguage = function () {
            return $translate.use();
        };

        $ctrl.changeLanguage = function (langKey, $event) {
            $translate.use(langKey);
            checkFaLanguage(langKey);
            $event.preventDefault();
        };

        function checkFaLanguage (lang) {
            if (lang === 'fa') {
                document.body.classList.add('lang-fa');
                document.documentElement.setAttribute('dir', 'rtl');
            } else {
                document.body.classList.remove('lang-fa');
                document.documentElement.setAttribute('dir', 'ltr');
            }
            switch (lang) {
                case 'fa':
                    $locale.DATETIME_FORMATS.SHORTDAY = [
                        "\u06cc\u06a9\u0634\u0646\u0628\u0647",
                        "\u062f\u0648\u0634\u0646\u0628\u0647",
                        "\u0633\u0647\u200c\u0634\u0646\u0628\u0647",
                        "\u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647",
                        "\u067e\u0646\u062c\u0634\u0646\u0628\u0647",
                        "\u062c\u0645\u0639\u0647",
                        "\u0634\u0646\u0628\u0647"
                    ];
                    break;
                case 'en':
                    $locale.DATETIME_FORMATS.SHORTDAY = [
                        "Sun",
                        "Mon",
                        "Tue",
                        "Wed",
                        "Thu",
                        "Fri",
                        "Sat"
                    ];
                    break;
                default:
                    $locale.DATETIME_FORMATS.SHORTDAY = [
                        "\u0432\u0441",
                        "\u043f\u043d",
                        "\u0432\u0442",
                        "\u0441\u0440",
                        "\u0447\u0442",
                        "\u043f\u0442",
                        "\u0441\u0431"
                    ];
                    break;
            }

        }

        checkFaLanguage($ctrl.currentLanguage());
    }

})();