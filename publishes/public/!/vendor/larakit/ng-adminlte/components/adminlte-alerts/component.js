(function () {

    angular
        .module('larakit')
        .component('adminlteAlerts', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkAlerts'];

    function Controller(LkAlerts) {
        var $ctrl = this;
        $ctrl.alerts = LkAlerts.get();
        $ctrl.close = function (index) {
            LkAlerts.close(index);
        }
    }

})();