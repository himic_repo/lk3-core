(function () {

    angular
        .module('larakit')
        .component('adminlteListItemOpened', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                listOpened: '='
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller() {
        var $ctrl = this;
    }

})();