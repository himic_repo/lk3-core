(function () {

    angular
        .module('larakit')
        .component('adminlteSidebarRight', {
            templateUrl: 'component.html',
            transclude: true,
            controller: Controller
        });

    Controller.$inject = ['LkSidebars', 'hotkeys'];

    function Controller (LkSidebars, hotkeys) {
        var $ctrl = this;
        hotkeys.add({
            combo: 'ctrl+right',
            description: 'Description goes here',
            callback: function (event, hotkey) {
                LkSidebars.rightToggle();
            }
        });
        $ctrl.$postLink = function () {
            LkSidebars.setUseRight(true);
        };
        $ctrl.$onDestroy = function () {
            LkSidebars.setUseRight(false);
        };

    }

})();