(function () {

    angular
        .module('larakit')
        .component('adminlteHelpbox', {
            templateUrl: 'component.html',
            transclude:true,
            bindings: {
                header: '=?'
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller() {
        var $ctrl = this;
    }

})();