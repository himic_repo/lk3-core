(function () {

    angular
        .module('larakit')
        .component('adminlteListItemBulk', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                listChecked: '=',
                isHideId: '=?'
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller() {
        var $ctrl = this;
    }

})();