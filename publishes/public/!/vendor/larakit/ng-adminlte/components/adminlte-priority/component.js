(function () {

    angular
        .module('larakit')
        .component('adminltePriority', {
            templateUrl: 'component.html',
            bindings: {
                model: '='
            }
        });
})();