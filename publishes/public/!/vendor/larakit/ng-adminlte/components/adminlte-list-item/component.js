(function () {

    angular
        .module('larakit')
        .component('adminlteListItem', {
            templateUrl: 'component.html',
            transclude: {
                'body': '?itemBody',
                'header': '?itemHeader',
                'buttons': '?itemButtons'
            },
            bindings: {
                model: '=',
                color: '=',
                current: '=?',
                currentMode: '=?',
                half: '=?',
                wLeft: '=?',
                wRight: '=?',
                listOpened: '=',
                listChecked: '=',
                isHideFooter: '=?',
                isHideId: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$transclude'];

    function Controller($transclude) {
        var $ctrl = this;
        $ctrl.isHideToggle = !$transclude.isSlotFilled('body');
        $ctrl.$postLink = function(){
            if(!$ctrl.wLeft){
                $ctrl.wLeft = '80px';
            }
            if(!$ctrl.wRight){
                $ctrl.wRight = '50px';
            }
        }
    }

})();