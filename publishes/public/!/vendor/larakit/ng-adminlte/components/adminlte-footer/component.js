(function () {

    angular
        .module('larakit')
        .component('adminlteFooter', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['$http'];

    function Controller($http) {
        var $ctrl = this;
        $ctrl.load = function () {
            $http
                .get('/!/adminlte/footer', {
                    cache:true
                })
                .then(function (response) {
                    $ctrl.data = response.data;
                });
        };
        $ctrl.load();

    }

})();