angular
    .module('larakit')
    .component('formfieldText', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=?',
            examples: '=?',
            append: '=?',
            max: '=?',
            isCalc: '=?',
            prepend: '=?',
            desc: '=?',
            change: '&?',
            model: '=',
            isExampleAppend: '=?'
        },
        controller: function () {
            var self = this;
            self.onChange = function () {
                if (self.change) {
                    self.change();
                }
            };
        }
    });
