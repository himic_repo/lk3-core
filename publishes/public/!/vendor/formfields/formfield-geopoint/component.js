angular
    .module('larakit')
    .component('formfieldGeopoint', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=?',
            desc: '=?',
            model: '='
        },
        controller: function ($http, $timeout, $scope, LkHttp, LkLocales) {
            var $ctrl = this;
            $ctrl.coords = [0, 0];
            $ctrl.cnt_find = 0;
            $ctrl.geoObject = {
                "geometry": {
                    "coordinates": [
                        0, 0
                    ],
                    "type": "Point"
                }
            };
            var map;
            $ctrl.search = '';
            $ctrl.afterMapInit = function ($map) {
                map = $map;

                if (!$ctrl.model) {
                    $ctrl.model = ymaps.geolocation.longitude + ' ' + ymaps.geolocation.latitude;
                    $ctrl.onSetValue();
                }
                $timeout(function () {
                    $ctrl.onSetValue();
                }, 1000);
            };

            $ctrl.onSetValue = function () {
                if (!$ctrl.model) {
                    $ctrl.coords = [0, 0];
                    $ctrl.geoObject = {
                        "geometry": {
                            "coordinates": [
                                0, 0
                            ],
                            "type": "Point"
                        }
                    };
                } else {
                    var val = $ctrl.model.split(' ');
                    $ctrl.coords = [
                        parseFloat(val[0]),
                        parseFloat(val[1])
                    ];
                }
                if (0 == Object.keys($ctrl.coords).length) {
                    return null;
                }
                // console.log('onSetValue', $ctrl.coords);
                $ctrl.geoObject = {
                    geometry: {
                        coordinates: $ctrl.coords,
                        type: 'Point'
                    }
                };
                $ctrl.center();
            };
            $ctrl.scale = function (zoom) {
                // console.log(map);
                map.setCenter($ctrl.coords, zoom);
            };
            $ctrl.center = function () {
                if (0 == Object.keys($ctrl.coords).length) {
                    return null;
                }
                map.panTo($ctrl.coords, {
                    // Задержка перед началом перемещения.
                    delay: 1500
                });
            };

            $ctrl.click = function (e) {
                var coords = e.get('coords');
                // console.log('coords', coords);
                $ctrl.model = coords.join(' ');
                $timeout(function () {
                    $ctrl.onSetValue();
                }, 1000);
            };
            $ctrl.find_process = false;

            $ctrl.find = function () {
                if (!$ctrl.search) {
                    return false;
                }
                LkLocales.get().then(function (response) {
                    $ctrl.lang_slug = response.lang_slug;
                });
                var url = 'https://geocode-maps.yandex.ru/1.x/?apikey=' + $('html').attr('yamap-apikey') + '&format=json&lang='+$ctrl.lang_slug+'&geocode=';
                $ctrl.cnt_find++;
                $ctrl.find_process = true;
                $.get(url + $ctrl.search).then(function (response) {
                    $ctrl.results = response.response.GeoObjectCollection.featureMember;
                    if (1 == $ctrl.results.length) {
                        $ctrl.apply(response.response.GeoObjectCollection.featureMember[0])
                    }
                    $ctrl.find_process = false;
                    $scope.$apply();
                });
                $.get(url + $ctrl.search, function (response) {
                    //$ctrl.results = response.response.GeoObjectCollection.featureMember;
                    //if (1 == $ctrl.results.length) {
                    //    $ctrl.apply(response.response.GeoObjectCollection.featureMember[0])
                    //}
                    //$ctrl.find_process = false;
                    //alert(123);
                });
                //$http
                //    .get(url + $ctrl.search)
                //    .then(function (response) {
                //        $ctrl.results = response.data.response.GeoObjectCollection.featureMember;
                //        if (1 == $ctrl.results.length) {
                //            $ctrl.apply(response.data.response.GeoObjectCollection.featureMember[0])
                //        }
                //        $ctrl.find_process = false;
                //    });
                //$http.defaults.headers.common['X-CSRF-TOKEN'] = $('html').attr('_token');
            };

            $ctrl.findByKey = function ($event) {
                if (13 == $event.keyCode) {
                    $ctrl.find();
                }
            }
            $ctrl.apply = function (result) {
                $ctrl.model = result.GeoObject.Point.pos;
                // console.log('$ctrl.model', $ctrl.model);
                $ctrl.onSetValue();
            };

        }
    });
