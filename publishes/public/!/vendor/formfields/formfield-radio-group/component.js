angular
    .module('larakit')
    .component('formfieldRadioGroup', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=',
            isInline: '=?',
            desc: '=',
            type: '=?',
            options: '=',
            label: '=',
            model: '='
        },
        controller: function () {
            var $ctrl = this;
            $ctrl.name = Math.random().toString(36).substring(7);
            $ctrl.$postLink = function () {
                if (!$ctrl.type) {
                    $ctrl.type = 'primary';
                }
            };
            $ctrl.set = function (value) {
                console.log(value);
                $ctrl.model = value;
            }
        }
    });
