(function () {
    angular
        .module('larakit')
        .component('formfieldDate', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                error: '=',
                desc: '=',
                label: '=',
                max: '=',
                min: '=',
                change: '&?',
                model: '='
            },
            controller: Controller
        })
        .directive('datetimepickerNeutralTimezone', function () {
            return {
                restrict: 'A',
                priority: 1,
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    ctrl.$formatters.push(function (value) {
                        return value ? value : null;
                    });

                    ctrl.$parsers.push(function (value) {
                        return value ? value : null;
                    });
                }
            };
        });

    Controller.$inject = [];

    function Controller () {
        var $ctrl = this;
        $ctrl.opened = false;
        $ctrl.dateOptions = {
            formatYear: 'yy',
            maxDate: (undefined !== $ctrl.max) ? new Date($ctrl.max) : null,
            minDate: (undefined !== $ctrl.min) ? new Date($ctrl.min) : null,
            startingDay: 1
        };
        $ctrl.$postLink = function () {
            moment.locale('ru');
        };

        //$ctrl.format = 'dd.MM.yyyy';
        $ctrl.format = 'yyyy-MM-dd';
        $ctrl.clear = function () {

        };
        $ctrl.onChange = function () {
            if ($ctrl.change) {
                $ctrl.change();
            }
        };
    }

})();
