angular
    .module('larakit')
    .component('formfieldPriority', {
        templateUrl: 'component.html',
        bindings: {
            error: '=?',
            examples: '=?',
            append: '=?',
            prepend: '=?',
            desc: '=?',
            step: '=?',
            change: '&?',
            model: '='
        }
    });
