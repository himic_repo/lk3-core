(function () {
    'use strict';

    angular
        .module('larakit')
        .component('formfieldAparat', {
            templateUrl: 'component.html',
            controller: aparatSearchController,
            transclude: true,
            bindings: {
                model: '=',
                errors: '=?',
            }
        });

    aparatSearchController.$inject = ['$element', '$http', '$sce'];

    function aparatSearchController ($element, $http, $sce) {
        var $ctrl = this;
        $ctrl.aparatPaste = function (e, $index) {
            var value = e.originalEvent.clipboardData.getData('text/plain');
            value = value.replace('https://www.aparat.com/v/','');
            $ctrl.model[$index] = value;
            e.preventDefault();
        };
        $ctrl.aparatAdd = function () {
            if (!$ctrl.model) {
                $ctrl.model = [];
            }
            if (-1 == $ctrl.model.indexOf('')) {
                $ctrl.model.push('');
            }
            //$ctrl.model[(Date.now() % 1000)] = '';
        };
        $ctrl.aparatDelete = function (k) {
            $ctrl.model.splice(k, 1);
        };
        $ctrl.aparatUp = function (k) {
            if (k > 0) {
                var val = $ctrl.model[k];
                $ctrl.model[k] = $ctrl.model[k - 1];
                $ctrl.model[k - 1] = val;
            }
        };
        $ctrl.aparatDown = function (k) {
            if ((k + 1) < $ctrl.model.length) {
                var val = $ctrl.model[k];
                $ctrl.model[k] = $ctrl.model[k + 1];
                $ctrl.model[k + 1] = val;
            }
        };
    }
})();
