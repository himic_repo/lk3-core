angular
    .module('larakit')
    .component('formfieldCheckboxGroup', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            options: '=',
            error: '=',
            desc: '=',
            type: '=?',
            label: '=',
            isInline: '=?',
            init: '=',
            model: '='
        },
        controller: function ($scope, $timeout) {
            var $ctrl = this;
            $ctrl.inner = {};
            $ctrl.initData = function () {
                $ctrl.inner = {};
                //получим данные для инициализации
                if (undefined === $ctrl.model) {
                    $ctrl.model = '';
                }
                var values = $ctrl.model.split(',');
                //пройдемся по ним
                _.each(values, function (id) {
                        var idx = _.findIndex($ctrl.options, function (option) {
                            return option.id === id;
                        });
                        console.log(idx);
                        //проверим их наличие в списке опций
                        if (-1 !== idx) {
                            //если есть - добавим в текущую модель
                            $ctrl.inner[id] = true;
                        }
                    }
                );
            }
            ;
            $scope.$watch('$ctrl.model', function () {
                $timeout(function () {
                    $ctrl.initData();
                }, 1);
            });
            $ctrl.$postLink = function () {
                $ctrl.id = '_' + Math.random().toString(36).substr(2, 9);
                if (!$ctrl.type) {
                    $ctrl.type = 'primary';
                }
            };
            $ctrl.isChecked = function (key) {
                return $ctrl.inner[key]
            };
            $ctrl.set = function (key) {
                if ($ctrl.inner[key]) {
                    delete $ctrl.inner[key];
                } else {
                    $ctrl.inner[key] = true;
                }
                $ctrl.model = Object.keys($ctrl.inner).join(',');
            }
        }
    })
;
