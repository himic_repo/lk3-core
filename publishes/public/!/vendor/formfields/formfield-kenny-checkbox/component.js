angular
    .module('larakit')
    .component('formfieldKennyCheckbox', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=',
            desc: '=',
            type: '=?',
            label: '=',
            model: '='
        },
        controller: function () {
            var $ctrl = this;
            $ctrl.$postLink = function () {
                $ctrl.id = '_' + Math.random().toString(36).substr(2, 9);
                if (!$ctrl.model) {
                    $ctrl.model = false;
                }
                if (!$ctrl.type) {
                    $ctrl.type = 'primary';
                }
            }
        }
    });
