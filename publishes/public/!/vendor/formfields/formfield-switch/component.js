angular
    .module('larakit')
    .component('formfieldSwitch', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=?',
            switchOn: '=?',
            switchOff: '=?',
            switchInverse: '=?',
            switchClass: '=?',
            desc: '=',
            change: '&?',
            label: '=',
            model: '='
        },
        controller: function () {
            var self = this;
            self.onChange = function () {
                if (self.change) {
                    self.change();
                }
            };
            self.$onInit = function () {
                self.switchOn = (undefined == self.switchOn) ? 'vendor.rtdl.formfield-switch.on' : self.switchOn;
                self.switchOff = (undefined == self.switchOff) ? 'vendor.rtdl.formfield-switch.off' : self.switchOff;
                self.switchClass = (undefined == self.switchClass) ? 'wide' : self.switchClass;
            };
        }
    });
