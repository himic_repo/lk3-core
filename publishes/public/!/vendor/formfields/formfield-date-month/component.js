(function () {
    angular
        .module('larakit')
        .component('formfieldDateMonth', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                error: '=',
                desc: '=',
                label: '=',
                max: '=',
                min: '=',
                change: '&?',
                model: '='
            },
            controller: Controller
        })
        .directive('datetimepickerNeutralTimezone', function () {
            return {
                restrict: 'A',
                priority: 1,
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    ctrl.$formatters.push(function (value) {
                        if (value) {
                            var date = new Date(Date.parse(value));
                            date = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
                            return date;
                        } else {
                            return null;
                        }
                    });

                    ctrl.$parsers.push(function (value) {
                        if (value) {
                            var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                            return date;
                        } else {
                            return null;
                        }
                    });
                }
            };
        });

    Controller.$inject = ['LkEvent', '$locale', '$filter'];

    function Controller (LkEvent, $locale, $filter) {
        var ctrl = this;
        ctrl.opened = false;
        ctrl.dateOptions = {
            formatYear: 'yy',
            datepickerMode:'month',
            minMode:'month',
            minViewMode: 1,
            maxDate: (undefined !== ctrl.max) ? new Date(ctrl.max) : null,
            minDate: (undefined !== ctrl.min) ? new Date(ctrl.min) : null,
            startingDay: 1
        };
        ctrl.applyLocale = function () {
            $locale.DATETIME_FORMATS.SHORTDAY = [
                $filter('translate')('vendor.date_formats.shortday.sunday'),
                $filter('translate')('vendor.date_formats.shortday.monday'),
                $filter('translate')('vendor.date_formats.shortday.tuesday'),
                $filter('translate')('vendor.date_formats.shortday.wednesday'),
                $filter('translate')('vendor.date_formats.shortday.thursday'),
                $filter('translate')('vendor.date_formats.shortday.friday'),
                $filter('translate')('vendor.date_formats.shortday.saturday')
            ];
            $locale.DATETIME_FORMATS.SHORTMONTH = [
                $filter('translate')('vendor.date_formats.shortmonth.january'),
                $filter('translate')('vendor.date_formats.shortmonth.february'),
                $filter('translate')('vendor.date_formats.shortmonth.march'),
                $filter('translate')('vendor.date_formats.shortmonth.april'),
                $filter('translate')('vendor.date_formats.shortmonth.may'),
                $filter('translate')('vendor.date_formats.shortmonth.june'),
                $filter('translate')('vendor.date_formats.shortmonth.july'),
                $filter('translate')('vendor.date_formats.shortmonth.august'),
                $filter('translate')('vendor.date_formats.shortmonth.september'),
                $filter('translate')('vendor.date_formats.shortmonth.october'),
                $filter('translate')('vendor.date_formats.shortmonth.november'),
                $filter('translate')('vendor.date_formats.shortmonth.december'),
            ];
            $locale.DATETIME_FORMATS.STANDALONEMONTH = [
                $filter('translate')('vendor.date_formats.shortmonth.january'),
                $filter('translate')('vendor.date_formats.shortmonth.february'),
                $filter('translate')('vendor.date_formats.shortmonth.march'),
                $filter('translate')('vendor.date_formats.shortmonth.april'),
                $filter('translate')('vendor.date_formats.shortmonth.may'),
                $filter('translate')('vendor.date_formats.shortmonth.june'),
                $filter('translate')('vendor.date_formats.shortmonth.july'),
                $filter('translate')('vendor.date_formats.shortmonth.august'),
                $filter('translate')('vendor.date_formats.shortmonth.september'),
                $filter('translate')('vendor.date_formats.shortmonth.october'),
                $filter('translate')('vendor.date_formats.shortmonth.november'),
                $filter('translate')('vendor.date_formats.shortmonth.december'),
            ];
            $locale.DATETIME_FORMATS.MONTH = [
                $filter('translate')('vendor.date_formats.month.january'),
                $filter('translate')('vendor.date_formats.month.february'),
                $filter('translate')('vendor.date_formats.month.march'),
                $filter('translate')('vendor.date_formats.month.april'),
                $filter('translate')('vendor.date_formats.month.may'),
                $filter('translate')('vendor.date_formats.month.june'),
                $filter('translate')('vendor.date_formats.month.july'),
                $filter('translate')('vendor.date_formats.month.august'),
                $filter('translate')('vendor.date_formats.month.september'),
                $filter('translate')('vendor.date_formats.month.october'),
                $filter('translate')('vendor.date_formats.month.november'),
                $filter('translate')('vendor.date_formats.month.december'),
            ];
        };
        ctrl.$postLink = function () {
            if (ctrl.model) {
                ctrl.model = new Date(ctrl.model);
            }
            //ctrl.applyLocale();
            //LkEvent.subscribe('lk-change-locale', null, function () {
                //ctrl.applyLocale();
            //});

        };

        ctrl.format = 'MMMM, yyyy';

        ctrl.clear = function () {

        };
        ctrl.onChange = function () {
            if (ctrl.change) {
                ctrl.change();
            }
        };
    }
})();
