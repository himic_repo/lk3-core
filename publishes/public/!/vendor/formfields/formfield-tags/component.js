(function () {
    'use strict';

    angular
        .module('larakit')
        .component('formfieldTags', {
            templateUrl: 'component.html',
            controller: Controller,
            transclude: true,
            bindings: {
                model: '=',
                errors: '=?',
            }
        });

    Controller.$inject = ['$http'];

    function Controller ($http) {
        var $ctrl = this;

        $http.get('/api/tags').then(function (response) {
            $ctrl.tags = response.data;
        });
        $ctrl.tagging = function (newTag) {
            var item = {
                toString: newTag
            };
            return item;
        };
    }
})();
