angular
    .module('larakit')
    .component('formfieldRadioButton', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=',
            isVertical: '=?',
            isFull: '=?',
            desc: '=',
            options: '=',
            label: '=',
            model: '='
        }
    });
