angular
    .module('larakit')
    .component('formfieldTextarea', {
        templateUrl: 'component.html',
        transclude: true,
        bindings: {
            error: '=',
            desc: '=',
            append: '=?',
            disabled: '=?',
            max: '=?',
            isCalc: '=?',
            prepend: '=?',
            placeholder: '=?',
            examples: '=',
            label: '=',
            rows: '=',
            cols: '=',
            isExampleAppend: '=?',
            model: '='
        },
        controller: function () {
            var self = this;
        }
    });
