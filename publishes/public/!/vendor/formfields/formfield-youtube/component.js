(function () {
    'use strict';

    angular
        .module('larakit')
        .component('formfieldYoutube', {
            templateUrl: 'component.html',
            controller: youtubeSearchController,
            transclude: true,
            bindings: {
                model: '=',
                errors: '=?',
            }
        });

    youtubeSearchController.$inject = ['$element', '$http', '$sce'];

    function youtubeSearchController ($element, $http, $sce) {
        var $ctrl = this;
        $ctrl.youtubePaste = function (e, $index) {
            var value = e.originalEvent.clipboardData.getData('text/plain');
            value = value.replace('https://www.youtube.com/watch?v=','');
            $ctrl.model[$index] = value;
            e.preventDefault();
        };
        $ctrl.youtubeAdd = function () {
            if (!$ctrl.model) {
                $ctrl.model = [];
            }
            if (-1 == $ctrl.model.indexOf('')) {
                $ctrl.model.push('');
            }
            //$ctrl.model[(Date.now() % 1000)] = '';
        };
        $ctrl.youtubeDelete = function (k) {
            $ctrl.model.splice(k, 1);
        };
        $ctrl.youtubeUp = function (k) {
            if (k > 0) {
                var val = $ctrl.model[k];
                $ctrl.model[k] = $ctrl.model[k - 1];
                $ctrl.model[k - 1] = val;
            }
        };
        $ctrl.youtubeDown = function (k) {
            if ((k + 1) < $ctrl.model.length) {
                var val = $ctrl.model[k];
                $ctrl.model[k] = $ctrl.model[k + 1];
                $ctrl.model[k + 1] = val;
            }
        };
    }
})();
