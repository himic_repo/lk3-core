(function () {

        angular
            .module('larakit')
            .component('rtdlAvatar', {
                templateUrl: 'component.html',
                transclude: true,
                bindings: {
                    name: '=',
                    type: '=',
                    model: '='
                },
                controller: Controller
            });

        Controller.$inject = [];

        function Controller () {
            var $ctrl = this;
        }
    }
)();
