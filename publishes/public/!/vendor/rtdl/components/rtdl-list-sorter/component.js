(function () {

    angular
        .module('larakit')
        .component('rtdlListSorter', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                params: '=',
                sorters: '=',
                load: '&'
            },
            controller: Controller
        });

    Controller.$inject = ['$location'];

    function Controller ($location) {
        var $ctrl = this;
        $ctrl.orderBy = function (sorter) {
            var order;
            if ($ctrl.params.order_field == sorter.name) {
                order = !$ctrl.params.order_desc;
            } else {
                order = false;
            }
            $location.search('_order_field', sorter.name);
            $location.search('_order_desc', order?'true':'false');
        };
    }

})();
