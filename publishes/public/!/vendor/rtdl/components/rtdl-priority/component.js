(function () {

    angular
        .module('larakit')
        .component('rtdlPriority', {
            templateUrl: 'component.html',
            bindings: {
                model: '='
            }
        });
})();