(function () {

        angular
            .module('larakit')
            .component('rtdlContentWrapper', {
                templateUrl: 'component.html',
                transclude: true,
                bindings: {
                    noBreadcrumb: '=?',
                    noTitle: '=?'
                },
                controller: Controller
            });

        Controller.$inject = ['$route', '$window', 'BreadCrumbs', '$location'];

        function Controller ($route, $window, BreadCrumbs, $location) {

            var $ctrl = this;
            $ctrl.breadcrumbs = BreadCrumbs;
            $ctrl.route = $route.current;
            $ctrl.resize = function () {
                var _sidebarHeight = $('.sidebar-v1').height(),
                    _mainWrapper = $('.main-wrapper').height() + 50,
                    winH = 600,
                    windowWidth = 767;
                if (document.body && document.body.offsetWidth) {
                    windowWidth = document.body.offsetWidth;
                    winH = document.body.offsetHeight;
                }
                if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
                    windowWidth = document.documentElement.offsetWidth;
                    winH = document.documentElement.offsetHeight;
                }
                if (window.innerWidth && window.innerHeight) {
                    windowWidth = window.innerWidth;
                    winH = window.innerHeight;
                }
                winH = winH - 51;
                $('.main-wrapper').css('min-height', winH + "px");
                if (windowWidth <= 767)
                    $('.sidebar-v1').css('height', $('.main-wrapper').height() + 71);

            };
            $ctrl.$postLink = function () {
                angular.element($window).bind('resize', function () {
                    $ctrl.resize();
                });
                $ctrl.resize();
                $ctrl.path = $location.path();
            };
        }
    }
)();
