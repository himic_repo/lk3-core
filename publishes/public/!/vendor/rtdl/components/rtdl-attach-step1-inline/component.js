(function () {

    angular
        .module('larakit')
        .component('rtdlAttachStep1Inline', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                load: '&',
                type: '=?',
                class: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', 'LkHttp', 'LkEvent', '$filter'];

    function Controller ($uibModal, LkHttp, LkEvent, $filter) {
        var $ctrl = this;

        //регистрируем действие ПОСЛЕ успешной загрузки
        $ctrl.onCompleteItem = function (fileItem, response) {
            //console.log(arguments);
            larakit_toastr(response);
            if('success'==response.result){
                $ctrl.model.attach_blocks = response.data;
            }
        };
        $ctrl.actionEdit = function (file) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlAttachStep3',
                resolve: {
                    model: function () {
                        return file;
                    }
                }
            });
            modalInstance.result.then(function (attach_blocks) {
                $ctrl.model.attach_blocks = attach_blocks;
            }, function () {
            });
        };
        $ctrl.onCopy = function () {
            larakit_toastr({
                result: 'success',
                message: $filter('translate')('vendor.rtdl.rtdl-attach-step1-inline.confirm.copy'),
            });
        };

        $ctrl.actionRemove = function (file_id) {
            if (confirm($filter('translate')('vendor.rtdl.rtdl-attach-step1-inline.confirm.delete'))) {
                LkHttp
                    .post('/api/attaches/' + file_id + '/delete ')
                    .then(function (response) {
                        if ('success' == response.result) {
                            $ctrl.model.attach_blocks = response.data;
                        }
                    });
            }
        };
        $ctrl.dndMoved = function (url, index, name) {
            $ctrl.model.attach_blocks[name].items.splice(index, 1);
            var ids = [];
            $ctrl.model.attach_blocks[name].items.forEach(function (item) {
                ids.push(item.id);
            });
            LkHttp.post(url, {ids: ids}).then(function (response) {
                $ctrl.model.attach_blocks = response.data;
            });
        };
    }
})();
