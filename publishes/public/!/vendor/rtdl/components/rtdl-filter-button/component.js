(function () {

    angular
        .module('larakit')
        .component('rtdlFilterButton', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkSidebars'];

    function Controller(LkSidebars) {
        var $ctrl = this;
        $ctrl.toggle = function () {
            LkSidebars.rightToggle();
        }
    }
})();
