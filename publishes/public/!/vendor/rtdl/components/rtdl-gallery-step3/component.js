(function () {

    angular
        .module('larakit')
        .component('rtdlGalleryStep3', {
            templateUrl: 'component.html',
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            controller: Controller
        });

    Controller.$inject = ['LkHttp', 'LkEvent'];

    function Controller (LkHttp, LkEvent) {
        var $ctrl = this;
        $ctrl.model = {};
        $ctrl.$postLink = function () {
            $ctrl.model = _.clone($ctrl.resolve.model);
        };
        $ctrl.save = function () {
            LkHttp
                .post('/api/galleries/' + $ctrl.model.id + '/update', $ctrl.model)
                .then(
                    function (response) {
                        if ('error' == response.result) {
                            $ctrl.errors = response.errors;
                        } else {
                            $ctrl.errors = {};
                            LkEvent.fire('gallery-reload');
                            $ctrl.close({$value: response.data});
                        }
                    }, function () {

                    }
                );
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
})();
