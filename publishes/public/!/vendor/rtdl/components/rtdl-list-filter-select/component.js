(function () {

    angular
        .module('larakit')
        .component('rtdlListFilterSelect', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                filter: '=',
                params: '=',
                load: '='
            },
            controller: Controller
        });

    Controller.$inject = ['LkList', '$location', '$timeout'];

    function Controller (LkList, $location, $timeout) {
        var $ctrl = this;
        $ctrl.isShow = function () {
            if (-1 === ['select2'].indexOf($ctrl.filter.type)) {
                return false;
            }
            if (null != $ctrl.filter.condition) {
                if (!eval($ctrl.filter.condition)) {
                    return false;
                }
            }
            return true;
        };
        $ctrl.onChange = function () {
            if ($ctrl.isShow()) {
                if ($ctrl.filter.is_location) {
                    //console.log('is_location');
                    $timeout(function () {
                        var value_new = [];
                        var value = $ctrl.params.filters[$ctrl.filter.name];
                        if (undefined !== value) {
                            //console.log(value);
                            _.each($ctrl.params.filters[$ctrl.filter.name], function (v) {
                                value_new.push(v.id);
                                //console.log('repeat', v.id);
                            });
                            console.log(value_new);
                            $location.search($ctrl.filter.name, value_new.join(','));
                        }
                    }, 200);
                } else {
                    console.log('! is_location');
                    $ctrl.load()(false, 1);
                }
            }
        }
    }
})();
