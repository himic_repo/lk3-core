(function () {
        angular
            .module('larakit')
            .component('rtdlPage', {
                templateUrl: 'component.html',
                transclude: true,
                controller: Controller
            });
        Controller.$inject = ['$route', '$location', '$window', 'BreadCrumbs', 'LkPage', 'LkSidebars', 'hotkeys', 'LkHttp', 'LkUser', 'LK_APP_NAME', 'LkLocales', 'LkEvent', '$translate'];

        function Controller ($route, $location, $window, BreadCrumbs, LkPage, LkSidebars, hotkeys, LkHttp, LkUser, LK_APP_NAME, LkLocales, LkEvent, $translate) {
            var $ctrl = this;
            $ctrl.breadcrumbs = BreadCrumbs;
            $ctrl.sidebars = LkSidebars;

            hotkeys.add({
                combo: 'ctrl+left',
                description: 'Description goes here',
                callback: function (event, hotkey) {
                    LkSidebars.leftToggle();
                }
            });
            $ctrl.me = {};
            $ctrl.app = LK_APP_NAME;
            $ctrl.menu_items = [];

            $ctrl.leftToggle = function () {
                LkSidebars.leftToggle();
            };
            $ctrl.rightToggle = function () {
                LkSidebars.rightToggle();
                return false;
            };
            $ctrl.isNeedLangs = false;
            $ctrl.load = function (is_clear) {
                LkHttp.get('/api/system/apps')
                    .then(function (response) {
                        $ctrl.apps = response.data;
                    });
                LkHttp.get('/api/system/menus')
                    .then(function (response) {
                        $ctrl.items = response.data;
                    });
                LkLocales.get(is_clear).then(function (response) {
                    $ctrl.langs = response.langs;
                    $ctrl.lang = response.lang;
                    $ctrl.lang_slug = response.lang_slug;
                    $ctrl.isNeedLangs = Object.keys($ctrl.langs).length > 1;
                });
            };
            $ctrl.load();
            LkUser.me().then(function (data) {
                $ctrl.me = data;
            });
            $ctrl.logout = function () {
                LkHttp.post('/logout').then(function (response) {
                    if ('success' == response.result) {
                        larakit_toastr(response);
                        setTimeout(function () {
                            window.location.href = '/';
                        }, 1000);
                    }
                })
            };
            $ctrl.setLocale = function (langSlug, $event) {
                $event.preventDefault();
                LkHttp.post('/api/system/locales/' + langSlug)
                    .then(function (response) {
                        $ctrl.lang_slug = langSlug;
                        LkEvent.fire('lk-change-locale');
                        $translate.use(langSlug);
                        $ctrl.load(true);
                    });
                return false;
            };

            hotkeys.add({
                combo: 'ctrl+left',
                description: 'Description goes here',
                callback: function (event, hotkey) {
                    LkSidebars.leftToggle();
                }
            });
            $ctrl.path = $location.path();
        }
    }
)();
