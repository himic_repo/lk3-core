(function () {

    angular
        .module('larakit')
        .component('rtdlListDetails', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                details: '=',
                fields: '=',
                prefixLang: '=',
                prefixComponent: '='
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller () {
        var $ctrl = this;
        $ctrl.$postLink = function () {
            $ctrl.details = {};
            _.each($ctrl.fields, function (k) {
                $ctrl.details[k] = {};
                $ctrl.details[k].name = $ctrl.prefixLang + '.list.details.' + k;
                $ctrl.details[k].value = 'true' === localStorage.getItem($ctrl.prefixComponent + k);
            });
        };

        $ctrl.detailToggle = function (k) {
            localStorage.setItem($ctrl.prefixComponent + k, $ctrl.details[k].value);
        };
    }
})();
