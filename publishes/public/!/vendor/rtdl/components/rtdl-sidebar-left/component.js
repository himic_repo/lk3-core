(function () {

    angular
        .module('larakit')
        .component('rtdlSidebarLeft', {
            templateUrl: 'component.html',
            transclude: true,
            controller: Controller
        });

    Controller.$inject = ['LkHttp', '$route', 'LkSidebars', 'hotkeys', 'LkEvent', 'LK_APP_NAME'];

    function Controller(LkHttp, $route, LkSidebars, hotkeys, LkEvent, LK_APP_NAME) {
        var $ctrl = this;
        $ctrl.opened = {};
        $ctrl.section = LK_APP_NAME;
        $ctrl.sidebars = LkSidebars;

        $ctrl.load = function () {
            LkHttp
                .get('/api/system/menus ')
                .then(function (response) {
                    $ctrl.menu_items = response.data[$ctrl.section];
                    // console.log(response.data);
                    // console.log($ctrl.section);
                    // console.log($ctrl.menu_items);
                });
        };
        $ctrl.current = $route.current.originalPath;
        $ctrl.load();
        //навешиваем слушателя на событие
        LkEvent.subscribe('sidebar-reload', 'rtdl-sidebar', function () {
            $ctrl.load();
        });

        $ctrl.treeViewMenuStyle = function (item) {
            return {
                display: $ctrl.isOpened(item) ? 'block' : 'none'
            };
        };
        $ctrl.toggle = function (item, $event) {
            if (item._items_.length > 0) {
                $ctrl.opened[item.url] = !$ctrl.opened[item.url];
                $event.preventDefault();
            }
        };

        $ctrl.isOpened = function (item) {
            if (undefined == $ctrl.opened[item.url]) {
                $ctrl.opened[item.url] = $ctrl.isActive(item);
            }
            return $ctrl.opened[item.url];
        };
        $ctrl.isActive = function (item) {
            // console.log($ctrl.opened[item.url], item.access_name, item.childs);
            for (var i = 0; i < item.childs.length; i++) {
                if (item.childs[i] == $route.current.originalPath) {
                    return true;
                }
            }
            return false;
        };

    }

})();
