(function () {

    angular
        .module('larakit')
        .component('rtdlAlerts', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkAlerts'];

    function Controller(LkAlerts) {
        var $ctrl = this;
        //LkAlerts.add({
        //    result: 'danger',
        //    message: 'Error message',
        //});
        //LkAlerts.add({
        //    result: 'warning',
        //    message: 'warning message',
        //});
        //LkAlerts.add({
        //    result: 'info',
        //    message: 'info message',
        //});
        //LkAlerts.add({
        //    result: 'success',
        //    message: 'Success message',
        //});
        $ctrl.alerts = LkAlerts.get();
        $ctrl.close = function (index) {
            LkAlerts.close(index);
        }
    }
})();