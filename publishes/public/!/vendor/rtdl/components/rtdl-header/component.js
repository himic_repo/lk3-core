(function () {

    angular
        .module('larakit')
        .component('rtdlHeader', {
            templateUrl: 'component.html',
            bindings:{
                rightIcon:'=?'
            },
            controller: Controller
        });

    Controller.$inject = ['LkSidebars', 'LkUser', '$location', 'LkHttp', 'hotkeys', 'LK_APP_NAME', '$translate', 'LkEvent', 'LkLocales'];

    function Controller (LkSidebars, LkUser, $location, LkHttp, hotkeys, LK_APP_NAME, $translate, LkEvent, LkLocales) {
        var $ctrl = this;
        $ctrl.me = {};
        $ctrl.app = LK_APP_NAME;
        $ctrl.menu_items = [];

        $ctrl.sidebars =  LkSidebars;

        $ctrl.isNeedLangs = false;
        $ctrl.load = function (is_clear) {
            LkHttp.get('/api/system/apps')
                .then(function (response) {
                    $ctrl.apps = response.data;
                    $ctrl.app_name = '';
                    _.each($ctrl.apps, function (app) {
                        if(app.code==$ctrl.app){
                            $ctrl.app_name = app.html;
                        }
                    })
                });
            LkHttp.get('/api/system/menus')
                .then(function (response) {
                    $ctrl.items = response.data;
                });
            LkLocales.get(is_clear).then(function (response) {
                $ctrl.langs = response.langs;
                $ctrl.lang = response.lang;
                $ctrl.lang_slug = response.lang_slug;
                $ctrl.isNeedLangs = Object.keys($ctrl.langs).length > 1;
            });
        };
        $ctrl.load();
        LkUser.me().then(function (data) {
            $ctrl.me = data;
        });
        $ctrl.logout = function () {
            LkHttp.post('/logout').then(function (response) {
                if ('success' == response.result) {
                    larakit_toastr(response);
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 1000);
                }
            })
        };

        $ctrl.setLocale = function (langSlug, $event) {
            $event.preventDefault();
            LkHttp.post('/api/system/locales/' + langSlug)
                .then(function (response) {
                    $ctrl.lang_slug = langSlug;
                    LkEvent.fire('lk-change-locale');
                    $translate.use(langSlug);
                    $ctrl.load(true);
                });
            return false;
        };


        hotkeys.add({
            combo: 'ctrl+left',
            description: 'Description goes here',
            callback: function (event, hotkey) {
                LkSidebars.leftToggle();
            }
        });
        hotkeys.add({
            combo: 'ctrl+right',
            description: 'Description goes here',
            callback: function (event, hotkey) {
                LkSidebars.rightToggle();
            }
        });
        $ctrl.path = $location.path();
    }
})();
