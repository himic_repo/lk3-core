(function () {
        angular
            .module('larakit')
            .component('rtdlTranslateOriginal', {
                templateUrl: 'component.html',
                controller: Controller
            });
        Controller.$inject = ['LkTranslateOriginal'];
        function Controller (LkTranslateOriginal) {
            var $ctrl = this;
            $ctrl.isTranslateOriginal = LkTranslateOriginal.get;
            $ctrl.detailToggle = function () {
                LkTranslateOriginal.toggle();
            }
        }
    }

)();

