(function () {

    angular
        .module('larakit')
        .component('rtdlListFilterBoolean', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                filter: '=',
                params: '=',
                load: '='
            },
            controller: Controller
        });

    Controller.$inject = ['LkList', '$location', '$timeout'];

    function Controller (LkList, $location, $timeout) {
        var $ctrl = this;
        $ctrl.isShow = function () {
            if (-1 === ['boolean'].indexOf($ctrl.filter.type)) {
                return false;
            }
            if (null != $ctrl.filter.condition) {
                if (!eval($ctrl.filter.condition)) {
                    return false;
                }
            }
            return true;
        };
        $ctrl.onChange = function () {
            if ($ctrl.isShow()) {
                if ($ctrl.filter.is_location) {
                    $location.search($ctrl.filter.name, $ctrl.params.filters[$ctrl.filter.name]);
                } else {
                    $ctrl.load()(false, 1);
                }
            }
        }
    }
})();
