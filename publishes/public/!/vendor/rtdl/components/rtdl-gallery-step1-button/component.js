(function () {

    angular
        .module('larakit')
        .component('rtdlGalleryStep1Button', {
            templateUrl: 'component.html',
            transclude: {
                'content': '?galleryButton'
            },
            bindings: {
                model: '=',
                load: '&',
                text: '=?',
                type: '=?',
                class: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', 'LkEvent', '$transclude'];

    function Controller ($uibModal, LkEvent, $transclude) {
        var $ctrl = this;

        $ctrl.isContent = $transclude.isSlotFilled('content');
        LkEvent.subscribe('gallery-reload', null, function () {
            if ($ctrl.load) {
                $ctrl.load()();
            }
        });
        $ctrl.gotoStep2 = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlGalleryStep2',
                backdrop: 'static',
                size: 'full',
                keyboard: false,
                resolve: {
                    model: function () {
                        return $ctrl.model;
                    },
                    type: function () {
                        return $ctrl.type;
                    }
                }
            });
            modalInstance.result.then(function (o) {
                LkEvent.fire('gallery-reload');
            });

        };
    }
})();
