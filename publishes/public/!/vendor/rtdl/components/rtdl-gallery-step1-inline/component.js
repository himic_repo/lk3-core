(function () {

    angular
        .module('larakit')
        .component('rtdlGalleryStep1Inline', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                load: '&',
                type: '=?',
                class: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', 'LkHttp', 'LkEvent', '$filter'];

    function Controller ($uibModal, LkHttp, LkEvent, $filter) {
        var $ctrl = this;
        LkEvent.subscribe('gallery-reload', null, function () {
            $ctrl.load()();
        });

        //регистрируем действие ПОСЛЕ успешной загрузки
        $ctrl.onCompleteItem = function (fileItem, response) {
            //console.log(arguments);
            larakit_toastr(response);
            if('success'==response.result){
                $ctrl.model.gallery_blocks = response.data;
            }
            LkEvent.fire('gallery-reload');
        };
        $ctrl.actionEdit = function (file) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlGalleryStep3',
                resolve: {
                    model: function () {
                        return file;
                    }
                }
            });
            modalInstance.result.then(function (gallery_blocks) {
                $ctrl.model.gallery_blocks = gallery_blocks;
            }, function () {
                LkEvent.fire('gallery-reload');
            });
        };

        $ctrl.actionRemove = function (file) {
            if (confirm($filter('translate')('vendor.rtdl.rtdl-gallery-step1-inline.confirm.delete'))) {
                LkHttp
                    .post('/api/galleries/' + file.id + '/delete ')
                    .then(function (response) {
                        if ('success' == response.result) {
                            $ctrl.model.gallery_blocks = response.data;
                            LkEvent.fire('gallery-reload');
                        }
                    });
            }
        };
        $ctrl.actionRemoveAll = function (block) {
            if (confirm($filter('translate')('vendor.rtdl.rtdl-gallery-step1-inline.confirm.delete_all'))) {
                LkHttp
                    .post($ctrl.model.gallery_blocks[block].url_clear)
                    .then(function (response) {
                        if ('success' == response.result) {
                            $ctrl.model.gallery_blocks = response.data;
                            LkEvent.fire('gallery-reload');
                        }
                    });
            }
        };

        $ctrl.dndMoved = function (url, index, name) {
            $ctrl.model.gallery_blocks[name].items.splice(index, 1);
            var ids = [];
            $ctrl.model.gallery_blocks[name].items.forEach(function (item) {
                ids.push(item.id);
            });
            LkHttp.post(url, {ids: ids}).then(function (response) {
                $ctrl.model.gallery_blocks = response.data;
            });
        };
    }
})();
