(function () {

    angular
        .module('larakit')
        .component('rtdlGalleryStep2', {
            templateUrl: 'component.html',
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            controller: Controller
        });

    Controller.$inject = ['$http', '$uibModal'];

    function Controller ($http, $uibModal) {
        var $ctrl = this;

        $ctrl.onPaste = function (base64, url) {
            $http
                .post(url, {
                    base64: base64
                })
                .then(function (response) {
                    $ctrl.model = response.data.model;
                });
        };
        $ctrl.$onInit = function () {
            $ctrl.model = $ctrl.resolve.model;
            $ctrl.type = $ctrl.resolve.type;
        };
        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };

    }
})();