(function () {

    angular
        .module('larakit')
        .component('rtdlLoader', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkEvent'];

    function Controller (LkEvent) {
        var $ctrl = this;
        LkEvent.subscribe('http-request', 'rtdlLoader', function (cnt) {
            $ctrl.cnt = cnt;
        });
    }
})();
