(function () {

    angular
        .module('larakit')
        .component('rtdlModal', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                model: '=',
                onSave: '&',
                onCancel: '&'
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller () {
        var $ctrl = this;
        $ctrl.save = function () {
            $ctrl.onSave()();
        };
        $ctrl.cancel = function () {
            $ctrl.onCancel()();
        };
    }

})();