(function () {

    angular
        .module('larakit')
        .component('rtdlListFilterText', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                filter: '=',
                params: '=',
                load: '='
            },
            controller: Controller
        });

    Controller.$inject = ['LkList', '$location'];

    function Controller (LkList, $location) {
        var $ctrl = this;
        $ctrl.isShow = function () {
            if (-1 === ['like', 'equal'].indexOf($ctrl.filter.type)) {
                return false;
            }
            if (null != $ctrl.filter.condition) {
                if (!eval($ctrl.filter.condition)) {
                    return false;
                }
            }
            return true;
        };
        $ctrl.onChange = function () {
            if ($ctrl.isShow()) {
                if ($ctrl.filter.is_location) {
                    $location.search($ctrl.filter.name, $ctrl.params.filters[$ctrl.filter.name]);
                } else {
                    $ctrl.load()(false, 1);
                }
            }
        }
    }
})();
