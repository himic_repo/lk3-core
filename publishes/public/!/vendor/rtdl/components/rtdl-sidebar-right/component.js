(function () {

    angular
        .module('larakit')
        .component('rtdlSidebarRight', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                isShow: '=',
                noPadd: '=?',
                width: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['hotkeys', 'LkSidebars'];

    function Controller (hotkeys, LkSidebars) {
        var $ctrl = this;
        $ctrl.sidebars = LkSidebars;
        $ctrl.$postLink = function () {
            if (!$ctrl.width) {
                $ctrl.width = 350;
            }
        };
        hotkeys.add({
            combo: 'ctrl+right',
            description: 'Description goes here',
            callback: function (event, hotkey) {
                $ctrl.sidebars.rightToggle();
            }
        });
    }
})();
