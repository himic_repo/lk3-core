(function () {

    angular
        .module('lkng-thumb')
        .component('rtdlThumbStep1', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                type: '=?',
                isRound: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', '$http', 'LkThumb'];
    function Controller ($uibModal, $http, LkThumb) {
        var $ctrl = this;

        $ctrl.load = function (type) {
            $http
                .get($ctrl.model.thumbs[type].url_thumb)
                .then(function (response) {
                    $ctrl.model = LkThumb.refreshHashModel(response.data.model);
                });
        };

        $ctrl.gotoStep2 = function (type, thumber) {
            if (!thumber.url_upload) {
                return false;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlThumbStep2',
                size: 'lg',
                resolve: {
                    model: function () {
                        return $ctrl.model;
                    },
                    type: function () {
                        return type;
                    }
                }
            });
            modalInstance.result.then(function (o) {
                $ctrl.load(type);
            }, function () {
            });
        };

    }
})();
