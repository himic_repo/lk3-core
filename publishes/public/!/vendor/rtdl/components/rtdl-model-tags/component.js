(function() {
    angular
        .module('larakit')
        .component('rtdlModelTags', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                type: '=?'
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller() {
        var $ctrl = this;
        $ctrl.$postLink = function() {
            if (!$ctrl.type) {
                $ctrl.type = 'tag';
            }
        };
    }
})();
