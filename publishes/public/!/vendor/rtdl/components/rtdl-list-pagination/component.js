(function () {

    angular
        .module('larakit')
        .component('rtdlListPagination', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                data: '=',
                params: '=',
                load: '&'
            },
            controller: Controller
        });

    Controller.$inject = [];

    function Controller() {
        var $ctrl = this;
    }
})();

