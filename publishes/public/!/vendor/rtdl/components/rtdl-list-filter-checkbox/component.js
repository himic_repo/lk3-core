(function () {

    angular
        .module('larakit')
        .component('rtdlListFilterCheckbox', {
            templateUrl: 'component.html',
            transclude: true,
            bindings: {
                filter: '=',
                params: '=',
                load: '='
            },
            controller: Controller
        });

    Controller.$inject = ['LkList', '$location', '$timeout'];

    function Controller (LkList, $location, $timeout) {
        var $ctrl = this;
        $ctrl.isShow = function () {
            if (-1 === ['checkbox'].indexOf($ctrl.filter.type)) {
                return false;
            }
            if (null != $ctrl.filter.condition) {
                if (!eval($ctrl.filter.condition)) {
                    return false;
                }
            }
            return true;
        };
        $ctrl.onChange = function () {
            if ($ctrl.isShow()) {
                if ($ctrl.filter.is_location) {
                    console.log($ctrl.filter.name);
                    $timeout(function () {
                        var value_new = [];
                        var value = $ctrl.params.filters[$ctrl.filter.name];
                        if (undefined !== value) {
                            _.each($ctrl.params.filters[$ctrl.filter.name], function (v, k) {
                                if (v && k.length > 0) {
                                    value_new.push(k);
                                }
                            });
                            if (value_new.length) {
                                $location.search($ctrl.filter.name, value_new.join(','));
                            } else {
                                $location.search($ctrl.filter.name, null);
                            }

                        }
                    }, 200);
                } else {
                    $ctrl.load()(false, 1);
                }
            }
        }
    }
})();
