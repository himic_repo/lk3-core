(function () {

    angular
        .module('lkng-thumb')
        .component('rtdlThumbStep2', {
            templateUrl: 'component.html',
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', 'LkHttp', 'LkThumb'];

    function Controller($uibModal, LkHttp, LkThumb) {
        var $ctrl = this;
        $ctrl.model = {};
        $ctrl.$onInit = function () {
            $ctrl.model = $ctrl.resolve.model;
            $ctrl.type = $ctrl.resolve.type;
            $ctrl.load();
        };

        $ctrl.clear = function () {
            LkHttp
                .post($ctrl.model.thumbs[$ctrl.type].url_clear)
                .then(function (response) {
                    $ctrl.load();
                });
        };

        $ctrl.load = function () {
            LkHttp
                .get($ctrl.model.thumbs[$ctrl.type].url_thumb)
                .then(function (response) {
                    $ctrl.model.thumbs = LkThumb.refreshHashModel(response.data);
                });
        };

        $ctrl.onCompleteItem = function(fileItem, response, status, headers){
            larakit_toastr(response);
            if('success'==response.result){
                $ctrl.model.thumbs = response.data;
            }
        };


        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: $ctrl.model});
        };

        $ctrl.gotoStep3 = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlThumbStep3',
                size: 'full',
                resolve: {
                    thumber: function () {
                        return $ctrl.model.thumbs[$ctrl.type];
                    },
                    size: function () {
                        return size;
                    }
                }
            });
            modalInstance.result.then(function (o) {
                $ctrl.load();
            }, function () {
                $ctrl.load();
            });
        };

    }
})();