(function () {

    angular
        .module('larakit')
        .component('rtdlAttachStep1Button', {
            templateUrl: 'component.html',
            bindings: {
                model: '=',
                load: '&',
                text: '=?',
                type: '=?',
                class: '=?'
            },
            controller: Controller
        });

    Controller.$inject = ['$uibModal', 'LkEvent'];

    function Controller ($uibModal, LkEvent) {
        var $ctrl = this;
        LkEvent.subscribe('gallery-reload', null, function(){
            if ($ctrl.load) {
                $ctrl.load()();
            }
        });
        $ctrl.gotoStep2 = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-bottom',
                ariaDescribedBy: 'modal-body-bottom',
                component: 'rtdlAttachStep2',
                backdrop: 'static',
                size: 'full',
                keyboard: false,
                resolve: {
                    model: function () {
                        return $ctrl.model;
                    },
                    type: function () {
                        return $ctrl.type;
                    }
                }
            });
            modalInstance.result.then(function (o) {
                LkEvent.fire('gallery-reload');
            });

        };
    }
})();
