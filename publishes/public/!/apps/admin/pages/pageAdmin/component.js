(function () {

    angular
        .module('larakit')
        .component('pageAdmin', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['BreadCrumbs'];

    function Controller (BreadCrumbs) {
        var $ctrl = this;
        // Хлебные крошки
        BreadCrumbs.clear();
        BreadCrumbs.add('admin.user');
    }
})();
