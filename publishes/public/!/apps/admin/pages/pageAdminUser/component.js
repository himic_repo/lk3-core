(function () {

    angular
        .module('larakit')
        .component('pageAdminUser', {
            templateUrl: 'component.html',
            controller: Controller
        });

    Controller.$inject = ['LkCrud'];

    function Controller (LkCrud) {
        var $ctrl = this;
        //префикс переводов
        $ctrl.prefix_lang = 'admin.pageAdminUser';
        $ctrl.modal_name = 'modalUser';
        $ctrl.modal_size = 'lg';
        $ctrl.modal_is_modal = true;
        //получаем настройки списка
        LkCrud.init($ctrl, '/api/users');
        // Хлебные крошки
        $ctrl.breadcrumbs.add('admin.user');

    }
})();
