(function() {
    angular
        .module('larakit', [
            'yaMap',
            'ngNamedRoute',
            'ngSanitize',
            'ngResource',
            'ngRoute',
            'ngCookies',
            'cfp.hotkeys',
            'lk-angular',
            'ngQuill',
            'ngTouch',
            'ui.select',
            'ui.tree',
            'ui.bootstrap',
            'uiSwitch',
            'dndLists',
            'angularFileUpload',
            'lkng-thumb',
            'angular-clipboard',
            'ui.mask',
            'fancyboxplus',
            'pascalprecht.translate'
        ]);

    var larakit = angular.module('larakit');
    larakit.constant('LK_APP_NAME', 'admin');
    larakit
        .config(['$locationProvider', '$routeProvider', '$translateProvider',
            function config($locationProvider,
                            $routeProvider,
                            $translateProvider) {
                $translateProvider.useStaticFilesLoader({
                    prefix: '/!/locales/',
                    suffix: '.json'
                });

                var lang = $('html').attr('lang');
                $translateProvider
                    .preferredLanguage(lang)
                    .fallbackLanguage('ru');
                // $translateProvider.useCookieStorage();
                $translateProvider.useSanitizeValueStrategy(null);

                $locationProvider.html5Mode(true);
                /* ################################################## */
                /*                      ROUTES START                  */
                /* ################################################## */
                $routeProvider
                    .otherwise('/admin/users')
                    .when('/admin/users',
                        {
                            'name': 'admin.user',
                            'template': '<page-admin-user><\/page-admin-user>',
                            'title': 'admin.pageAdminUser.title',
                            'subtitle': 'vendor.crud.manage_list',
                            'icon': 'fa fa-list'
                        }
                    )
                    .when('/admin/snippets',
                        {
                            'name': 'admin.snippet',
                            'template': '<page-admin-snippet>' +
                            '<\/page-admin-snippet>',
                            'title': 'vendor.admin.pageAdminSnippet.title',
                            'subtitle': 'vendor.crud.manage_list',
                            'icon': 'fa fa-list'
                        }
                    )
;
            }
        ]);

    larakit.run(
        ['$http', '$rootScope', 'LkSidebars',
            function($http, $rootScope, LkSidebars) {
                $http.defaults.headers.common['X-CSRF-TOKEN'] =
                    $('html').attr('_token');
                $rootScope.sidebars = LkSidebars;
            }]);
})();
