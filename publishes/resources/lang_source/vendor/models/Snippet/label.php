<?php
return [
    'id'    => '#',
    'label' => 'Наименование',
    'group' => 'Группа сниппетов',
    'html'  => 'Значение сниппета',
    'type'  => 'Тип сниппета',
    'thumb' => 'Скриншот блока',
];
