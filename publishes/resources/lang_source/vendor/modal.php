<?php
return [
    'btn'   => [
        'create' => 'Создать',
        'update' => 'Сохранить',
        'delete' => 'Удалить',
        'cancel' => 'Отмена',
    ],
    'title' => [
        'create' => 'Создание',
        'update' => 'Сохранение',
        'delete' => 'Удаление',
        'cancel' => 'Отмена',
    ],
];
