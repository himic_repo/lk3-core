<?php
return [
    'filter' => 'Фильтр',
    'show' => 'Показано с {{from}} по {{to}} из {{total}}',
];
