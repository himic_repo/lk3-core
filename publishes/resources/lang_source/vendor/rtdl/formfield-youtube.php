<?php
return [
    'btn_add' => 'Добавить ролик',
    'footer'  => 'То, что идет после "https://www.youtube.com/watch?v=". <br>Например: "W3AHCqstyKs".<br>Убедитесь, что после вставки кода появилась корректная картинка ролика',
    'movies'  => [
        'main'    => 'Основной ролик',
        'variant' => 'Вариант №{{index}}',
    ],
];
