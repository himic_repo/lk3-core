<?php
return [
    'btn_add' => 'Добавить ролик',
    'footer'  => 'То, что идет после "https://www.aparat.com/v/". <br>Например: "1Qu7Y".<br>Убедитесь, что после вставки кода появилась корректная картинка ролика',
    'movies'  => [
        'main'    => 'Основной ролик',
        'variant' => 'Вариант №{{index}}',
    ],
];
