<?php
return [
    'control'     => 'Управление слайдом',
    'name'        => 'Название слайда',
    'description' => 'Описание слайда',
    'priority'    => 'Приоритет вывода',
    'top_list'    => 'Чем выше, тем ближе к началу списка',
];
