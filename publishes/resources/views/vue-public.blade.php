<!DOCTYPE html>
<html>

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>public</title>
    <link rel="stylesheet" href="{{ mix('/!/vue/public.css') }}">
    <link rel="stylesheet" href="{{ mix('/!/vue/public-vue.css') }}">
</head>
<body>
<div id="app">
    <a href="/dashboard">ADMIN</a>
    <example-component></example-component>
</div>
<script src="{{ mix('/!/vue/public.js') }}"></script>
</body>
</html>
