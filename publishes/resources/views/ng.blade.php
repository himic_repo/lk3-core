<!DOCTYPE html>
<html
    dir="ltr" lang="{{ \Larakit\LangManager::getCurrentSlug()  }}"
    _token="{{ csrf_token() }}"
    ng-app="larakit">
<head>
    <!-- title -->
    <title>Larakit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="generator" content="Larakit (https://github.com/larakit)"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <base href="/"/>
    <link rel="icon" href="/favicon.ico">
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="/!/dist/apps/{{ $app }}/dev.css?{{ microtime(true) }}"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
</head>

<body class="pace-done fresh-apple-light-theme">
<rtdl-loader></rtdl-loader>
@verbatim
    <div class="wrapper" style="opacity: 1" ng-class="{'toggle-sidebar':sidebars.leftValue()}">
        <ng-view></ng-view>
    </div>
@endverbatim
<script type="text/javascript" src="/!/dist/apps/{{ $app }}/dev.js?{{ microtime(true) }}"></script>
</body>
</html>
