<?php
foreach (rglob('*.php', 0, __DIR__ . '/web/') as $file) {
    include_once $file;
};

Route::get('/', function () {
    return view('vue-public');
});
Route::get('/dashboard', function () {
    return view('vue-admin');
});

