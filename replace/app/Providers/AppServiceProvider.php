<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        foreach (rglob('*.php', 0, base_path('bootstrap/app_boot')) as $file) {
            include_once $file;
        }
        foreach (rglob('*.php', 0, base_path('bootstrap/larakit')) as $file) {
            include_once $file;
        }
        \Larakit\Event\Event::notify('larakit:init');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        foreach (rglob('*.php', 0, base_path('bootstrap/app_register')) as $file) {
            include_once $file;
        }
    }
}
