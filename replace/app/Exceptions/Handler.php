<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Str;
use Larakit\LkValidateException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [//
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     *
     * @return void
     */
    public function report(Exception $exception) {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) {
        if ($this->isJsonError($exception) || $request->expectsJson()) {
            $response = [
                'result'  => 'error',
                'message' => $this->JsonErrorMessage($exception),
            ];
            if (is_a($exception, AuthenticationException::class)) {
                $response['auth'] = true;
            }
            if (is_a($exception, TokenMismatchException::class)) {
                $response['refresh'] = true;
            }
            if (is_a($exception, LkValidateException::class)) {
                $response['errors'] = $exception->getErrors();
            }
            if (config('app.debug')) {
                $response['file']      = $exception->getFile() . ':' . $exception->getLine();
                $response['exception'] = get_class($exception);
            }
            if ($request->expectsJson() || $request->hasFile('file')) {
                return response()->json($response, 200);
            }
            dd($response);
        }

        return parent::render($request, $exception);
    }

    function JsonErrorMessage($exception) {
        if (is_a($exception, ModelNotFoundException::class)) {
            return __('vendor.exception.model_not_found');
        }
        if (is_a($exception, MethodNotAllowedHttpException::class)) {
            return __('vendor.exception.method_not_allowed');
        }
        if (is_a($exception, NotFoundHttpException::class)) {
            return __('vendor.exception.not_found');
        }
        if (is_a($exception, AuthenticationException::class)) {
            return __('vendor.exception.auth');
        }
        if (is_a($exception, AuthorizationException::class)) {
            return __('vendor.exception.deny');
        }

        return $exception->getMessage();

    }

    function isJsonError() {
        $uri = \Request::url();
        $uri = parse_url($uri, PHP_URL_PATH);
        if ('/api/' == Str::substr($uri, 0, 5)) {
            return true;
        }

        return false;
    }
}
