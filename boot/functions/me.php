<?php
if(!function_exists('me')) {
    function me($prop = null) {
        return \Larakit\Me::_($prop);
    }
}

if(class_exists('\Larakit\Twig')) {
    \Larakit\Twig::register_function('me', function ($prop = null) {
        return me($prop);
    });
}