<?php
if (!function_exists('larasafepath')) {
    function larasafepath($path) {
        $path      = str_replace('\\', '/', $path);
        $base_path = base_path('');
        $base_path = str_replace('\\', '/', $base_path);
        return str_replace($base_path, '', $path);
    }
}
