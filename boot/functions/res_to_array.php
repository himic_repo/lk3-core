<?php
if (!function_exists('resource_to_array')) {
    function resource_to_array($resource) {
        $data = $resource->toArray(\Request::instance());
        foreach ($data as $k => $v) {
            if (is_a($v, \Larakit\Resource\JsonResourceCollection::class)) {
                $data[$k] = resources_to_array($v);
            }
            if (is_a($v, \Larakit\Resource\JsonResource::class)) {
                $data[$k] = resource_to_array($v);
            }
        }

        return $data;
    }
}
if (!function_exists('resources_to_array')) {
    function resources_to_array($resources) {
        $ret = [];
        foreach ($resources as $k => $v) {
            $ret[$k] = resource_to_array($v);
        }

        return $ret;
    }
}
