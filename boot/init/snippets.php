<?php
return;
\Larakit\Snippet\SnippetConstructor::instance()
                                   ->registerGroup('vendor-auth', ['vendor', 'auth'])
                                   ->registerSnippetText('login', 'Кнопка войти', 'Войти')
                                   ->registerSnippetText('logout', 'Кнопка выйти', 'Выйти')
                                   ->registerSnippetText('logout-success', 'Сообщение о выходе', 'Вы успешно вышли с проекта');
\Larakit\Snippet\SnippetConstructor::instance()
                                   ->registerGroup('vendor-exception', ['vendor', 'exception'])
                                   ->registerSnippetText('deny', 'Сообщение об отсутствии прав', 'Отсутствую права на это действие')
                                   ->registerSnippetText('auth', 'Сообщение об отсутствии авторизации', 'Вы не авторизованы')
                                   ->registerSnippetText('not_found', 'Сообщение об отсутствии страницы', 'Страница не найдена')
                                   ->registerSnippetText('method_not_allowed', 'Сообщение об отсутствии метода запроса', 'Недопустимый метод запроса')
                                   ->registerSnippetText('model_not_found', 'Сообщение об отсутствии записи в БД', 'Запись не найдена');
\Larakit\Snippet\SnippetConstructor::instance()
                                   ->registerGroup('vendor-formExamples', ['formfield'])
                                   ->registerSnippetText('text', 'Например:', 'Например:');
\Larakit\Snippet\SnippetConstructor::instance()
                                   ->registerGroup('vendor-formfieldSwitch', ['formfield'])
                                   ->registerSnippetText('on', 'Включено', 'Да')
                                   ->registerSnippetText('off', 'Выключено', 'Нет')
;
