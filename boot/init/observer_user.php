<?php
if (class_exists('\App\Observers\UserObserver')) {
    \App\User::observe(\App\Observers\UserObserver::class);
}
