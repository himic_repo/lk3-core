module.exports = {
    apps: {},
    init: function () {
        var self = this;
        self.debug = false;
        self.gulp = require('gulp');
        self.concat = require('gulp-concat');
        self.sass = require('gulp-sass');
        self.filter = require('gulp-custom-filter');
        self.gulpCopy = require('gulp-copy');
        self.autoprefixer = require('gulp-autoprefixer');
        self.cleanCss = require('gulp-clean-css');
        self.clean = require('gulp-clean');
        self.uglify = require('gulp-uglify');
        self.watch = require('gulp-watch');
        self.angularEmbedTemplates = require('gulp-angular-embed-templates');
        self.replace = require('gulp-replace');
        self.plugins = require('gulp-load-plugins')();
        self.svgmin = require('gulp-svgmin');
        self.cheerio = require('gulp-cheerio');
        self.replace = require('gulp-replace');
        self.svgSprite = require('gulp-svg-sprite');
        self.registerApp('codegen', require('./codegen.json'));
    },
    /**
     * Регистрация приложения
     * @param name
     * @param config
     */
    registerApp: function (name, config) {
        var self = this;
        self.apps[name] = config;
        self.apps[name] = {
            vendor_css: [],
            vendor_js: [],
            vendor_html: [],
            app_sass: [],
            app_css: [],
            app_html: [],
            app_js: []
        };
        if (undefined != config.vendor) {
            config.vendor.forEach(function (item) {
                if (item.endsWith('.css')) {
                    self.apps[name].vendor_css.push(item);
                }
                if (item.endsWith('.html')) {
                    self.apps[name].vendor_html.push(item);
                }
                if (item.endsWith('.js')) {
                    self.apps[name].vendor_js.push(item);
                }
            });
        }
        if (undefined != config.app) {
            config.app.forEach(function (item) {
                if (item.endsWith('.scss')) {
                    self.apps[name].app_sass.push(item);
                }
                if (item.endsWith('.css')) {
                    self.apps[name].app_css.push(item);
                }
                if (item.endsWith('.js')) {
                    self.apps[name].app_js.push(item);
                }
                if (item.endsWith('.html')) {
                    self.apps[name].app_html.push(item);
                }
            });
        }
        //главный таск приложения
        self.gulp.task(name, [
            name + '-sass',
            name + '-js',
            name + '-css',
        ]);
        //таск по сборке JS в двух версиях: разработчика и конечной
        self.gulp.task(name + '-js', [
            name + '-js-dev',
            name + '-js-prod',
        ]);
        //таск по сборке CSS в двух версиях: разработчика и конечной
        self.gulp.task(name + '-sass', function () {
            return self.gulp.src(self.apps[name].app_sass, { base: "." })
                .pipe(self.sass.sync())
                .pipe(self.gulp.dest(function (file) {
                    return file.base;
                }));
        });
        //таск по сборке CSS в двух версиях: разработчика и конечной
        self.gulp.task(name + '-css', [
            name + '-css-dev',
            name + '-css-prod',
        ]);
        //сборка версии конечной (просто склейка готовых вендора и приложения)
        self.gulp.task(name + '-js-prod-app-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-js-prod-app.js', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-js-dev-app-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-js-dev-app.js', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-css-prod-app-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-css-prod-app.css', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-css-dev-app-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-css-dev-app.css', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-css-vendor-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-css-vendor.css', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-js-vendor-clean', function () {
            return self.gulp.src('storage/gulp/' + name + '-css-vendor.js', {read: false})
                .pipe(self.clean());
        });
        self.gulp.task(name + '-js-prod',
            [
                name + '-js-vendor',
                name + '-js-prod-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-js-vendor.js',
                        'storage/gulp/' + name + '-js-prod-app.js'
                    ])
                    .pipe(self.concat('prod.js'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));
            });
        //сборка конечной версии приложения во временную папку в один файл
        self.gulp.task(name + '-js-prod-app', [
                name + '-js-prod-app-clean'
            ],
            function () {
                return self.gulp.src(self.apps[name].app_js)
                    .pipe(self.angularEmbedTemplates())
                    .pipe(self.concat(name + '-js-prod-app.js'))
                    //.pipe(self.uglify())
                    .pipe(self.gulp.dest('storage/gulp'));
            });

        self.gulp.task(name + '-js-dev',
            [
                name + '-js-vendor',
                name + '-js-dev-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-js-vendor.js',
                        'storage/gulp/' + name + '-js-dev-app.js'
                    ])
                    .pipe(self.concat('dev.js'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));

            });
        self.gulp.task(name + '-css-dev',
            [
                name + '-css-vendor',
                name + '-css-dev-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-css-vendor.css',
                        'storage/gulp/' + name + '-css-dev-app.css'
                    ])
                    .pipe(self.concat('dev.css'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));
            });
        self.gulp.task(name + '-css-prod',
            [
                name + '-css-vendor',
                name + '-css-prod-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-css-vendor.css',
                        'storage/gulp/' + name + '-css-prod-app.css'
                    ])
                    .pipe(self.concat('prod.css'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));
            });
        self.gulp.task(name + '-css-prod-app', [
                name + '-css-prod-app-clean'
            ],
            function () {
                return self
                    .gulp.src(self.apps[name].app_css)
                    .pipe(self.concat(name + '-css-prod-app.css'))
                    .pipe(self.autoprefixer())
                    .pipe(self.cleanCss())
                    .pipe(self.gulp.dest('storage/gulp'));
            });
        //предварительная сборка всех вендоров в папку storage
        self.gulp.task(name + '-css-vendor', [
                name + '-css-vendor-clean'
            ],
            function () {
                return self
                    .gulp.src(self.apps[name].vendor_css)
                    .pipe(self.concat(name + '-css-vendor.css'))
                    .pipe(self.autoprefixer())
                    .pipe(self.cleanCss())
                    .pipe(self.gulp.dest('storage/gulp'));
            });

        self.gulp.task(name + '-css-dev-app', [
                name + '-css-dev-app-clean'
            ],
            function () {
                return self
                    .gulp.src(self.apps[name].app_css)
                    .pipe(self.concat(name + '-css-dev-app.css'))
                    .pipe(self.gulp.dest('storage/gulp'));
            });
        //сборка версии разработчика (просто склейка готовых вендора и приложения)
        //без сборки вендоров, для этого предварительно надо один раз выполнить дефолтный таск
        self.gulp.task(name + '-js-watch-app',
            [
                name + '-js-dev-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-js-vendor.js',
                        'storage/gulp/' + name + '-js-dev-app.js'
                    ])
                    .pipe(self.concat('dev.js'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));

            });
        self.gulp.task(name + '-css-watch-app',
            [
                name + '-css-dev-app',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-css-vendor.css',
                        'storage/gulp/' + name + '-css-dev-app.css'
                    ])
                    .pipe(self.concat('dev.css'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));

            });
        //сборка версии разработчика (просто склейка готовых вендора и приложения)
        //без сборки приложения, для этого предварительно надо один раз выполнить дефолтный таск
        self.gulp.task(name + '-js-watch-vendor',
            [
                name + '-js-vendor',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-js-vendor.js',
                        'storage/gulp/' + name + '-js-dev-app.js'
                    ])
                    .pipe(self.concat('dev.js'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));

            });
        self.gulp.task(name + '-css-watch-vendor',
            [
                name + '-css-vendor',
            ],
            function () {
                return self.gulp.src([
                        'storage/gulp/' + name + '-css-vendor.css',
                        'storage/gulp/' + name + '-css-dev-app.css'
                    ])
                    .pipe(self.concat('dev.css'))
                    .pipe(self.gulp.dest('public/!/dist/apps/' + name));

            });
        //предварительная сборка всех вендоров в папку storage
        self.gulp.task(name + '-js-vendor',
            function () {
                return self
                    .gulp.src(self.apps[name].vendor_js)
                    .pipe(self.concat(name + '-js-vendor.js'))
                    //.pipe(self.uglify())
                    .pipe(self.gulp.dest('storage/gulp'));
            });
        //сборка приложения версии разработчика во временную папку в один файл
        self.gulp.task(name + '-js-dev-app', [
                name + '-js-dev-app-clean'
            ],
            function () {
                return self
                    .gulp.src(self.apps[name].app_js)
                    .pipe(self.angularEmbedTemplates())
                    .pipe(self.concat(name + '-js-dev-app.js'))
                    .pipe(self.gulp.dest('storage/gulp'));

            });
    },
    /**
     * запуск приложения
     */
    run: function () {
        var app_names = Object.keys(this.apps), self = this;
        self.gulp.task('webfont',
            function () {
                var Fontmin = require('fontmin');
                var fontmin = new Fontmin().src('public/!/source/fonts/**/*.ttf')
                    .use(Fontmin.css())
                    .use(Fontmin.ttf2eot())
                    .use(Fontmin.ttf2woff({
                        deflate: true
                    }))
                    .use(Fontmin.ttf2svg()).dest('public/!/dist/fonts/webfont');
                fontmin.run();
                return self
                    .gulp
                    .src('public/!/dist/fonts/**/*.css')
                    .pipe(self.concat('webfont.css'))
                    .pipe(self.replace('url("', 'url("/!/dist/fonts/webfont/'))
                    .pipe(self.gulp.dest('storage/gulp'));
                // path.src.app.push('storage/gulp/app/fonts.css');
            });
        self.gulp.task('sprites',
            function () {
                var spritesmith = require('gulp.spritesmith');
                var spriteData = self.gulp.src('public/!/source/sprites/png/**/*.png').pipe(spritesmith({
                    imgName: 'sprite.png',
                    cssName: 'sprite.css'
                }));

                spriteData.img
                // DEV: We must buffer our stream into a Buffer for `imagemin`
                //     .pipe(buffer())
                //     .pipe(plugins.imagemin())
                //.pipe(self.gulp.dest('public/!/dist/img/'))
                ;
                spriteData.css
                    // .pipe(csso())
                    .pipe(self.replace('url(', 'url(/!/dist/sprites/')).pipe(self.gulp.dest('storage/gulp'));
                return spriteData.pipe(self.gulp.dest('public/!/dist/sprites/'));
                // var imgStream = spriteData.img.pipe('public/!/sprites');
                // var cssStream = spriteData.css.pipe('public/!/sprites');
                // lkGulp.path.src.app.push('storage/gulp/app/sprite-icons.css');
            });
        self.gulp.task('sprites_svg', function () {
            return self.gulp.src('public/!/source/sprites/svg/*.svg')
                .pipe(self.svgmin({
                    js2svg: {
                        pretty: true
                    }
                }))
                .pipe(self.cheerio({
                    run: function ($) {
                        $('[fill]').removeAttr('fill');
                        $('[stroke]').removeAttr('stroke');
                        $('[style]').removeAttr('style');
                    },
                    parserOptions: {xmlMode: true}
                }))
                .pipe(self.replace('&gt;', '>'))
                .pipe(self.svgSprite({
                    mode: {
                        symbol: {
                            dest: "",
                            sprite: "sprite.svg"
                        }
                    }
                }))
                .pipe(self.gulp.dest('public/!/dist/sprites'));
        });

        self.gulp.task('default', ['webfont', 'sprites', 'sprites_svg'].concat(app_names));
        self.gulp.task('watch',
            function () {
                app_names.forEach(function (app_name) {
                    self.watch(self.apps[app_name].app_sass,
                        function () {
                            self.gulp.start(app_name + '-sass');
                            self.gulp.start(app_name + '-css-watch-app');
                        });
                    self.watch(self.apps[app_name].vendor_js,
                        function () {
                            self.gulp.start(app_name + '-js-watch-vendor');
                        });
                    self.watch(self.apps[app_name].vendor_html,
                        function () {
                            self.gulp.start(app_name + '-js-watch-vendor');
                        });
                    self.watch(self.apps[app_name].vendor_css,
                        function () {
                            self.gulp.start(app_name + '-css-watch-vendor');
                        });
                    self.watch(self.apps[app_name].app_css,
                        function () {
                            self.gulp.start(app_name + '-css-watch-app');
                        });
                    self.watch(self.apps[app_name].app_js,
                        function () {
                            self.gulp.start(app_name + '-js-watch-app');
                        });
                    self.watch(self.apps[app_name].app_html,
                        function () {
                            self.gulp.start(app_name + '-js-watch-app');
                        });
                });
            });
    }
};
